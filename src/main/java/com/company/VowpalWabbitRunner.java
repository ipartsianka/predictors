package com.company;

import com.company.commons.Exceptions;
import com.company.vw.*;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;


public class VowpalWabbitRunner {
    private static final Logger LOG = LoggerFactory.getLogger(VowpalWabbitRunner.class);
    public static final Splitter EQUALS_SPLITTER = Splitter.on("=").limit(2);
    public static final int CONSTANT_HASH = 116060;
    private static final double TEST_SET_RATIO = 0.2;
    private String vwBinaryLocation;
    private String sedLocation;
    private String sortLocation;
    private String tmpDir;
    private LossFunction lossFunction;
    private String[] vwOpts = new String[]{};
    private File modelOutput;
    private int samplingRate;
    private int testSetSize = 0;
    private int trainSetSize = 0;
    private double calibrationCoeff = 0.;

    private File binaryModelOuput = null;
    private String inputDataFile;
    private String inputDataFileTest;
    private String predictionFile;
    private String logFile;
    private String logFilePrediction;
    private BufferedWriter dataWriter;
    private BufferedWriter dataWriterTest;
    private String hashingFile;
    private String aucFile;
    private String modelCoeffsFile;
    private int passes = 1;
    private File dictionaryFile;
    private FeatureDictionary dictionary = new FeatureDictionary();
    private boolean useDictionary = true;
    private boolean sortedCoeff = true;
    private List<String> quadraticFeatures = Lists.newArrayList();
    public static final DecimalFormat VALUE_FORMAT = new DecimalFormat("#.############", DecimalFormatSymbols.getInstance(Locale.US));
    private HashMap<String,Character> featureNamespaces = new HashMap<>();
    private double threshold = 0.;

    public VowpalWabbitRunner(String vwBinaryLocation, String tmpDir, File modelOutput, LossFunction lossFunction, int sample, double thr) {
        this.vwBinaryLocation = vwBinaryLocation;
        this.tmpDir = tmpDir;
        this.modelOutput = modelOutput;
        this.lossFunction = lossFunction;
        this.samplingRate = sample;
        this.threshold = thr;
        modelCoeffsFile = tmpDir + "/model.coefficients";
        inputDataFile = tmpDir + "/input.txt";
        inputDataFileTest = tmpDir + "/input_test.txt";
        logFile = tmpDir + "/run.log";
        logFilePrediction = tmpDir + "/run_prediction.log";
        predictionFile = tmpDir + "/prediction.txt";
        hashingFile = tmpDir + "/model.invert_hash";
        aucFile = tmpDir + "/auc.txt";
    }

    public void start(HashMap<String,Character> namespaces, String sedLocation, String sortLocation) {
        this.featureNamespaces = namespaces;
        this.sedLocation = sedLocation;
        this.sortLocation = sortLocation;
        try {
            dataWriter = new BufferedWriter(new FileWriter(inputDataFile));
            dataWriterTest = new BufferedWriter(new FileWriter(inputDataFileTest));
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }


    public void feed(FeatureVector v, double value, double weight) {
        //splitting input data into train/test sets according to TEST_SET_RATIO
        boolean useNamespaces = true;//quadraticFeatures.isEmpty() ? false : true;
        try {
            String str = useDictionary ? VWHelper.toLine(dictionary, featureNamespaces, v, value, weight, useNamespaces) : VWHelper.toLine(v, value, weight);
            if ((1 - TEST_SET_RATIO)*testSetSize <= TEST_SET_RATIO*trainSetSize){
                dataWriterTest.write(str + "\n");
                testSetSize++;
            } else {
                dataWriter.write(str + "\n");
                trainSetSize++;
            }
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }


    public void setBinaryModelOuput(File binaryModelOuput) {
        this.binaryModelOuput = binaryModelOuput;
    }

    public void hashModelWriting(){
        File cacheFile = new File(tmpDir, "cache");
        if (cacheFile.exists()) {
            cacheFile.delete();
        }
        List<String> argsPrediction = Lists.newArrayList(vwBinaryLocation);
        argsPrediction.add("-t");
        argsPrediction.addAll(Lists.newArrayList("-i", binaryModelOuput.getAbsolutePath()));
        argsPrediction.addAll(Lists.newArrayList("-p", new File(predictionFile).getAbsolutePath()));
        argsPrediction.addAll(Lists.newArrayList("--invert_hash", new File(hashingFile).getAbsolutePath()));

        ProcessBuilder processBuilder = new ProcessBuilder(argsPrediction);
        LOG.info("Running prediction: " + Joiner.on(" ").join(argsPrediction) + " < " + new File(inputDataFile).getAbsolutePath());
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File(logFilePrediction)));
        processBuilder.redirectInput(ProcessBuilder.Redirect.from(new File(inputDataFile)));
        processBuilder.directory(new File(tmpDir));
        try {
            Process process = processBuilder.start();
            LOG.info("Waiting for exit code hash model");
            int exitCode = process.waitFor();
            LOG.info("Exit code hash model: " + exitCode + ". VW log:\n", Files.toString(new File(logFilePrediction), Charset.defaultCharset()));
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code prediction: " + exitCode);
            }
            if (sortedCoeff){
                //removing non coefficients from the model file
                /*
                  Version 7.3.2
                  Min label:-50.000000
                  Max label:50.000000
                  bits:18
                  0 pairs:
                  0 triples:
                  rank:0
                  lda:0
                  0 ngram:
                  0 skip:
                  options:
                  :0
                  Constant:116060:0.228012
                  a^10083:102677:0.000230
                  a^100:92694:-0.376985
                 */
                List<String> argsHead = Lists.newArrayList(sedLocation);
                argsHead.addAll(Lists.newArrayList("-e", "1,/^:0/d"));
                //argsSorting.addAll(Lists.newArrayList("sort", "-t:", "-k3n"));
                ProcessBuilder processBuilderHead = new ProcessBuilder(argsHead);
                LOG.info("Running head throwing: " + Joiner.on(" ").join(argsHead) + " " + new File(hashingFile).getAbsolutePath() + " > " +
                        new File(hashingFile + "_head").getAbsolutePath());
                processBuilderHead.redirectInput(ProcessBuilder.Redirect.PIPE);
                processBuilderHead.redirectErrorStream(true);
                processBuilderHead.redirectOutput(ProcessBuilder.Redirect.to(new File(hashingFile + "_head")));
                processBuilderHead.redirectInput(ProcessBuilder.Redirect.from(new File(hashingFile)));
                processBuilderHead.directory(new File(tmpDir));
                Process processHead = processBuilderHead.start();
                LOG.info("Waiting for exit code head throwing");
                int exitCodeHead = processHead.waitFor();
                LOG.info("Exit code head throwing: " + exitCodeHead);
                if (0 != exitCodeHead) {
                    throw new IllegalStateException("sed exited with non-zero exit code prediction: " + exitCodeHead);
                } else {
                    //sorting coefficients in increasing order
                    List<String>  argsSorting = Lists.newArrayList(sortLocation, "-t:", "-k3n");
                    ProcessBuilder processBuilderSorting = new ProcessBuilder(argsSorting);
                    LOG.info("Running head sorting: " + Joiner.on(" ").join(argsSorting) + " " + new File(hashingFile + "_head").getAbsolutePath() + " > " +
                            new File(hashingFile + "_sorted").getAbsolutePath());
                    processBuilderSorting.redirectInput(ProcessBuilder.Redirect.PIPE);
                    processBuilderSorting.redirectErrorStream(true);
                    processBuilderSorting.redirectOutput(ProcessBuilder.Redirect.to(new File(hashingFile + "_sorted")));
                    processBuilderSorting.redirectInput(ProcessBuilder.Redirect.from(new File(hashingFile + "_head")));
                    processBuilderSorting.directory(new File(tmpDir));
                    Process processSorting = processBuilderSorting.start();
                    LOG.info("Waiting for exit code sorting");
                    int exitCodeSorting = processSorting.waitFor();
                    LOG.info("Exit code sorting: " + exitCodeSorting);
                    if (0 != exitCodeSorting) {
                        throw new IllegalStateException("sort exited with non-zero exit code prediction: " + exitCodeSorting);
                    }
                }
            }
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            LOG.info("Artifact - HashModel: " + new File(hashingFile).getAbsolutePath());
        }

    }

    public void predictionWriting(){
        // vw -t -i /Users/slava/affectv/model.vw -p /Users/slava/affectv/prediction.txt --invert_hash /Users/slava/affectv/model.invert_hash
        File cacheFile = new File(tmpDir, "cache");
        if (cacheFile.exists()) {
            cacheFile.delete();
        }
        List<String> argsPrediction = Lists.newArrayList(vwBinaryLocation);
        argsPrediction.add("-t");
        argsPrediction.addAll(Lists.newArrayList("-i", binaryModelOuput.getAbsolutePath()));
        argsPrediction.addAll(Lists.newArrayList("-p", new File(predictionFile).getAbsolutePath()));
        //argsPrediction.addAll(Lists.newArrayList("--invert_hash", new File(hashingFile).getAbsolutePath()));

        ProcessBuilder processBuilder = new ProcessBuilder(argsPrediction);
        LOG.info("Running prediction: " + Joiner.on(" ").join(argsPrediction) + " < " + new File(inputDataFileTest).getAbsolutePath());
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File(logFilePrediction)));
        processBuilder.redirectInput(ProcessBuilder.Redirect.from(new File(inputDataFileTest)));
        processBuilder.directory(new File(tmpDir));
        try {
            Process process = processBuilder.start();
            LOG.info("Waiting for exit code prediction");
            int exitCode = process.waitFor();
            LOG.info("Exit code prediction: " + exitCode + ". VW log:\n", Files.toString(new File(logFilePrediction), Charset.defaultCharset()));
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code prediction: " + exitCode);
            }

            HashMap<String, Integer> predictions = predictionPerformance();
            Double precision = precisionRecall(predictions.get("true_positive"),predictions.get("false_positive"));
            Double recall = precisionRecall(predictions.get("true_positive"),predictions.get("false_negative"));
            Double accuracy = 1.0*(predictions.get("true_positive") + predictions.get("true_negative"))/(predictions.get("true_positive")
                    + predictions.get("true_negative") + predictions.get("false_positive") + predictions.get("false_negative"));
            LOG.info("Accuracy - prediction: " + accuracy);
            LOG.info("Precision - prediction: " + precision);
            LOG.info("Recall - prediction: " + recall);
            LOG.info("F measure - prediction: " + fMeasure(recall, precision));
            //LOG.info("kappa measure - prediction: " + kappaMeasure(predictions.get("true_positive"), predictions.get("false_positive"), predictions.get("false_negative"), predictions.get("true_negative")));

        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            LOG.info("Artifact - prediction: " + new File(predictionFile).getAbsolutePath());
        }
    }

    public static List<Double> readPredictedLabels(String prFile){
        List<Double> predictedLabels = Lists.newArrayList();
        try {
            BufferedReader predictionReader = new BufferedReader(new FileReader(prFile));
            String line;
            while ((line = predictionReader.readLine())!= null){
                predictedLabels.add(Double.valueOf(line));
            }
            predictionReader.close();
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
        return predictedLabels;
    }

    public static List<Integer> readRealLabels(String inputFile){
        List<Integer> realLabels = Lists.newArrayList();
        try {
            BufferedReader inputReader = new BufferedReader(new FileReader(inputFile));
            String line;
            while ((line = inputReader.readLine())!= null){
                realLabels.add(Integer.valueOf(line.split(" ")[0]));
            }
            inputReader.close();
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
        return realLabels;
    }

    public static double sigmoid(double x) {
        return 1 / (1 + Math.exp(-x));
    }

    public static double inverseSigmoid(double x) {return Math.log(x/(1-x));}


    public HashMap<String, Integer> predictionPerformance(){
        List<Double> predictedLabels = readPredictedLabels(predictionFile);
        List<Integer> realLabels = readRealLabels(inputDataFileTest);
        List<Double> predictedProbabilities = Lists.newArrayList();
        //Double[] arrPredicted = predictedLabels.toArray(new Double[predictedLabels.size()]);
        //Integer[] arrReal = realLabels.toArray(new Integer[realLabels.size()]);
        HashMap<String, Integer> hm = new HashMap<String, Integer>(4);
        Integer truePositive = 0;
        Integer falsePositive = 0;
        Integer falseNegative = 0;
        Integer trueNegative = 0;
        Double logLoss = 0.;
        Double avgCTR = 0.;
        Double avgProbability = 0.;
        long count = 0;
        for (int i = 0; i < predictedLabels.size(); i++){
            count++;
            double currLabel = predictedLabels.get(i);
            double probability = sigmoid(currLabel);
            predictedProbabilities.add(probability);
            avgCTR += (realLabels.get(i)>0) ? 1 : 0;
            avgProbability += probability;
            if (realLabels.get(i) == 1) {
                if (currLabel >= 0) truePositive++;
                else  falseNegative++;
                logLoss -= Math.log(probability);
            } else {
                if (currLabel >= 0) falsePositive++;
                else trueNegative++;
                logLoss -= Math.log(1 - probability);
            }
        }
        LOG.info("LogLoss - prediction: " + logLoss/count);
        hm.put("true_positive",truePositive);
        hm.put("false_positive",falsePositive);
        hm.put("false_negative",falseNegative);
        hm.put("true_negative",trueNegative);

        calibrationCoeff = inverseSigmoid(avgProbability/count) - inverseSigmoid(avgCTR/count);
        LOG.info("calibration coefficient: " + calibrationCoeff);
        LOG.info("avgCTR: " + avgCTR/count);
        LOG.info("avgProbability: " + avgProbability/count);

        /*try{
            BufferedWriter dataWriter = new BufferedWriter(new FileWriter(aucFile));

            StringBuilder b = new StringBuilder();
            for (int i = 0; i < predictedLabels.size(); i++){
                b.append(realLabels.get(i)).append(" ").append(VALUE_FORMAT.format(sigmoid(predictedLabels.get(i))));
                dataWriter.write(b.toString() + "\n");
                b.setLength(0);
            }
            dataWriter.close();

            int[] trueLabels = Ints.toArray(realLabels);
            int[] trueLabels_new = new int[trueLabels.length];
            int i = 0;
            for (int l : trueLabels){
                trueLabels_new[i] = 0;
                if (l == 1)  trueLabels_new[i] = 1;
                i++;
            }
            double[] scores = Doubles.toArray(predictedProbabilities);

            Curve analysis = new PrimitivesBuilder()
                    .predicteds(scores)
                    .actuals(trueLabels)
                    .build();
            // Calculate AUC ROC
            double area = analysis.rocArea();
            LOG.info("AUC - prediction: " + area);*/
        try {
            BufferedWriter dataWriter = new BufferedWriter(new FileWriter(aucFile));

            StringBuilder b = new StringBuilder();
            //List<Tuple2<Object,Object>> scoreAndLabels = Lists.newArrayList();
            if (predictedLabels.size() == realLabels.size()){
                for (int i = 0; i < predictedLabels.size(); i++){
                    Integer rLabel = realLabels.get(i);
                    Double pProbability = sigmoid(predictedLabels.get(i) - Math.log(samplingRate) - calibrationCoeff);
                    b.append(rLabel).append("\t").append(VALUE_FORMAT.format(pProbability));
                    dataWriter.write(b.toString() + "\n");
                    //scoreAndLabels.add(new Tuple2<Object, Object>(pProbability, rLabel));
                    b.setLength(0);
                }
            }

            dataWriter.close();

            // Compute raw scores on the test set.
            //SparkConf conf = new SparkConf().setAppName("AUC").setMaster("local");
            //JavaSparkContext sc = new JavaSparkContext(conf);

            //JavaRDD<Tuple2<Object, Object>> scoreAndLabelsRDD = sc.parallelize(scoreAndLabels);


            // Calculate AUC ROC
            // Get evaluation metrics.
            //BinaryClassificationMetrics metrics = new BinaryClassificationMetrics(scoreAndLabelsRDD.rdd());
            //double auROC = metrics.areaUnderROC();
            //LOG.info("AUC - prediction: " + auROC);
        }catch (IOException e) {
            throw Exceptions.wrap(e);
        }
        return hm;
    }

    public static Double precisionRecall(Integer tp, Integer x){
        return 1.0*tp/(tp+x);
    }

    public static Double fMeasure(Double recall, Double precision){
        return 2.0*recall*precision/(recall+precision);
    }

    public static Double kappaMeasure(Integer tp, Integer fp, Integer fn, Integer tn){
        Integer count = tp + fp + fn + tn;
        Double agreement = 1.*(tp + tn)/count;
        Double actualTrue = 1.*(tp + fn)/count;
        Double predictedTrue = 1.*(tp + fp);
        Double randomAgreement = actualTrue*predictedTrue + (1 - actualTrue)*(1 - predictedTrue);
        return (agreement - randomAgreement)/(1 - randomAgreement);
    }

    public void run(){
        //command line arguments https://github.com/JohnLangford/vowpal_wabbit/wiki/Command-line-arguments
        List<String> args = Lists.newArrayList(vwBinaryLocation);
        args.addAll(Lists.newArrayList(vwOpts));
        args.addAll(Arrays.asList("--holdout_period", "10"));
        // args.addAll(Arrays.asList("--holdout_off"));
        args.addAll(Lists.newArrayList("--passes", passes + ""));
        if (passes > 1) {
            File cacheFile = new File(tmpDir, "cache");
            if (cacheFile.exists()) {
                cacheFile.delete();
            }
            args.addAll(Lists.newArrayList("--cache_file", cacheFile.getAbsolutePath()));
            cacheFile.deleteOnExit();
        }
        if (binaryModelOuput != null) {
            LOG.info("Binary model will be written to " + binaryModelOuput.getAbsolutePath());
            args.addAll(Lists.newArrayList("-f", binaryModelOuput.getAbsolutePath()));
        }
        args.addAll(Arrays.asList("--loss_function", lossFunction.name().toLowerCase()));
        args.addAll(Arrays.asList("-b", "22"));
        //args.addAll(Arrays.asList("--sort_features"));
        // args.addAll(Arrays.asList("-shuf","true"));
        // args.addAll(Arrays.asList("--bootstrap","4"));

        // Change learning rate if desired (doesn't have real effects on accuracy)
        args.addAll(Arrays.asList("-l","0.7"));

        // Uncomment these 3 parameters for FTRL - proximity
        // args.addAll(Arrays.asList("--ftrl"));
        // args.addAll(Arrays.asList("--ftrl_alpha","0.075"));
        // args.addAll(Arrays.asList("--ftrl_beta","0.5"));

        // Uncomment these 3 parameters for BFGS algorithm
        args.addAll(Arrays.asList("--bfgs"));
        args.addAll(Arrays.asList("--mem", "50"));
        args.addAll(Arrays.asList("--termination", "0.000001"));

        // Found with VW-Hypersearch
        args.addAll(Arrays.asList("--l1", "1e-6"));
        args.addAll(Arrays.asList("--l2", "1e-6"));
        args.addAll(quadraticFeatures);
        args.addAll(Arrays.asList("--readable_model", new File(modelCoeffsFile).getAbsolutePath()));


        ProcessBuilder processBuilder = new ProcessBuilder(args);
        LOG.info("Running: " + Joiner.on(" ").join(args) + " < " + new File(inputDataFile).getAbsolutePath());
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File(logFile)));
        processBuilder.redirectInput(ProcessBuilder.Redirect.from(new File(inputDataFile)));
        processBuilder.directory(new File(tmpDir));
        try {
            Process process = processBuilder.start();
            LOG.info("Waiting for exit code");
            int exitCode = process.waitFor();
            LOG.info("Exit code: " + exitCode + ". VW log:\n", Files.toString(new File(logFile), Charset.defaultCharset()));
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code: " + exitCode);
            }
            if (useDictionary && dictionaryFile != null) {
                try (FileWriter w = new FileWriter(dictionaryFile)) {
                    dictionary.write(w);
                }
            }
            LOG.info("Building - OK. Reading model file");
            hashModelWriting();
            predictionWriting();
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            if (binaryModelOuput != null) {
                LOG.info("Binary output: " + binaryModelOuput.getAbsolutePath());
            }
            if (useDictionary && dictionaryFile != null) {
                LOG.info("Dictionary file: " + dictionaryFile.getAbsolutePath());
            }
            LOG.info("Model json: " + modelOutput.getAbsolutePath());
            LOG.info("Artifact - log file: " + new File(logFile).getAbsolutePath());
            LOG.info("Artifact - data file: " + new File(inputDataFile).getAbsolutePath());
            if (useDictionary && dictionaryFile != null) {
                LOG.info("Artifact - dictionary file: " + dictionaryFile.getAbsolutePath());
            }
            //LOG.info("Artifact - raw model    : " + new File(readableModelFile).getAbsolutePath());
            LOG.info("Artifact - model coeffs: " + new File(modelCoeffsFile).getAbsolutePath());
            LOG.info("Artifact - model json: " + modelOutput.getAbsolutePath());
        }
    }

    public void build() {
        try {
            dataWriter.close();
            dataWriterTest.close();
        } catch (Exception e) {
            throw new IllegalStateException("Unable to close data writer file: " + inputDataFile);
        }
        run();
        readModelFile();
    }

    private void addLinesLists(Map<String, List<List<Object>>> coeffs, ArrayList<WabbitWeightLine> lines) throws IOException{
        HashMap<String, ArrayList<String>> groupedLines = new HashMap<>();
        StringBuilder b = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        for (WabbitWeightLine line : lines){
            ArrayList<String> features = line.getFeatures();
            String prefix = "";
            for (String ft : features){
                b.append(prefix);
                prefix = "_";
                if (ft.contains("user_segments"))
                    b.append("user_segment").append("_").append("ad_id");
                else
                if (ft.contains("user_segment_"))
                    b.append("user_segment");
                else
                if (ft.contains("ad_categories"))
                    b.append("ad_category");
                else
                    b.append(ft);
            }
            ArrayList<String> values = Lists.newArrayList();
            if (groupedLines.containsKey(b.toString())) values = groupedLines.get(b.toString());
            HashMap<String,String> featureValues = line.getFeatureValues();
            HashMap<String,FeatureType> featureTypes = line.getFeatureTypes();

            for (String v : features) {
                if (v.contains("user_segment") || v.contains("ad_categories")){
                    String number = v.split("_")[2];
                    sb.append(number).append("\t");
                }
                String fv = featureValues.get(v);
                boolean ft = featureTypes.get(v).isCategorial();
                if (ft && !fv.equals("!"))
                    sb.append(fv).append("\t");
            }
            sb.append(line.getCoeff());
            values.add(sb.toString());
            groupedLines.put(b.toString(), values);
            b.setLength(0);
            sb.setLength(0);
        }

        for (String ft : groupedLines.keySet()){
            ArrayList<String> values = groupedLines.get(ft);
            List<List<Object>> coef = new ArrayList<>();
            for (String f : values){
                List<Object> st = new ArrayList<>();
                double cf = 0.;
                if (f.contains("\t")){
                    for (String fn : f.substring(0, f.lastIndexOf("\t")).split("\\t")) {
                        st.add(fn);
                    }
                    cf = Double.valueOf(f.substring(f.lastIndexOf("\t") + 1));
                    //st.add(Double.valueOf(f.substring(f.lastIndexOf("\t") + 1)));
                } else
                cf = Double.valueOf(f);
                if (Math.abs(cf) > threshold){
                    st.add(cf);
                    coef.add(st);
                }
                //st.add(Double.valueOf(f));
                //coef.add(st);
            }
            coeffs.put(ft, coef);
        }
    }

    private void readModelFile() {
        if (sortedCoeff && useDictionary) hashingFile += "_sorted";
        try (
                JsonGenerator gen = new JsonFactory().createJsonGenerator(modelOutput, JsonEncoding.UTF8);
                BufferedReader r = new BufferedReader(new FileReader(hashingFile))
        ) {
            String line;
            gen.setPrettyPrinter(new DefaultPrettyPrinter());
            gen.writeStartObject();
            gen.writeFieldName("meta");
            gen.writeStartObject();
            gen.writeStringField("loss_function", lossFunction.name());
            gen.writeEndObject();
            gen.writeFieldName("features");
            gen.writeStartArray();
            if (!useDictionary) {
                while (null != (line = r.readLine())) {
                    if (line.startsWith("Constant:")) {
                        WabbitWeightLine l = new WabbitHumanReadableLine(line);
                        writeConstant(gen, l);
                    } else if (line.contains(":") && line.contains("^")) {
                        WabbitWeightLine l = new WabbitHumanReadableLine(line);
                        writeLine(gen, l);
                    }
                }
            } else {
                boolean weightVectorStarted = false;
                if (sortedCoeff) weightVectorStarted = true;
                ArrayList<WabbitWeightLine> lines = Lists.newArrayList();
                while (null != (line = r.readLine())) {
                    if (!weightVectorStarted) {
                        if (line.equals(":0")) {
                            weightVectorStarted = true;
                        }
                        continue;
                    } else {
                        WabbitWeightLine l = new WabbitRawLine(line, dictionary);
                        if (l.getFeatures() == null) {
                            writeConstant(gen, l);
                            //writeConstantasArray(gen, l);
                        } else {
                            writeLine(gen, l);
                            //lines.add(l);
                        }
                    }
                }
                /*if (lines.size() > 0){
                    writeLines(gen, lines);
                } */
            }
            gen.writeEndArray();
            gen.writeEndObject();
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    private void writeLines(JsonGenerator gen, ArrayList<WabbitWeightLine> lines) throws IOException{
        HashMap<String, ArrayList<String>> groupedLines = new HashMap<>();
        for (WabbitWeightLine line : lines){
            ArrayList<String> features = line.getFeatures();
            StringBuilder b = new StringBuilder();
            String prefix = "";
            for (String ft : features){
                b.append(prefix);
                prefix = ", ";
                b.append(ft);
            }
            ArrayList<String> values = Lists.newArrayList();
            if (groupedLines.containsKey(b.toString())) values = groupedLines.get(b.toString());
            HashMap<String,String> featureValues = line.getFeatureValues();
            StringBuilder sb = new StringBuilder();
            for (String v : featureValues.keySet()) {
                sb.append(featureValues.get(v)).append("\t");
            }
            sb.append(line.getCoeff());
            values.add(sb.toString());
            groupedLines.put(b.toString(), values);
        }

        for (String ft : groupedLines.keySet()){
            ArrayList<String> values = groupedLines.get(ft);
            gen.writeStartObject();
            gen.writeFieldName(ft);
            gen.writeStartArray();
            for (String f : values){
                gen.writeStartArray();
                for (String fn : f.substring(0, f.lastIndexOf("\t")).split("\\t"))
                    gen.writeString(fn);
                gen.writeNumber(Double.valueOf(f.substring(f.lastIndexOf("\t") + 1)));
                gen.writeEndArray();
            }
            gen.writeEndArray();
            gen.writeEndObject();

        }

    }

    private void writeConstant(JsonGenerator gen, WabbitWeightLine l) throws IOException {
        gen.writeStartObject();
        gen.writeFieldName("tuple");
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField("feature", "constant");
        gen.writeStringField("ftype", "constant");
        gen.writeStringField("value", "");
        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeNumberField("c", l.coeff - Math.log(samplingRate) - calibrationCoeff);
        gen.writeEndObject();
    }

    private void writeLine(JsonGenerator gen, WabbitWeightLine l) throws IOException {
        gen.writeStartObject();
        gen.writeFieldName("tuple");
        gen.writeStartArray();
        for (String ft : l.getFeatures()){
            gen.writeStartObject();
            gen.writeStringField("feature", ft);
            gen.writeStringField("ftype", FeatureType.classToString(l.getFeatureTypes().get(ft).getType()));
            if (l.getFeatureValues().get(ft) != null) {
                gen.writeStringField("value", l.getFeatureValues().get(ft));
            }
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeNumberField("c", l.coeff);
        gen.writeEndObject();
    }

    public void setPasses(int passes) {
        this.passes = passes;
    }

    public int getPasses() {
        return passes;
    }

    public void setQuadratic(List<String> quadraticFeatures) {
        List<String> qFeatures = Lists.newArrayList();
        for (String pair : quadraticFeatures){
            qFeatures.add("-q");
            if (pair.equals("::"))
                qFeatures.add("::");
            else {
                String[] sPair = pair.split(":");
                if (sPair.length == 2)
                    qFeatures.add(featureNamespaces.get(sPair[0]).toString() + featureNamespaces.get(sPair[1]).toString());
                else if (sPair.length == 1)
                    qFeatures.add(featureNamespaces.get(sPair[0]).toString() + ":");
            }
        }
        this.quadraticFeatures = qFeatures;
    }

    public void setSaveDictionaryTo(File saveDictionary) {
        this.dictionaryFile = saveDictionary;
    }

    private static class WabbitRawLine extends WabbitWeightLine {
        private WabbitRawLine(String line, FeatureDictionary dictionary) {
            //reading coefficients from VW output format
            try{
                Iterator<String> it = Splitter.on(":").split(line).iterator();
                String hashedFeature = it.next();
                int hash = Integer.parseInt(it.next());
                features = new ArrayList<>();
                featureValues = new HashMap<>();
                featureTypes = new HashMap<>();
                coeff = Double.parseDouble(it.next());
                hashes = new HashMap<>();
                //if (hash == CONSTANT_HASH || hashedFeature.contains("Constant")) {
                if (hashedFeature.contains("Constant")) {
                    features = null;
                    //featureType = FeatureType.get(Double.class, false);
                    this.featureValues = null;
                } else {
                    if (hashedFeature.contains("^")){
                        //Iterator<String> itf = Splitter.on("^").split(hashedFeature).iterator();
                        Iterator<String> itf = Splitter.on("*").split(hashedFeature).iterator();
                        while (itf.hasNext()){
                            Iterator<String> itf2 = Splitter.on("^").split(itf.next()).iterator();
                            String name = itf2.next();
                            int hCode = Integer.parseInt(itf2.next());
                            FeatureDictionary.Feature resolve = dictionary.resolve(hCode);
                            Preconditions.checkState(resolve != null, "Unknown feature %s", hCode);
                            String feature = resolve.getName();
                            FeatureType featureType = resolve.getFeatureType();
                            String featureValue = resolve.getValue() == null ? null : resolve.getFeatureType().getDataType().toString(resolve.getValue());
                            features.add(feature);
                            featureValues.put(feature,featureValue);
                            featureTypes.put(feature,featureType);
                            hashes.put(feature,hCode);
                        }
                    }
                }
            } catch (Exception e)  {

            }
        }
    }

    private static class WabbitHumanReadableLine extends WabbitWeightLine {
        private WabbitHumanReadableLine(String line) {
            throw new UnsupportedOperationException();
        }

    }
    public static abstract class WabbitWeightLine {
        protected ArrayList<String> features;
        protected HashMap<String,String> featureValues;
        protected HashMap<String,Integer> hashes;
        protected double coeff;
        protected HashMap<String,FeatureType> featureTypes;

        public ArrayList<String> getFeatures() {
            return features;
        }

        public HashMap<String,String>  getFeatureValues() {
            return featureValues;
        }

        public HashMap<String,Integer> getHashes() {
            return hashes;
        }

        public double getCoeff() {
            return coeff;
        }

        public HashMap<String,FeatureType> getFeatureTypes() {
            return featureTypes;
        }
    }

    public void setUseDictionary(boolean useDictionary) {
        this.useDictionary = useDictionary;
    }
}
