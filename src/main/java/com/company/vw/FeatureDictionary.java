package com.company.vw;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class FeatureDictionary {
    public static final String TYPE_CATEGORIAL = "categorial";
    public static final String TYPE_NUMERIC = "numeric";
    private int nextId = 0;
    private Map<Feature, Integer> features = new HashMap<>();
    private Map<Integer, Feature> reverse = new HashMap<>();

    public int getId(String categorialFeature, FeatureType type, Object value) {
        Feature key = new Feature(categorialFeature, type, value);
        Integer id = features.get(key);
        if (id != null) {
            return id;
        }
        id = nextId++;
        add(id, key);
        return id;
    }

    public void write(Writer w) {
        PrintWriter pw = new PrintWriter(w);

        Map<Feature, Integer> numericSorted = new TreeMap<>(features);
        for (Feature key : numericSorted.keySet()) {
            Integer id = numericSorted.get(key);
            if (key.getFeatureType().isCategorial()) {
                pw.printf(TYPE_CATEGORIAL + "\t%s\t%s=%s\t%s%n", FeatureType.classToString(key.featureType.getType()), key.name, key.value, id);
            } else {
                pw.printf(TYPE_NUMERIC + "\t%s\t%s\t%s%n", FeatureType.classToString(key.featureType.getType()), key.name, id);
            }
        }
        pw.flush();
    }

    public void read(Reader r) throws IOException {
        BufferedReader buff = new BufferedReader(r);
        String line;
        while (null != (line = buff.readLine())) {
            Iterator<String> iterator = Splitter.on('\t').split(line).iterator();
            String type = iterator.next();
            String dataClass = iterator.next();
            FeatureType featureType = FeatureType.get(FeatureType.getClass(dataClass), type.equals(TYPE_CATEGORIAL));
            Preconditions.checkState(featureType != null, "Can't find feature for type %s/%s", dataClass, type);
            String original = iterator.next();
            int id = Integer.parseInt(iterator.next());
            Feature feature;
            if (featureType.isCategorial()) {
                Iterator<String> kv = Splitter.on("=").limit(2).split(original).iterator();
                feature = new Feature(kv.next(), featureType, featureType.getDataType().parse(kv.next()));
            } else {
                feature = new Feature(original, featureType, null);
            }
            nextId++;
            add(id, feature);
        }
    }


    private void add(int id, Feature feature) {
        features.put(feature, id);
        reverse.put(id, feature);
        //if (read)
        //    nextId++;
    }

    public Feature resolve(int id) {
        return reverse.get(id);
    }

    public static class Feature implements Comparable<Feature> {
        private String name;
        private FeatureType featureType;
        private Object value;

        private Feature(String name, FeatureType featureType, Object value) {
            this.name = name;
            this.featureType = featureType;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Feature)) return false;

            Feature that = (Feature) o;
            return !(name != null ? !name.equals(that.name) : that.name != null) && !(value != null ? !value.equals(that.value) : that.value != null);

        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (value != null ? value.hashCode() : 0);
            return result;
        }

        @Override
        public int compareTo(Feature o) {
            int res = name.compareTo(o.name);
            if (res == 0) {
                res = String.valueOf(value).compareTo(String.valueOf(o.value));
            }
            return res;
        }

        public String getName() {
            return name;
        }

        public FeatureType getFeatureType() {
            return featureType;
        }

        public Object getValue() {
            return value;
        }
    }
}
