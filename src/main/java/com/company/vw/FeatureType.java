package com.company.vw;

import com.company.beans.DataType;
import com.company.beans.DataTypes;
import com.company.commons.Exceptions;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;

public abstract class FeatureType<T> {
    public static final String CATEGORIAL_PREFIX = "$";
    public static final String NUMERIC_PREFIX = "!";

    private static int nextId;
    private static FeatureType[] idIndex;

    private static LinkedHashMap<String, FeatureType> nameIndex = new LinkedHashMap();
    private static LinkedHashMap<TypeKey, FeatureType> typeIndex = new LinkedHashMap();
    private int id;
    private String name;
    private Class<T> type;
    private boolean isCategorial;
    private DataType<T> dataType;

    private FeatureType(String name, Class<T> type, boolean categorial, DataType dataType) {
        this.dataType = dataType;
        id = nextId++;
        this.name = name;
        this.type = type;
        isCategorial = categorial;
        nameIndex.put(name, this);
        typeIndex.put(new TypeKey(type, categorial), this);
    }

    public T value(Object o) {
        return (T) o;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Class<T> getType() {
        return type;
    }

    public DataType<T> getDataType() {
        return dataType;
    }

    public boolean isCategorial() {
        return isCategorial;
    }

    public static int typesNumber() {
        return nameIndex.size();
    }

    protected abstract void validate(T o);

    public abstract Double toDouble(T o);

    public static FeatureType get(int id) {
        return id >= 0 && id < idIndex.length ? idIndex[id] : null;
    }

    public static FeatureType get(String name) {
        return nameIndex.get(name);
    }

    public static final FeatureType<Integer> CATEGORIAL_INT = new FeatureType<Integer>(CATEGORIAL_PREFIX + "int", Integer.class, true, DataTypes.getType("integer")) {
        @Override
        protected void validate(Integer o) {

        }

        @Override
        public Double toDouble(Integer o) {
            throw new UnsupportedOperationException("Not supported for categorial features (" + getName() + ")");
        }
    };

    public static final FeatureType<String> CATEGORIAL_STRING = new FeatureType<String>(CATEGORIAL_PREFIX + "string", String.class, true, DataTypes.getType("string")) {
        @Override
        protected void validate(String str) {
            /*if (str.contains(" ")) {
                throw new IllegalStateException("Strings with spaces aren't supported");
            }
            if (str.contains(":")) {
                throw new IllegalStateException("Strings with ':' aren't supported");
            }  */
        }
        @Override
        public Double toDouble(String o) {
            throw new UnsupportedOperationException("Not supported for categorial features (" + getName() + ")");
        }
    };
    public static final FeatureType<Double> NUMERIC_DOUBLE = new FeatureType<Double>(NUMERIC_PREFIX + "double", Double.class, false, DataTypes.getType("double")) {
        @Override
        protected void validate(Double o) {

        }
        @Override
        public Double toDouble(Double o) {
            return o;
        }
    };
    public static final FeatureType<Integer> NUMERIC_INTEGER = new FeatureType<Integer>(NUMERIC_PREFIX + "integer", Integer.class, false, DataTypes.getType("integer")) {
        @Override
        protected void validate(Integer o) {

        }
        @Override
        public Double toDouble(Integer o) {
            return Double.valueOf(o);
        }
    };
    public static final FeatureType<Boolean> NUMERIC_BOOLEAN = new FeatureType<Boolean>(NUMERIC_PREFIX + "bool", Boolean.class, true, DataTypes.getType("boolean")) {
        @Override
        protected void validate(Boolean o) {

        }

        @Override
        public Double toDouble(Boolean o) {
            return o ? 1.0 : 0.0;
        }
    };

    static {
        idIndex = new FeatureType[nameIndex.size()];
        for (FeatureType t : nameIndex.values()) {
            idIndex[t.id] = t;
        }
    }

    public static <T> FeatureType<T> get(Class<T> cls, boolean categorial) {
        return typeIndex.get(new TypeKey(cls, categorial));
    }

    public static Collection<FeatureType> allTypes() {
        return nameIndex.values();
    }

    public static Collection<FeatureType> categorialTypes() {
        Collection<FeatureType> res = new HashSet<>();
        for (FeatureType type : FeatureType.allTypes()) {
            if (type.isCategorial)
                res.add(type);
        }
        return res;
    }

    private static class TypeKey {
        private Class type;
        private boolean categorial;
        public TypeKey(Class type, boolean categorial) {
            this.type = type;
            this.categorial = categorial;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TypeKey typeKey = (TypeKey) o;

            if (categorial != typeKey.categorial) return false;
            if (!type.equals(typeKey.type)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + (categorial ? 1 : 0);
            return result;
        }
    }


    public static String classToString(Class cls) {
        if (cls.getName().startsWith("java.lang.")) {
            return cls.getSimpleName();
        } else {
            return cls.getName();
        }
    }

    public static Class getClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            try {
                return Class.forName("java.lang." + className);
            } catch (ClassNotFoundException e1) {
                throw Exceptions.illegalState(e1, "Can't resolve class %s", className);
            }
        }
    }


}
