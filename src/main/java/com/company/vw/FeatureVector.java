package com.company.vw;

import com.google.common.base.Preconditions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FeatureVector {
    private Map<String, Object>[] featuresByType = new Map[FeatureType.typesNumber()];
    private Map<String, Object> allFeatures = new HashMap<>();
    {
        for (int i = 0; i < featuresByType.length; i++) {
            featuresByType[i] = new HashMap<>(10, 1.0f);
        }
    }
    public <T> void setFeatureValue(String feature, FeatureType<T> t, T value) {
        try {
            setFeatureValue(feature, new FeatureValue<T>(t, value));
        } catch (Exception e) {
            throw new IllegalArgumentException("Can't set " + feature + "=" + value + " type of " + t, e);
        }
    }

    public <T> void setFeatureValue(String feature, Object value, boolean categorial) {
        Preconditions.checkState(value != null, "Null value for feature %s", feature);
        FeatureType type = FeatureType.get(value.getClass(), categorial);
        Preconditions.checkState(type != null, "Features of type %s (categorial=%s) aren't supported", value.getClass().getName(), categorial);
        /*if (feature.equals("user_segments")){
            String[] values = value.toString().split(",");
            for (String val: values)
                setFeatureValue(feature, type, val);
        } else
            setFeatureValue(feature, type, value); */
        setFeatureValue(feature, type, value);
    }

    public <T> void setFeatureValue(String feature, FeatureValue<T> val) {
        featuresByType[val.getType().getId()].put(feature, val.getValue());
        allFeatures.put(feature, val.getValue());
    }


    public <T> Map<String, T> getFeatures(FeatureType<T> type) {
        return (Map<String, T>) featuresByType[type.getId()];
    }

    public Set<String> getCategoricalFeatures(){
        Set<String> res = new HashSet<>();
        for (FeatureType type : FeatureType.categorialTypes())
            res.addAll(getFeatures(type).keySet());
        return res;
    }

    public Object getFeature(String feature) {
        return allFeatures.get(feature);
    }

    public Set<String> getFeaturesNames() { return allFeatures.keySet(); }
}
