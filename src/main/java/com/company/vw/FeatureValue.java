package com.company.vw;

import com.google.common.base.Preconditions;

public class FeatureValue<T> {
    private FeatureType<T> type;
    private T value;

    public FeatureValue(FeatureType<T> type, T value) {
        Preconditions.checkState(value != null, "Null features aren't supported");
        type.validate(value);
        this.type = type;
        this.value = value;
    }

    public FeatureType<T> getType() {
        return type;
    }

    public T getValue() {
        return value;
    }
}
