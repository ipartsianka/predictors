package com.company.vw;

public enum LossFunction {
    SQUARED, HINGE, LOGISTIC, QUANTILE
}
