package com.company.vw;

import com.google.common.base.Preconditions;
import com.company.Predictor;
import org.codehaus.jackson.JsonNode;

import java.util.HashMap;
import java.util.Map;

public class VowpalWabbit implements Predictor {
    private LossFunction lossFunction;
    private double constant;
    private Map<CategorialFeatureKey, Double> categorialCoefficients = new HashMap<>();
    private Map<String, Double> numericCoefficients = new HashMap<>();
    private Map<CategorialFeatureKey, Map<CategorialFeatureKey, Double>> categorialCategorialCoefficients = new HashMap<>();
    private Map<CategorialFeatureKey, Map<String, Double>> categorialNumericCoefficients = new HashMap<>();
    private Map<String, Map<String, Double>> numericNumericCoefficients = new HashMap<>();

    public VowpalWabbit(JsonNode node) {
        String lossFunctionName = node.get("meta").get("loss_function").asText().toUpperCase();
        lossFunction = LossFunction.valueOf(lossFunctionName);
        Preconditions.checkState(lossFunction != null, "Unknown loss function %s " + lossFunctionName);
        for (JsonNode n : node.get("features")) {
            JsonNode nd = n.get("tuple");
            if (nd.size() > 0){
                JsonNode first = nd.get(0);
                String typeNameFirst = first.get("ftype").asText();
                String featureNameFirst = first.get("feature").asText();
                double coeff = n.get("c").asDouble();
                if (featureNameFirst.equals("constant")) {
                    constant = coeff;
                } else {
                    FeatureType featureTypeFirst = FeatureType.get(FeatureType.getClass(typeNameFirst), first.has("value"));
                    Preconditions.checkState(featureTypeFirst != null, "Unknown feature type %s", typeNameFirst);
                    if (nd.size() == 1) {
                        if (!featureTypeFirst.isCategorial()) {
                            numericCoefficients.put(featureNameFirst, coeff);
                        } else {
                            Object value = featureTypeFirst.getDataType().parse(first.get("value").asText());
                            categorialCoefficients.put(new CategorialFeatureKey(featureNameFirst, value), coeff);
                        }
                    } else {
                        JsonNode second = nd.get(1);
                        String typeNameSecond = second.get("ftype").asText();
                        FeatureType featureTypeSecond = FeatureType.get(FeatureType.getClass(typeNameSecond), second.has("value"));
                        Preconditions.checkState(featureTypeSecond != null, "Unknown feature type %s", typeNameSecond);
                        String featureNameSecond = second.get("feature").asText();
                        if (featureTypeFirst.isCategorial() && featureTypeSecond.isCategorial()){
                            Object valueFirst = featureTypeFirst.getDataType().parse(first.get("value").asText());
                            Object valueSecond = featureTypeSecond.getDataType().parse(second.get("value").asText());
                            CategorialFeatureKey firstFeature = new CategorialFeatureKey(featureNameFirst, valueFirst);
                            CategorialFeatureKey secondFeature = new CategorialFeatureKey(featureNameSecond, valueSecond);
                            Map<CategorialFeatureKey, Double> featureCoefficients;
                            if (categorialCategorialCoefficients.containsKey(firstFeature))
                                featureCoefficients = categorialCategorialCoefficients.get(firstFeature);
                            else
                                featureCoefficients = new HashMap<>();
                            featureCoefficients.put(secondFeature, coeff);
                            categorialCategorialCoefficients.put(firstFeature, featureCoefficients);
                        }  else
                        if (featureTypeFirst.isCategorial()) {
                            Object valueFirst = featureTypeFirst.getDataType().parse(first.get("value").asText());
                            CategorialFeatureKey firstFeature = new CategorialFeatureKey(featureNameFirst, valueFirst);
                            Map<String, Double> featureCoefficients;
                            if (categorialNumericCoefficients.containsKey(firstFeature))
                                featureCoefficients = categorialNumericCoefficients.get(firstFeature);
                            else
                                featureCoefficients = new HashMap<>();
                            featureCoefficients.put(featureNameSecond, coeff);
                            categorialNumericCoefficients.put(firstFeature, featureCoefficients);
                        }  else {
                            if (featureTypeSecond.isCategorial()) {
                                Object valueSecond = featureTypeSecond.getDataType().parse(second.get("value").asText());
                                CategorialFeatureKey secondFeature = new CategorialFeatureKey(featureNameSecond, valueSecond);
                                Map<String, Double> featureCoefficients;
                                if (categorialNumericCoefficients.containsKey(secondFeature))
                                    featureCoefficients = categorialNumericCoefficients.get(secondFeature);
                                else
                                    featureCoefficients = new HashMap<>();
                                featureCoefficients.put(featureNameFirst, coeff);
                                categorialNumericCoefficients.put(secondFeature, featureCoefficients);
                            } else {
                                Map<String, Double> featureCoefficients;
                                if (numericNumericCoefficients.containsKey(featureNameFirst))
                                    featureCoefficients = numericNumericCoefficients.get(featureNameFirst);
                                else
                                    featureCoefficients = new HashMap<>();
                                featureCoefficients.put(featureNameSecond, coeff);
                                numericNumericCoefficients.put(featureNameFirst, featureCoefficients);
                            }
                        }
                    }
                }
            }
        }
    }

    public Prediction predict(FeatureVector key) {
        double predictedValue = constant;
        final StringBuilder debugInformationBuilder = new StringBuilder();
        debugInformationBuilder.append(constant);
        for (FeatureType type : FeatureType.allTypes()) {
            Map<String, Object> featureMap = key.<Object>getFeatures(type);
            if (type.isCategorial()) {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    Double coeff = categorialCoefficients.get(new CategorialFeatureKey(e.getKey(), e.getValue()));
                    if (coeff != null) {
                        predictedValue += rectifier(coeff, 0.5);
                        String sign = (coeff >= 0.) ? " +" : " ";
                        debugInformationBuilder.append(sign).append(coeff.toString()).append("*").append(e.getKey()).append("=").append(e.getValue());
                    }
                }
            } else {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    Double coeff = numericCoefficients.get(e.getKey());
                    if (coeff != null) {
                        predictedValue += rectifier(coeff, 0.5) * type.toDouble(e.getValue());
                        String sign = (coeff >= 0.) ? "+" : "";
                        debugInformationBuilder.append(sign).append(coeff.toString()).append("*").append(type.toDouble(e.getValue())).append("*").append(e.getKey());
                    }
                }

            }
        }
        return new Prediction(VWHelper.getPrediction(lossFunction, predictedValue)) {
            @Override
            public String getDebugInformation() {
                return debugInformationBuilder.toString();
            }
        };
    }

    public Prediction predictNew(FeatureVector key) {
        double predictedValue = constant;
        final StringBuilder debugInformationBuilder = new StringBuilder();
        debugInformationBuilder.append(constant);

        for (FeatureType type : FeatureType.allTypes()) {
            Map<String, Object> featureMap = key.<Object>getFeatures(type);
            if (type.isCategorial()) {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    CategorialFeatureKey categorialFeatures = new CategorialFeatureKey(e.getKey(), e.getValue());
                    Double coeff = categorialCoefficients.get(categorialFeatures);
                    if (coeff != null) {
                        predictedValue += rectifier(coeff, 1.);
                        String sign = (coeff >= 0.) ? " +" : " ";
                        debugInformationBuilder.append(sign).append(coeff.toString()).append("*").append(e.getKey()).append("=").append(e.getValue());
                    }

                    Map<CategorialFeatureKey, Double> cCoefficients = categorialCategorialCoefficients.get(categorialFeatures);
                    Map<String, Double> nCoefficients = categorialNumericCoefficients.get(categorialFeatures);

                    for (FeatureType typeSecond : FeatureType.allTypes()) {
                        Map<String, Object> featureMapSecond = key.<Object>getFeatures(typeSecond);
                        if (typeSecond.isCategorial() && cCoefficients != null) {
                            for (Map.Entry<String, Object> e2 : featureMapSecond.entrySet()) {
                                CategorialFeatureKey categorialFeatures2 = new CategorialFeatureKey(e2.getKey(), e2.getValue());
                                Double coeff2 = cCoefficients.get(categorialFeatures2);
                                if (coeff2 != null) {
                                    predictedValue += rectifier(coeff2, 1.);
                                    String sign = (coeff2 >= 0.) ? " +" : " ";
                                    debugInformationBuilder.append(sign).append(coeff2.toString()).append("*").append(e2.getKey()).append("=").append(e2.getValue()).
                                            append("&").append(e.getKey()).append("=").append(e.getValue());
                                }
                            }

                        }
                        if (!typeSecond.isCategorial() && nCoefficients != null) {
                            for (Map.Entry<String, Object> e2 : featureMapSecond.entrySet()) {
                                Double coeff2 = nCoefficients.get(e2.getKey());
                                if (coeff2 != null) {
                                    //System.out.println(e.getKey() + " " + e.getValue() + " " + e2.getKey() + " " + e2.getValue());
                                    predictedValue += rectifier(coeff2, 1.) * typeSecond.toDouble(e2.getValue());
                                    String sign = (coeff2 >= 0.) ? "+" : "";
                                    try{
                                        debugInformationBuilder.append(sign).append(coeff2.toString()).append("*").append(typeSecond.toDouble(e2.getValue())).append("*").append(e2.getKey()).
                                                append("&").append(e.getKey()).append("=").append(e.getValue());
                                    }catch(Exception ex){

                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    Double coeff = numericCoefficients.get(e.getKey());
                    if (coeff != null) {
                        predictedValue += rectifier(coeff, 1.) * type.toDouble(e.getValue());
                        String sign = (coeff >= 0.) ? "+" : "";
                        debugInformationBuilder.append(sign).append(coeff.toString()).append("*").append(type.toDouble(e.getValue())).append("*").append(e.getKey());
                    }

                    Map<String, Double> nCoefficients = numericNumericCoefficients.get(e.getKey());
                    for (FeatureType typeSecond : FeatureType.allTypes()) {
                        Map<String, Object> featureMapSecond = key.<Object>getFeatures(typeSecond);
                        if (!typeSecond.isCategorial() && nCoefficients != null) {
                            for (Map.Entry<String, Object> e2 : featureMapSecond.entrySet()) {
                                Double coeff2 = nCoefficients.get(e2.getKey());
                                if (coeff2 != null) {
                                    predictedValue += rectifier(coeff2, 1.) * typeSecond.toDouble(e2.getValue()) * type.toDouble(e.getValue());
                                    String sign = (coeff2 >= 0.) ? "+" : "";
                                    debugInformationBuilder.append(sign).append(coeff.toString()).append("*").append(typeSecond.toDouble(e2.getValue())).append("*").append(e2.getKey()).
                                            append("*").append(type.toDouble(e.getValue())).append("*").append(e.getKey());
                                }
                            }
                        }
                    }
                }
            }
        }

        return new Prediction(VWHelper.getPrediction(lossFunction, predictedValue)) {
            @Override
            public String getDebugInformation() {
                return debugInformationBuilder.toString();
            }
        };
    }

    public Double rectifier(Double coefficient, Double threshold){
        return coefficient >= threshold ? threshold : coefficient;
    }

    public static class CategorialFeatureKey {
        private String feature;
        private Object value;

        public CategorialFeatureKey(String feature, Object value) {
            this.feature = feature;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CategorialFeatureKey that = (CategorialFeatureKey) o;
            return feature.equals(that.feature) && value.equals(that.value);

        }

        @Override
        public int hashCode() {
            int result = feature.hashCode();
            result = 31 * result + value.hashCode();
            return result;
        }
    }
}
