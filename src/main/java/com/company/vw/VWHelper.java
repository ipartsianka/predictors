package com.company.vw;

import com.company.commons.Util;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class VWHelper {
    public static final DecimalFormat VALUE_FORMAT = Util.decimalFormat(9);

    private VWHelper() {
    }

    public static void validate(FeatureVector features, double value, double weight) throws RuntimeException {
        toLine(features, value, weight);
    }

    //writing features in vw format. http://hunch.net/~vw/validate.html
    public static String toLine(FeatureDictionary dict, HashMap<String,Character> namespaces, FeatureVector features, double value, double weight, boolean useNamespaces) {
        StringBuilder b = new StringBuilder();
        b.append(format(value)).append(" ").append(format(weight)).append(" ");
        for (FeatureType type : FeatureType.allTypes()) {
            Map<String, Object> featureMap = features.<Object>getFeatures(type);
            if (featureMap.isEmpty()) {
                continue;
            }
            if (!useNamespaces) b.append("| ");
            if (!type.isCategorial()) {
                for (Iterator<Map.Entry<String, Object>> iterator = featureMap.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry<String, Object> e = iterator.next();
                    int id = dict.getId(e.getKey(), type, null);
                    if (useNamespaces){
                        b.append("|");
                        b.append(namespaces.get(e.getKey()) + " ");
                    }
                    b.append(id).append(":").append(format(type.toDouble(e.getValue())));
                    //if (iterator.hasNext())
                    b.append(" ");
                }
            } else {
                for (Iterator<Map.Entry<String, Object>> iterator = featureMap.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry<String, Object> e = iterator.next();
                    int id = dict.getId(e.getKey(), type, e.getValue());
                    if (useNamespaces){
                        b.append("|");
                        b.append(namespaces.get(e.getKey()) + " ");
                    }
                    b.append(id);//.append(":1");
                    // if (iterator.hasNext())
                    b.append(" ");
                }
            }
        }
        return b.toString();

    }

    public static String toLine(FeatureVector features, double value, double weight) {
        StringBuilder b = new StringBuilder();
        b.append(format(value)).append(" ").append(format(weight)).append(" ");
        for (FeatureType type : FeatureType.allTypes()) {
            Map<String, Object> featureMap = features.<Object>getFeatures(type);
            if (featureMap.isEmpty()) {
                continue;
            }
            //b.append("|").append(type.getName()).append(" ");
            if (!type.isCategorial()) {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    //b.append(e.getKey()).append(":").append(VALUE_FORMAT.format(type.toDouble(e.getValue()))).append(" ");
                    b.append("|").append(e.getKey()).append(" ").append(VALUE_FORMAT.format(type.toDouble(e.getValue()))).append(" ");
                }
            } else {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    //b.append(e.getKey()).append("=").append(type.getDataType().toString(e.getValue())).append(" ");
                    b.append("|").append(e.getKey()).append(" ").append(type.getDataType().toString(e.getValue())).append(" ");
                }
            }
        }
        return b.toString();
    }

    public static String format(double value) {
        return VALUE_FORMAT.format(value);
    }

    public static double getPrediction(LossFunction lossFunction, double predictedValue) {
        return lossFunction == LossFunction.LOGISTIC ? sigmoid(predictedValue) : predictedValue;
    }

    public static double sigmoid(double x) {
        return 1 / (1 + Math.exp(-x));
    }
}
