package com.company.vw;

import com.company.VowpalWabbitRunner;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import com.company.Predictor;
import com.company.commons.Exceptions;
import com.company.commons.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Uses native-binary model file from vowpal wabbit and
 * calls vw binary. Since each prediction implies external process call
 * which is pretty expensive this class could be used only for debug purposes
 */
public class NativeVowpalWabbit implements Predictor {
    private static final Logger LOG = LoggerFactory.getLogger(NativeVowpalWabbit.class);
    public static final Charset VW_CHARSET = Charset.forName("UTF-8");
    private String vwBinary;
    private File binaryModelFile;
    private File dictionaryFile;
    private FeatureDictionary dictionary;
    private HashMap<String,Character> featureNamespaces = new HashMap<>();
    private boolean useNamespaces;
    private BufferedWriter tmpPredictWriter;

    public NativeVowpalWabbit(String vwBinary, File binaryModelFile, File dictionaryFile, HashMap<String, Character> featureNamespaces, boolean useNamespaces) {
        this.vwBinary = vwBinary;
        this.binaryModelFile = binaryModelFile;
        this.dictionaryFile = dictionaryFile;
        this.featureNamespaces = featureNamespaces;
        this.useNamespaces = useNamespaces;
        try{
            this.tmpPredictWriter = new BufferedWriter(new FileWriter(dictionaryFile.getAbsolutePath().substring(0, dictionaryFile.getAbsolutePath().lastIndexOf("/") + 1) + "tmpInput.txt"));
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
        if (dictionaryFile != null) {
            try (Reader r = new FileReader(dictionaryFile)) {
                dictionary = new FeatureDictionary();
                dictionary.read(r);
            } catch (Exception e) {
                throw Exceptions.wrap(e);
            }
        }
    }

    @Override
    public Prediction predict(FeatureVector key) {

        File tmpOut = null;
        File tmpIn = null;
        try {
            List<String> args = Lists.newArrayList(vwBinary);
            //args.addAll(Arrays.asList("--loss_function", lossFunction.name().toLowerCase()));
            //args.addAll(Arrays.asList("--readable_model", new File(readableModelFile).getAbsolutePath()));
            args.addAll(Arrays.asList("-i", binaryModelFile.getAbsolutePath()));
            args.add("-t");
            tmpOut = File.createTempFile(Util.newIdStr(), Util.newIdStr());
            tmpIn = File.createTempFile(Util.newIdStr(), Util.newIdStr());
            args.addAll(Arrays.asList("-r", tmpOut.getAbsolutePath()));
            args.addAll(Arrays.asList("-d", tmpIn.getAbsolutePath()));
            ProcessBuilder processBuilder = new ProcessBuilder(args);
            String line = dictionary == null ? VWHelper.toLine(key, 1.0, 1.0) : VWHelper.toLine(dictionary, featureNamespaces, key, 1.0, 1.0, useNamespaces);
            Files.write(line, tmpIn, VW_CHARSET);
            processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
            processBuilder.redirectErrorStream(true);
            processBuilder.redirectOutput(ProcessBuilder.Redirect.PIPE);
            processBuilder.redirectError();
            Process process = processBuilder.start();
            process.getOutputStream().write((line + "\n").getBytes());
            int res = process.waitFor();
            byte[] outputBytes = ByteStreams.toByteArray(process.getInputStream());
            if (res != 0) {
                LOG.info("Running: " + Joiner.on(" ").join(args) + " on '" + line + "'");
                LOG.info("Wabbit log:\n" + new String(outputBytes));
                throw new IllegalStateException("Wabbit failed with exit code " + res);
            }
            String outStr = Files.toString(tmpOut, VW_CHARSET);
            double val = Double.parseDouble(Splitter.on(" ").split(outStr).iterator().next());
            return Prediction.prob(VWHelper.getPrediction(LossFunction.LOGISTIC, val));
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            if (tmpOut != null) {
                tmpOut.delete();
            }
            if (tmpIn != null) {
                tmpOut.delete();
            }
        }
    }

    public void addFeatureVector(FeatureVector key){
        String line = dictionary == null ? VWHelper.toLine(key, 1.0, 1.0) : VWHelper.toLine(dictionary, featureNamespaces, key, 1.0, 1.0, useNamespaces);
        try{
            tmpPredictWriter.write(line + "\n");
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public void closeTmpWriter(){
        try{
            this.tmpPredictWriter.close();
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public List<Double> predictAll(){
        File tmpOut = null;
        String tmpBase = dictionaryFile.getAbsolutePath().substring(0, dictionaryFile.getAbsolutePath().lastIndexOf("/") + 1);
        List<String> argsPrediction = Lists.newArrayList(vwBinary);
        argsPrediction.add("-t");
        argsPrediction.addAll(Lists.newArrayList("-i", binaryModelFile.getAbsolutePath()));
        argsPrediction.addAll(Lists.newArrayList("-p", new File(tmpBase + "tmpPrediction.txt").getAbsolutePath()));

        ProcessBuilder processBuilder = new ProcessBuilder(argsPrediction);
        LOG.info("Running prediction: " + Joiner.on(" ").join(argsPrediction) + " < " + new File(tmpBase + "tmpInput.txt").getAbsolutePath());
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        try {
            tmpOut = File.createTempFile(Util.newIdStr(), Util.newIdStr());
            processBuilder.redirectOutput(ProcessBuilder.Redirect.to(tmpOut));
            processBuilder.redirectInput(ProcessBuilder.Redirect.from(new File(tmpBase + "tmpInput.txt")));

            Process process = processBuilder.start();
            LOG.info("Waiting for exit code prediction");
            int exitCode = process.waitFor();
            LOG.info("Exit code prediction: " + exitCode);
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code prediction: " + exitCode);
            }
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            if (tmpOut != null) {
                tmpOut.delete();
            }
        }
        return VowpalWabbitRunner.readPredictedLabels(tmpBase + "tmpPrediction.txt");
    }
}
