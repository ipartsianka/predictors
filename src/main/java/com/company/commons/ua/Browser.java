package com.company.commons.ua;

public enum Browser {
    FIREFOX,
    IE,
    OPERA,
    YANDEX,
    ANDROID_BROWSER,
    CHROME,
    SAFARI,
    OTHER
}
