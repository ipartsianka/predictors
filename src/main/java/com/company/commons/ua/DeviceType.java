package com.company.commons.ua;

public enum DeviceType {
    UNKNOWN,
    SMARTPHONE,
    TABLET,
    DESKTOP
}
