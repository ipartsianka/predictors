package com.company.commons.ua;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

import java.util.concurrent.TimeUnit;

public class UserAgentInfo {
    public static final int CACHE_SIZE = 2_500;
    /**
     * The parser is used for detection of version (browser and os)
     */
    private static final UserAgentStringParser parser = UADetectorServiceFactory.getCachingAndUpdatingParser();

    /**
     * We cache parsed agents in order to reduce parsing time
     */
    private static final Cache<String, ReadableUserAgent> cache = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).expireAfterAccess(1, TimeUnit.HOURS).build();

    private DeviceType deviceType;
    private OperatingSystem operatingSystem;
    private Browser browser;
    private boolean bot;
    private boolean enabledThirdPartyCookie;
    private String osVersion;
    private String browserVersion;

    public UserAgentInfo(DeviceType deviceType, OperatingSystem operatingSystem, Browser browser, boolean bot, boolean thirdPartyCookie) {
        this.deviceType = deviceType;
        this.operatingSystem = operatingSystem;
        this.browser = browser;
        this.bot = bot;
        this.enabledThirdPartyCookie = thirdPartyCookie;
    }

    private static ReadableUserAgent parseWithVersion(String ua) {
        ReadableUserAgent agent = cache.getIfPresent(ua);
        if (agent == null) {
            agent = parser.parse(ua);
            cache.put(ua, agent);
        }
        return agent;
    }

    /**
     * Parsed user agent and gets data like browser, os and etc. Optionally detects version
     * as well
     * @param userAgent user agent
     * @param detectVersions if versions (os, browser) should be detected. May require additional CPU
     * @return user agent info
     */
    public static UserAgentInfo parse(String userAgent, boolean detectVersions) {
        boolean bot = false;
        if (userAgent == null) {
            return new UserAgentInfo(DeviceType.UNKNOWN, OperatingSystem.OTHER, Browser.OTHER, false, true);
        }
        Browser browser = Browser.OTHER;
        DeviceType deviceType = DeviceType.UNKNOWN;
        OperatingSystem operatingSystem = OperatingSystem.OTHER;
        boolean enabledThirdPartyCookie = true;

        if (userAgent.contains("YaBrowser")) {
            browser = Browser.YANDEX;
        } else if (userAgent.contains("Chrome")) {
            browser = Browser.CHROME;
        } else if (userAgent.contains("Safari")) {
            browser = Browser.SAFARI;
            enabledThirdPartyCookie = false;
        } else if (userAgent.contains("Firefox")) {
            browser = Browser.FIREFOX;
        } else if (userAgent.contains("MSIE") || userAgent.contains("IEMobile") || userAgent.contains("Trident")) {
            browser = Browser.IE;
        } else if (userAgent.contains("Opera")) {
            browser = Browser.OPERA;
        }
        if (userAgent.contains("Android")) {
            operatingSystem = OperatingSystem.ANDROID;
            deviceType = DeviceType.SMARTPHONE;
            if (browser == Browser.SAFARI) {
                browser = Browser.ANDROID_BROWSER;
            }
            enabledThirdPartyCookie = true;
        } else if (userAgent.contains("iPad")) {
            operatingSystem = OperatingSystem.IOS;
            deviceType = DeviceType.TABLET;
        } else if (userAgent.contains("iPhone")) {
            operatingSystem = OperatingSystem.IOS;
            deviceType = DeviceType.SMARTPHONE;
        } else if (userAgent.contains("Linux")) {
            operatingSystem = OperatingSystem.LINUX;
            deviceType = DeviceType.DESKTOP;
        } else if (userAgent.contains("Windows Phone")) {
            operatingSystem = OperatingSystem.WINDOWS_PHONE;
            deviceType = DeviceType.SMARTPHONE;
        } else if (userAgent.contains("Windows NT")) {
            operatingSystem = OperatingSystem.WINDOWS;
            deviceType = DeviceType.DESKTOP;
        } else if (userAgent.contains("Mac OS X")) {
            operatingSystem = OperatingSystem.MACOS;
            deviceType = DeviceType.DESKTOP;
        }
        if (userAgent.contains("Googlebot")
                || userAgent.contains("AdsBot-Google")
                || userAgent.contains("Mediapartners-Google")
                || userAgent.contains("YandexBot") || userAgent.contains("adbeat.com")
                ) {
            bot = true;
            enabledThirdPartyCookie = false;
        }
        UserAgentInfo info = new UserAgentInfo(deviceType, operatingSystem, browser, bot, enabledThirdPartyCookie);
        if (detectVersions) {
            ReadableUserAgent parsed = parseWithVersion(userAgent);
            info.osVersion = parsed.getOperatingSystem().getVersionNumber().toVersionString();
            info.browserVersion = parsed.getVersionNumber().toVersionString();
        }
        return info;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    public Browser getBrowser() {
        return browser;
    }

    public boolean isEnabledThirdPartyCookie(){
        return enabledThirdPartyCookie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAgentInfo that = (UserAgentInfo) o;

        if (bot != that.bot) return false;
        if (enabledThirdPartyCookie != that.enabledThirdPartyCookie) return false;
        if (browser != that.browser) return false;
        if (deviceType != that.deviceType) return false;
        if (operatingSystem != that.operatingSystem) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deviceType.hashCode();
        result = 31 * result + operatingSystem.hashCode();
        result = 31 * result + browser.hashCode();
        result = 31 * result + (bot ? 1 : 0);
        result = 31 * result + (enabledThirdPartyCookie ? 1 : 0);
        return result;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public boolean isBot() {
        return bot;
    }
}
