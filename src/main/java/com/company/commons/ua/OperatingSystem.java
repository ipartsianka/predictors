package com.company.commons.ua;

public enum OperatingSystem {
    IOS,
    ANDROID,
    WINDOWS,
    LINUX,
    MACOS,
    OTHER,
    WINDOWS_PHONE
}
