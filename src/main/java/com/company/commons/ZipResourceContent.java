package com.company.commons;

import com.google.common.base.Preconditions;
import org.joda.time.DateTime;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipResourceContent extends ResourceContent {
    public ZipResourceContent(DateTime lastModiefied, ZipInputStream stream) {
        ZipEntry entry;
        try {
            entry = stream.getNextEntry();
        } catch (IOException e) {
            throw new RuntimeException("Can't read entry from ZIP stream", e);
        }
        Preconditions.checkNotNull(entry, "No entries found in ZIP archive.");
        this.lastModiefied = lastModiefied;
        this.stream = new BufferedInputStream(stream);
    }
}
