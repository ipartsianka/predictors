package com.company.commons;

import com.google.common.base.Preconditions;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class JsonUtil {
    private JsonUtil() {
    }


    public static JsonNode getNodeStrict(JsonNode parent, String name, String context) {
        final JsonNode node = parent.get(name);
        Preconditions.checkState(node != null, "Syntax error, JSON node '%s' should present in %s", name, context);
        return node;
    }

    public static String getTextStrict(JsonNode parent, String name, String context) {
        return getNodeStrict(parent, name, context).asText();
    }

    public static ObjectMapper createJsonReader() {
        ObjectMapper m = new ObjectMapper();
        m.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        m.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        return m;
    }

    public static Iterable<String> fields(final JsonNode node) {
        return new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                return node.getFieldNames();
            }
        };
    }

    public static List<JsonNode> toList(JsonNode node) {
        if (node == null) {
            return Collections.emptyList();
        } else if (node.isArray()) {
            List<JsonNode> res = new ArrayList<JsonNode>();
            for (JsonNode n : node) {
                res.add(n);
            }
            return res;
        } else {
            return Collections.singletonList(node);
        }
    }

    public static List<String> toStringList(JsonNode node) {
        List<JsonNode> nodes = toList(node);
        ArrayList<String> res = new ArrayList<String>(nodes.size());
        for (JsonNode n : nodes) {
            res.add(n.asText());
        }
        return res;
    }

    public static String getText(JsonNode n, String field, String defaultValue) {
        if (n.has(field)) {
            return n.get(field).asText();
        } else {
            return defaultValue;
        }
    }

    public static Integer getInt(JsonNode n, String field, Integer defaultValue){
        if (n.has(field)) {
            return n.get(field).asInt();
        } else {
            return defaultValue;
        }
    }

    public static void main(String[] args) {
        System.out.println(URLEncoder.encode("H4sIAAAAAAAAAGOw/2xkPn3SinkMDPwZJSUFVvr6JanFJXrJ+bkMEMAIIlyBJEt6fn46A0I9DDCHBrswMAiAtMUnF6UmlmSWpcabWTKwGxsYVBiZGjAwMgJ1c/uGVJgWmHvm5DkHO0wPD/fd9dIApFudgQEAlqqxMYQAAAA="));
    }
}
