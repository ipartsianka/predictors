package com.company.commons;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.Charset;

public abstract class ResourceLoader {
    public static final Joiner ON = Joiner.on('\n');
    private static final Logger LOG = LoggerFactory.getLogger(ResourceLoader.class);

    public static final ResourceLoader EMPTY = new ResourceLoader() {
        @Override
        public ResourceContent load() {
            return new ResourceContent(new DateTime(0), new ByteArrayInputStream(new byte[]{}));
        }

        @Override
        public ResourceContent loadIfModified(DateTime dateTime) {
            if (dateTime == null) {
                return load();
            } else {
                return null;
            }
        }

        @Override
        public String getLocation() {
            return "EMPTY";
        }
    };

    /**
     * @param location location of resource
     * @return resource loader
     */
    public static ResourceLoader get(String location) {
        if (location.startsWith("http://") || location.startsWith("https://")) {
            return new HttpResourceLoader(location);
        } else {
            File file = Util.getLocation(location);
            if (!file.exists() && location.startsWith("classpath:")) {
                location = location.replace("classpath:", "");
                return new ClasspathLoader(location);
            }
            Preconditions.checkState(file.exists(), "Location %s was translated to absolute path %s which doesn't exist", location, file.getAbsolutePath());
            Preconditions.checkState(file.isFile(), "Location %s was translated to absolute path %s which isn't a file", location, file.getAbsolutePath());
            return new FileResourceLoader(file);
        }
    }

    public abstract ResourceContent load();

    public abstract ResourceContent loadIfModified(DateTime dateTime);

    public abstract String getLocation();

    public String toString() {
        return getLocation();
    }

    private static class FileResourceLoader extends ResourceLoader {
        public static final Charset UTF8 = Charset.forName("UTF-8");
        private final File file;

        public FileResourceLoader(File file) {
            this.file = file;
        }

        @Override
        public ResourceContent load() {
            return loadIfModified(null);
        }

        @Override
        public ResourceContent loadIfModified(DateTime dateTime) {
            return ResourceLoaderUtil.loadFileIfModified(file, dateTime);
        }

        @Override
        public String getLocation() {
            return file.getAbsolutePath();
        }
    }

    private static class HttpResourceLoader extends ResourceLoader {
        private final String location;

        public HttpResourceLoader(String location) {
            this.location = location;
        }

        @Override
        public ResourceContent load() {
            return loadIfModified(null);
        }

        @Override
        public ResourceContent loadIfModified(DateTime time) {
            return ResourceLoaderUtil.loadUriIfModified(location, time);
        }

        @Override
        public String getLocation() {
            return location;
        }
    }

    private static class ClasspathLoader extends ResourceLoader {
        private final String location;

        public ClasspathLoader(String location) {
            this.location = location;
        }

        @Override
        public ResourceContent load() {
            return loadIfModified(null);
        }

        @Override
        public ResourceContent loadIfModified(DateTime dateTime) {
            return ResourceLoaderUtil.loadClasspathResIfModified(location, dateTime);
        }

        @Override
        public String getLocation() {
            return "classpath:" + location;
        }
    }
}
