package com.company.commons;

import com.google.common.base.*;
import com.google.common.collect.Iterables;
import com.google.common.io.Files;
import com.google.common.net.InetAddresses;
import com.google.common.net.InternetDomainName;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import static com.google.common.io.ByteStreams.toByteArray;

public class Util {
    private static final Logger LOG = LoggerFactory.getLogger(Util.class);
    public static final Splitter QUERY_STRING_SPLITTER = Splitter.on("&");
    public static final Joiner QUERY_STRING_JOINER = Joiner.on("&");

    private static final char negativeSign = 'u';
    public static final char[] codeSymbols = "KLe7PgsM6805xzklSJyaThd3AZ1DwiVfCNXb4HWBoc2tYG9REqUnpOvrIFQjm".toCharArray();
    public static final Joiner LINES_JOINER = Joiner.on("\n");
    public static final Charset UTF8 = Charset.forName("UTF-8");
    public static HashMap<Character, Integer> digitIndex = new HashMap<Character, Integer>();
    public static final SecureRandom SECURE_RANDOM = new SecureRandom();
    public static final String CLASSPATH_PREFIX = "classpath:";

    static {
        for (int i = 0, codeSymbolsLength = Util.codeSymbols.length; i < codeSymbolsLength; i++) {
            char c = Util.codeSymbols[i];
            Util.digitIndex.put(c, i);
        }
    }

    public static final char[] HEX_ARRAY = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private Util() {
    }

    public static String ratePerSecond(double operations, long millis) {
        if (millis == 0) {
            return "N/A";
        }
        return new DecimalFormat("#.##").format((double) operations * 1000 / millis);
    }

    public static String msToString(long millis) {
        if (millis < 1000) {
            return String.format("%sms", millis);
        } else if (millis >= 1000 && millis < 1000 * 60) {
            return String.format("%ss %sms (%sms)", millis / 1000, millis % 1000, millis);
        } else {
            return String.format("%sm %ss %sms (%sms)", millis / (1000 * 60), (millis % (1000 * 60)) / 1000, (millis % (1000 * 60)) % 1000, millis);
        }
    }

    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw Exceptions.wrap(e);
        }
    }

    public static String urlDecode(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw Exceptions.wrap(e);
        }
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes, 0, 8);
        buffer.flip();//need flip
        return buffer.getLong();
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
    public static void ensureDir(String dir) {
        ensureDir(new File(dir));
    }
    /**
     * Ensures that dir exists
     * @param dir directory
     */
    public static void ensureDir(File dir) {
        if (dir.exists()) {
            Preconditions.checkState(dir.isDirectory(), "File %s is not a directory", dir.getAbsolutePath());
        } else {
            Preconditions.checkState(dir.mkdirs(), "Can't create directory %s", dir.getAbsolutePath());
        }
    }

    public static <T> int nullSafeCompare(T c1, T c2, Comparator<T> cmp) {
        if (c1 == null && c2 == null) {
            return 0;
        } else if (c1 == null) {
            return -1;
        } else if (c2 == null) {
            return 1;
        } else {
            return cmp.compare(c1, c2);
        }
    }

    public static <T> Comparator<T> nullSafeComparator(final T c1, final T c2, final Comparator<T> cmp) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return nullSafeCompare(c1, c2, cmp);
            }
        };
    }

    public static <T extends Comparable<T>> Comparator<T> comparableComparator() {
        return new Comparator<T>() {
            @Override
            public int compare(T c1, T c2) {
                return nullSafeCompare(c1, c2);
            }
        };
    }

    /**
     * Gets a real location of resource if it starts from classpath:/path/ or
     * just gives a location of file
     *
     * @param location location (classpath:/path or /path)
     * @return real fs location of resource
     */
    public static File getLocation(String location) {
        return getLocation(location, Util.class);
    }

    /**
     * Gets a real location of resource if it starts from classpath:/path/ or
     * just gives a location of file
     *
     * @param cls      class needed for resolving classpath location
     * @param location location (classpath:/path or /path)
     * @return real fs location of resource
     */
    public static File getLocation(String location, Class cls) {
        if (location.startsWith(CLASSPATH_PREFIX)) {
            location = location.replace(CLASSPATH_PREFIX, "");
            URL resource = cls.getResource(location);
            Preconditions.checkState(resource != null, "Unable to find classpath resource %s (relative to class %s)", location, cls.getName());
            String file = resource.getFile();
            return new File(file);
        } else {
            return new File(location);
        }
    }

    public static Path getPath(String location) {
        return getLocation(location).toPath();
    }


    public static String getDomainName(String url) {
        try {
            url = URLDecoder.decode(url, "utf-8");
            url = fixUrl(url);
            URL netUrl = new URL(url);
            return getDomainName(netUrl);
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public static String fixUrl(String url) {
        if (!url.startsWith("http") && !url.startsWith("https")) {
            url = "http://" + url;
        }
        return url;
    }

    public static String getDomainName(URL netUrl) {
        String host = netUrl.getHost();
        return fixDomainName(host);
    }

    public static String fixDomainName(String host) {
        int startIndex = 0;
        int endIndex = host.length() - 1;
        if (host.startsWith("www.")) {
            startIndex += "www.".length();
        }

        while (endIndex >= 0 && host.charAt(endIndex) == '.') {
            endIndex--;
        }
        while (startIndex < host.length() && host.charAt(startIndex) == '.') {
            startIndex++;
        }
        if (startIndex >= endIndex) {
            return "";
        }
        return host.substring(startIndex, endIndex + 1).toLowerCase();
    }

    public static String makeArtificialDomain(String publisherId, String publisherWebsiteId){
        StringBuilder b = new StringBuilder("anonymous");
        if (!Strings.isNullOrEmpty(publisherId)) {
            b.append("_").append(publisherId);
        }
        if (!Strings.isNullOrEmpty(publisherWebsiteId)) {
            b.append("_").append(publisherWebsiteId);
        }
        return b.toString();
    }

    public static int nullSafeCompare(Comparable c1, Comparable c2) {
        return nullSafeCompare(c1, c2, true);
    }

    public static int nullSafeCompare(Comparable c1, Comparable c2, boolean nullIsLesserThenOthers) {
        if (c1 == null && c2 == null) {
            return 0;
        } else if (c1 == null) {
            return nullIsLesserThenOthers ? -1 : 1;
        } else if (c2 == null) {
            return 1;
        } else {
            return c1.compareTo(c2);
        }
    }

    public static String toQueryString(Map<String, String> data) {
        Collection<Map.Entry<String, String>> entries = data.entrySet();
        return toQueryString(entries);
    }

    public static String toQueryString(Collection<Map.Entry<String, String>> entries) {
        return QUERY_STRING_JOINER.join(Iterables.transform(entries, new Function<Map.Entry<String, String>, Object>() {
            @Override
            public Object apply(@Nullable Map.Entry<String, String> input) {
                try {
                    return input.getKey() + "=" + URLEncoder.encode(input.getValue(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    throw Exceptions.wrap(e);
                }
            }
        }));
    }

    public static Map<String, String> parseQueryString(String queryString, boolean ignoreDecodingExceptions) {
        Map<String, String> data = new HashMap<String, String>();
        if (queryString != null) {
            for (String decl : QUERY_STRING_SPLITTER.split(queryString)) {
                int idx = decl.indexOf("=");
                if (idx >= 0) {
                    String var = decl.substring(0, idx);
                    try {
                        String value = URLDecoder.decode(decl.substring(idx + 1), "UTF-8");
                        data.put(var, value);
                    } catch (Exception e) {
                        if (!ignoreDecodingExceptions) {
                            throw Exceptions.wrap(e);
                        } else {
                            //ignore
                        }
                    }
                }
            }
        }
        return data;
    }

    public static String get(HttpServletRequest request, String field, String defaultValue) {
        String str = request.getParameter(field);
        if (str == null) {
            return defaultValue;
        }
        return str;
    }

    public static String get(HttpServletRequest request, String field) {
        String str = request.getParameter(field);
        if (str == null) {
            throw new IllegalStateException("Request parameter " + field + " is missing");
        }
        return str;
    }

    public static String getLocalHostname() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            return "localhost";
        }
    }

    public static String getShortLocalHostname() {
        return Splitter.on(".").split(Util.getLocalHostname()).iterator().next();
    }

    public static DateTime roundToDayStart(DateTime date) {
        return date.withMillisOfDay(0);
    }

    public static DateTime roundToHourStart(DateTime date) {
        return date.withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }

    public static DateTime roundToMonthStart(DateTime date) {
        return date.withMillisOfDay(0).withDayOfMonth(1);
    }

    private static final Pattern DEFAULT_PROPERTY_VAL_PATTERN = Pattern.compile("\\$\\{(.+?):(.+?)\\}");

    public static String placeholdProperties(String where, Properties properties) {
        for (Object key : properties.keySet()) {
            String value = String.valueOf(properties.get(key));
            String token = "${" + key + "}";
            where = where.replace(token, value);
        }
        Matcher m = DEFAULT_PROPERTY_VAL_PATTERN.matcher(where);
        if (m.find()) {
            where = m.replaceAll("$2");
        }
        return where;
    }


    private static final Pattern FRIENDS_PAGE_PATTERN = Pattern.compile("//([^\\.]+)\\.livejournal.com/friends");
    private static final Pattern FRIENDSTIMES_PAGE_PATTERN = Pattern.compile("//([^\\.]+)\\.livejournal.com/friendstimes");

    public static String extractLiveJournalUsername(String url) {
        Matcher friends = FRIENDS_PAGE_PATTERN.matcher(url);
        if (friends.find()) {
            return friends.group(1);
        }
        Matcher friendstimes = FRIENDSTIMES_PAGE_PATTERN.matcher(url);
        if (friendstimes.matches()) {
            return friendstimes.group(1);
        }
        return null;
    }

    public static long newId() {
        return Math.abs(SECURE_RANDOM.nextLong());
    }

    public static Long parseId(String s) {
        Preconditions.checkState(s != null, "Can't parse null string");
        if (s.isEmpty()) {
            return null;
        }
        int radix = codeSymbols.length;
        long result = 0;
        boolean negative = false;
        int i = 0, max = s.length();
        long limit;
        long multmin;
        int digit;

        if (max > 0) {
            if (s.charAt(0) == negativeSign) {
                negative = true;
                limit = Long.MIN_VALUE;
                i++;
            } else {
                limit = -Long.MAX_VALUE;
            }
            multmin = limit / radix;
            if (i < max) {
                digit = digitIndex.get(s.charAt(i++));
                if (digit < 0) {
                    throw new IllegalStateException("Invalid digit " + digit);
                } else {
                    result = -digit;
                }
            }
            while (i < max) {
                char nextChar = s.charAt(i++);
                digit = digitIndex.get(nextChar);
                if (digit < 0) {
                    throw new IllegalStateException("Bad character " + nextChar);
                }
                if (result < multmin) {
                    throw new IllegalStateException("Bad character " + nextChar);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw new IllegalStateException("Bad character " + nextChar);
                }
                result -= digit;
            }
        } else {
            throw new IllegalStateException("Empty string");
        }
        if (negative) {
            if (i > 1) {
                return result;
            } else {
                throw new IllegalStateException("Illegal string");
            }
        } else {
            return -result;
        }
    }

    public static String idToString(Long object) {
        if (object == null) {
            return "";
        }
        int radix = codeSymbols.length;
        char[] buf = new char[12];
        int charPos = buf.length - 1;
        boolean negative = (object < 0);
        long l = negative ? object : -object;
        while (l <= -radix) {
            buf[charPos--] = codeSymbols[(int) (-(l % radix))];
            l = l / radix;
        }
        buf[charPos] = codeSymbols[(int) (-l)];
        if (negative) {
            buf[--charPos] = negativeSign;
        }
        return new String(buf, charPos, (buf.length - charPos));
    }

    public static String newIdStr() {
        return idToString(newId());
    }

    public static String trim(String value, char first, char last) {
        int len = value.length();
        int st = 0;
        while ((st < len) && (value.charAt(st) == first)) {
            st++;
        }
        while ((st < len) && (value.charAt(len - 1) == last)) {
            len--;
        }
        return ((st > 0) || (len < value.length())) ? value.substring(st, len) : value;
    }

    public static String trim(String str, char c) {
        return trim(str, c, c);
    }

    public static byte[] readResource(String location) {
        try {
            if (location.startsWith(CLASSPATH_PREFIX)) {
                location = location.replace(CLASSPATH_PREFIX, "");
            }
            InputStream inputStream = Util.class.getResourceAsStream(location);
            Preconditions.checkState(inputStream != null, "Unable to find classpath resource %s (relative to class %s)", location, Util.class.getName());
            return toByteArray(inputStream);
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    public static String readResourceAsString(String location, String encoding) {
        return new String(readResource(location), Charset.forName(encoding));
    }

    public static String readResourceAsString(String location) {
        return new String(readResource(location), Charset.defaultCharset());
    }

    public static String escapeJS(String js) {
        return js.replace("\\", "\\\\").replace("'", "\\'").replace("\n", "\\n");
    }

    public static String getTLD(String urlStr) {
        try {
            URL url = new URL(fixUrl(urlStr));
            return fixDomainName(getTLD(url));
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public static int countOccurrencesOf(String s, char c) {
        int occurences = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                occurences++;
            }
        }
        return occurences;
    }

    public static String getTLD(URL url) {
        String host = url.getHost();
        int dots = countOccurrencesOf(host, '.');
        if (dots <= 1 || isIp(host)) {
            return host;
        }
        return InternetDomainName.from(host).topPrivateDomain().name();
    }

    private static boolean isIp(String host) {
        return InetAddresses.isInetAddress(host);
    }

    public static String readFile(String file, Charset charset) {
        return readFile(new File(file), charset);
    }

    public static String readFile(String file) {
        return readFile(new File(file));
    }

    public static String readFile(File file) {
        return readFile(file, UTF8);
    }

    public static String readFile(File file, Charset charset) {
        try {
            return LINES_JOINER.join(Files.readLines(file, charset));
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    public static boolean sameDay(DateTime d1, DateTime d2) {
        return d1.getYear() == d2.getYear() && d2.getMonthOfYear() == d1.getMonthOfYear() && d2.getDayOfMonth() == d1.getDayOfMonth();
    }


    /**
     * Opens file with support of gzipped files. If file is gzipped will be judged by name
     *
     * @param file file
     * @return stream
     */
    public static InputStream open(File file) {
        try {
            return file.getName().endsWith(".gz") ?
                    new GZIPInputStream(new BufferedInputStream(new FileInputStream(file), 4096)) :
                    new FileInputStream(file);
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public static InputStream open(String file) {
        return open(new File(file));
    }

    public static InputStream openLocation(String location) {
        return open(getLocation(location));
    }

    public static InetAddress getInetAddress(String ip) {
        InetAddress inetAddres;
        try {
            inetAddres = InetAddress.getByName(ip);
        } catch (Exception e) {
            //don't log - too many of them
            inetAddres = null;
        }
        return inetAddres;
    }

    public static boolean nullSafeEquals(Object o1, Object o2) {
        if (o1 == null && o2 == null) {
            return true;
        } else if (o1 != null) {
            return o1.equals(o2);
        } else {
            return o2.equals(o1);
        }
    }

    public static int findFreePort() {
        try {
            ServerSocket server = new ServerSocket(0);
            int port = server.getLocalPort();
            server.close();
            return port;
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public static byte[] shortToBytes(short s) {
        return new byte[]{(byte) (s & 0x00FF), (byte) ((s & 0xFF00) >> 8)};
    }

    public static DateTime roundToDayEnd(DateTime dateTime) {
        return roundToDayStart(dateTime).plusDays(1).minusMillis(1);
    }

    public static DecimalFormat decimalFormat(String pattern) {
        return new DecimalFormat(pattern, DecimalFormatSymbols.getInstance(Locale.US));
    }
    public static DecimalFormat decimalFormat(int digits) {
        return decimalFormat("#." + Strings.repeat("#", digits));
    }

    public static String decimalFormat(double rate, int digits) {
        return decimalFormat(digits).format(rate);
    }

    public static String memoryReport() {
        long freeMb = Runtime.getRuntime().freeMemory() / 1000000;
        long maxMemory = Runtime.getRuntime().maxMemory() / 1000000;
        return String.format("%s/%smb", freeMb, maxMemory);
    }

    public static Dimension getImageDimension(File file) {
        ImageInputStream in = null;
        try {
            in = ImageIO.createImageInputStream(file);
            Preconditions.checkNotNull(in, "File not found or is not an image " + file.getAbsolutePath());
            final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
            if (readers.hasNext()) {
                ImageReader reader = readers.next();
                try {
                    reader.setInput(in);
                    return new Dimension(reader.getWidth(0), reader.getHeight(0));
                } finally {
                    reader.dispose();
                }
            } else {
                throw new RuntimeException("No appropriate reader found for image " + file.getAbsolutePath());
            }
        } catch (IOException e) {
            throw new RuntimeException("IOException occured due to reading image file " + file.getAbsolutePath(), e);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }
}
