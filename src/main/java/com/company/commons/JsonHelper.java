package com.company.commons;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

public class JsonHelper {
    private JsonHelper() {
    }

    public static JsonNode readFile(String file) {
        return readFile(new File(file));
    }

    public static JsonNode readFile(File file) {
        try (Reader reader = new InputStreamReader(new FileInputStream(file), "UTF-8")) {
            return new ObjectMapper(new JsonFactory()).readTree(reader);
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public static JsonNode read(String json) {
        try {
            return new ObjectMapper(new JsonFactory()).readTree(json);
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public static String getText(JsonNode parent, String name, String defaultValue) {
        final JsonNode node = parent.get(name);
        return node != null ? node.asText() : defaultValue;
    }

    public static String getText(JsonNode parent, String name) {
        return getText(parent, name, null);
    }

    public static JsonNode getNodeStrict(JsonNode parent, String name) {
        return getNodeStrict(parent, name, "unknown");
    }

    public static JsonNode getNodeStrict(JsonNode parent, String name, String context, Object... params) {
        final JsonNode node = parent.get(name);
        Preconditions.checkState(node != null, "Syntax error, JSON node '%s' should present in %s", name, String.format(context, params));
        return node;
    }

    public static String getTextStrict(JsonNode parent, String name, String context, Object... params) {
        return getNodeStrict(parent, name, context, params).asText();
    }

    public static ObjectMapper createJsonReader() {
        ObjectMapper m = new ObjectMapper();
        m.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        m.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        return m;
    }

    public static Iterable<String> fields(final JsonNode node) {
        if (node.isArray()) {
            return new Iterable<String>() {
                @Override
                public Iterator<String> iterator() {
                    return node.getFieldNames();
                }
            };
        } else {
            return Collections.emptyList();
        }
    }

    public static List<String> toStringList(JsonNode node) {
        return Lists.newArrayList(Iterables.transform(toList(node), new Function<JsonNode, String>() {
            @Override
            public String apply(@Nullable JsonNode input) {
                return input.asText();
            }
        }));
    }

    public static List<JsonNode> toList(JsonNode node) {
        if (node == null) {
            return Collections.emptyList();
        } else if (node.isArray()) {
            List<JsonNode> res = new ArrayList<JsonNode>();
            for (JsonNode n : node) {
                res.add(n);
            }
            return res;
        } else {
            return Collections.singletonList(node);
        }
    }

    public static double getDouble(JsonNode node, String fieldName, double defaultValue) {
        JsonNode child = node.get(fieldName);
        return child == null ? defaultValue : child.asDouble();
    }

    public static int getInteger(JsonNode node, String fieldName, int defaultValue) {
        JsonNode child = node.get(fieldName);
        return child == null ? defaultValue : child.asInt();
    }

    public static Number getFieldNumber(JsonNode jsonNode, String fieldName){
        try {
            return NumberFormat.getInstance(Locale.US).parse(checkFieldExistance(jsonNode, fieldName).asText());
        } catch (ParseException e) {
            throw new IllegalStateException("value of field ["+fieldName+"] isn`t number", e);
        }
    }

    public static JsonNode checkFieldExistance(JsonNode jsonNode, String fieldName) {
        Preconditions.checkState(jsonNode.has(fieldName), "json node of ProductFeed doesn`t have '"+fieldName+"' field");
        return jsonNode.get(fieldName);
    }
}