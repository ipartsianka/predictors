package com.company.commons;

import com.google.common.base.Preconditions;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.util.DateParseException;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipInputStream;

public class ResourceLoaderUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceLoaderUtil.class);

    /**
     * Returns resource if was modified after dateTime, null otherwise.
     * @param location location string, not null
     * @param dateTime dateTime object to check modification date, use null to disable modification check
     * @return ResourceContent object
     */
    public static ResourceContent loadIfModified(String location, DateTime dateTime) {
        if (location.startsWith("http://") || location.startsWith("https://")) {
            return loadUriIfModified(location, dateTime);
        } else {
            File file = Util.getLocation(location);
            if (!file.exists() && location.startsWith("classpath:")) {
                location = location.replace("classpath:", "");
                return loadClasspathResIfModified(location, dateTime);
            }
            Preconditions.checkState(file.exists(), "Location %s was translated to absolute path %s which doesn't exist", location, file.getAbsolutePath());
            Preconditions.checkState(file.isFile(), "Location %s was translated to absolute path %s which isn't a file", location, file.getAbsolutePath());
            return loadFileIfModified(file, dateTime);
        }
    }

    protected static ResourceContent loadFileIfModified(File file, DateTime dateTime) {
        try {
            DateTime lastModified = new DateTime(file.lastModified()).withZone(DateTimeZone.UTC);
            if (dateTime == null || lastModified.isAfter(dateTime)) {

                if (FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("zip")) {
                    return new ZipResourceContent(lastModified, new ZipInputStream(new FileInputStream(file)));
                } else {
                    return new ResourceContent(lastModified, new FileInputStream(file));
                }
            } else {
                return null;
            }
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    protected static ResourceContent loadUriIfModified(String uri, DateTime time) {
        HttpClient client = new HttpClient();
        client.getParams().setParameter(HttpClientParams.ALLOW_CIRCULAR_REDIRECTS, true);
        GetMethod method = new GetMethod(uri);
        if (time != null) {
            String dateStr = DateUtil.formatDate(time.toDate());
            method.addRequestHeader("If-Modified-Since", dateStr);
        }
        int status;
        try {
            status = client.executeMethod(method);
            if (status == 304) {
                return null;
            } else {
                if (status != 200) {
                    String response = method.getResponseBodyAsString();
                    throw new IllegalStateException("Invalid response code: " + status + ". Body: " + response);
                } else {
                    Header lastModifiedHeader = method.getResponseHeader("Last-Modified");
                    DateTime modified;
                    if (lastModifiedHeader != null) {
                        try {
                            modified = new DateTime(DateUtil.parseDate(lastModifiedHeader.getValue()));
                        } catch (DateParseException e) {
                            LOG.error("Can't parse Last-Modified header: " + lastModifiedHeader.getValue(), e);
                            modified = new DateTime();
                        }
                    } else {
                        modified = new DateTime();
                    }
                    ResourceContent content = null;
                    Header contentTypeHeader = method.getResponseHeader("Content-Type");
                    if (contentTypeHeader != null && contentTypeHeader.getValue().equals("application/zip")) {
                        content = new ZipResourceContent(modified.withZone(DateTimeZone.UTC), new ZipInputStream(method.getResponseBodyAsStream()));
                    } else {
                        content = new ResourceContent(modified.withZone(DateTimeZone.UTC), method.getResponseBodyAsStream());
                    }
                    if (method.getResponseCharSet() != null) {
                        content.setCharset(method.getResponseCharSet());
                    }
                    return content;
                }
            }
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    protected static ResourceContent loadClasspathResIfModified(String classpathLocation, DateTime dateTime) {
        File file = Util.getLocation(classpathLocation, ResourceLoaderUtil.class);
        DateTime lastModified = new DateTime(file.lastModified());
        if (dateTime == null || lastModified.isAfter(dateTime)) {
            if (FilenameUtils.getExtension(classpathLocation).equalsIgnoreCase("zip")) {
                return new ZipResourceContent(lastModified, new ZipInputStream(ResourceLoaderUtil.class.getResourceAsStream(classpathLocation)));
            } else {
                return new ResourceContent(lastModified, ResourceLoaderUtil.class.getResourceAsStream(classpathLocation));
            }
        }
        return null;
    }
}
