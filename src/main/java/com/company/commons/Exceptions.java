package com.company.commons;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Utility class for working with exceptions
 */
public class Exceptions {
    /**
     * Prevent instantiation
     */
    private Exceptions() {}

    /**
     * Wraps checked exception by runtime exception
     * @param e exception
     * @return exception to throw
     */
    public static RuntimeException wrap(Throwable e) {
        Exceptions.<RuntimeException>rethrow0(e);
        return new RuntimeException();
    }

    private static <T extends Throwable> void rethrow0(Throwable t) throws T {
        throw (T) t;
    }

    public static String getTrace(Throwable e) {
        StringWriter w = new StringWriter();
        e.printStackTrace(new PrintWriter(w));
        return w.toString();
    }


    public static String getMessage(Throwable ex) {
        if (ex.getMessage() != null) {
            return ex.getMessage();
        } else if (ex.getCause() != null) {
            return getMessage(ex.getCause());
        } else {
            return ex.getClass().getSimpleName();
        }
    }

    public static RuntimeException runtime(String pattern, Object ... args) {
        return new RuntimeException(String.format(pattern, args));
    }

    public static RuntimeException runtime(Throwable cause, String pattern, Object ... args) {
        return new RuntimeException(String.format(pattern, args), cause);
    }

    public static RuntimeException illegalState(String pattern, Object ... args) {
        return new IllegalStateException(String.format(pattern, args));
    }

    public static RuntimeException illegalState(Throwable cause, String pattern, Object ... args) {
        return new IllegalStateException(String.format(pattern, args), cause);
    }

    public static RuntimeException illegalArgument(String pattern, Object ... args) {
        return new IllegalArgumentException(String.format(pattern, args));
    }

    public static RuntimeException illegalArgument(Throwable cause, String pattern, Object ... args) {
        return new IllegalArgumentException(String.format(pattern, args), cause);
    }

    public static boolean hasInTrace(Throwable e, Class cls) {
        while (e != null) {
            if (e.getClass().equals(cls)) {
                return true;
            }
            e = e.getCause();
        }
        return false;
    }
}
