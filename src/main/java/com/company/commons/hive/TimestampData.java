package com.company.commons.hive;


import com.google.common.base.Strings;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.*;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.util.*;

public class TimestampData extends GenericUDF {
    ObjectInspector[] argOIs;
    private static final Map<String, Locale> COUNTRY_TO_LOCALE_MAP = new HashMap<> ();

    @Override
    public Object evaluate(DeferredObject[] args) throws HiveException {
        ArrayList<String> fields = new ArrayList<>();
        fields.addAll(Arrays.asList("", ""));
        if (args[0].get() == null || args.length != 2)
            return fields;

        fields.clear();
        String day = "";
        String hour = "";
        Locale locale = Locale.getDefault();
        Long ts = (Long) getJavaObject(args[0].get(), argOIs[0], new ArrayList<ObjectInspector.Category>());
        String country = (String) getJavaObject(args[1].get(), argOIs[1], new ArrayList<ObjectInspector.Category>());
        if (!(ts==null)&&!Strings.isNullOrEmpty(country)) {
            locale = Locale.forLanguageTag(country);
            //locale = getLocaleFromCountry(country.toUpperCase());
            if (locale != null) {
                TimeZone tZone = getTimeZoneForCountry(locale);
                LocalDateTime dt = new LocalDateTime(ts, DateTimeZone.forTimeZone(tZone));
                day = String.valueOf(dt.getDayOfWeek());
                hour = String.valueOf(dt.getHourOfDay());
            }
        }
        fields.add(day);
        fields.add(hour);
        return fields;
    }

    private Object getJavaObject(Object o, ObjectInspector oi, List<ObjectInspector.Category> categories) {
        if(categories != null) {
            categories.add(oi.getCategory());
        }
        if(oi.getCategory() == ObjectInspector.Category.LIST) {
            List<?> l = ((ListObjectInspector)oi).getList(o);
            List<Object> result = new ArrayList<>();
            ObjectInspector elemOI = ((ListObjectInspector)oi).getListElementObjectInspector();
            for(Object lo : l) {
                result.add(getJavaObject(lo, elemOI, categories));
            }
            return result;
        } else if (oi.getCategory() == ObjectInspector.Category.MAP) {
            Map<?,?> m = ((MapObjectInspector)oi).getMap(o);
            Map<String, String> result = new HashMap<>();
            ObjectInspector koi = ((MapObjectInspector)oi).getMapKeyObjectInspector();
            ObjectInspector voi = ((MapObjectInspector)oi).getMapValueObjectInspector();
            for(Map.Entry<?,?> e: m.entrySet()) {
                result.put((String)getJavaObject(e.getKey(), koi, null),
                        (String)getJavaObject(e.getValue(), voi, null));
            }
            return result;

        } else if (oi.getCategory() == ObjectInspector.Category.STRUCT) {
            List<Object> s = ((StructObjectInspector)oi).getStructFieldsDataAsList(o);
            List<? extends StructField> sf = ((StructObjectInspector)oi).getAllStructFieldRefs();
            List<Object> result = new ArrayList<>();
            for(int i = 0; i < s.size(); i++) {
                result.add(getJavaObject(s.get(i), sf.get(i).getFieldObjectInspector(), categories));
            }
            return result;
        } else if(oi.getCategory() == ObjectInspector.Category.PRIMITIVE) {
            return ((PrimitiveObjectInspector)oi).getPrimitiveJavaObject(o);
        }
        throw new RuntimeException("Unexpected error!");
    }

    @Override
    public String getDisplayString(String[] arg0) {
        return "string";
    }

    /* @Override
     public ObjectInspector initialize(ObjectInspector[] argOIs)
             throws UDFArgumentException {

         this.argOIs = argOIs;

         //return ObjectInspectorFactory.getReflectionObjectInspector(String.class,
                 //ObjectInspectorOptions.JAVA);
         return ObjectInspectorFactory.getStandardListObjectInspector(ObjectInspectorFactory.getReflectionObjectInspector(String.class,
                ObjectInspectorOptions.JAVA));
     }  */
    @Override
    public StructObjectInspector initialize(ObjectInspector[] args)
            throws UDFArgumentException {

        if (args.length != 2) {
            throw new UDFArgumentException("tokenize() takes exactly 2 arguments");
        }

        argOIs = args;

        Locale[] locales = Locale.getAvailableLocales();
        for(Locale l : locales) {
            COUNTRY_TO_LOCALE_MAP.put(l.getCountry (), l);
        };

        List<String> fieldNames = new ArrayList<>(2);
        List<ObjectInspector> fieldOIs = new ArrayList<>(2);
        fieldNames.add("day");
        fieldNames.add("hour");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        /*List<String> fieldNames1 = new ArrayList<>(1);
        List<ObjectInspector> fieldOIs1 = new ArrayList<ObjectInspector>(1);
        fieldNames1.add("segmentsitem");
        fieldOIs1.add(ObjectInspectorFactory.getStandardListObjectInspector(ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs)));
        */
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);
    }

    public static TimeZone getTimeZoneForCountry(Locale locale) {
        TimeZone t;
        try {
            String[] ids = com.ibm.icu.util.TimeZone.getAvailableIDs(locale.getCountry());
            t = TimeZone.getTimeZone(ids[0]);
        } catch (Exception e) {
            t = TimeZone.getDefault();
        }
        return t;
    }

    public static Locale getLocaleFromCountry(String country) {
        return COUNTRY_TO_LOCALE_MAP.get(country);
    }
}
