package com.company.commons.hive;

import com.company.commons.ua.UserAgentInfo;
import com.google.common.base.Strings;
import com.company.commons.Util;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.*;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.ObjectInspectorOptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopLevelDomain extends GenericUDF {
    ObjectInspector[] argOIs;

    @Override
    public Object evaluate(DeferredObject[] args) throws HiveException {

        if (args[0].get() == null || args.length != 1)
            return "";

        String domain = "";
        String url = (String) getJavaObject(args[0].get(), argOIs[0], new ArrayList<ObjectInspector.Category>());
        domain = Util.getDomainName(url);
        return domain;
    }

    private Object getJavaObject(Object o, ObjectInspector oi, List<ObjectInspector.Category> categories) {
        if(categories != null) {
            categories.add(oi.getCategory());
        }
        if(oi.getCategory() == ObjectInspector.Category.LIST) {
            List<?> l = ((ListObjectInspector)oi).getList(o);
            List<Object> result = new ArrayList<>();
            ObjectInspector elemOI = ((ListObjectInspector)oi).getListElementObjectInspector();
            for(Object lo : l) {
                result.add(getJavaObject(lo, elemOI, categories));
            }
            return result;
        } else if (oi.getCategory() == ObjectInspector.Category.MAP) {
            Map<?,?> m = ((MapObjectInspector)oi).getMap(o);
            Map<String, String> result = new HashMap<>();
            ObjectInspector koi = ((MapObjectInspector)oi).getMapKeyObjectInspector();
            ObjectInspector voi = ((MapObjectInspector)oi).getMapValueObjectInspector();
            for(Map.Entry<?,?> e: m.entrySet()) {
                result.put((String)getJavaObject(e.getKey(), koi, null),
                        (String)getJavaObject(e.getValue(), voi, null));
            }
            return result;

        } else if (oi.getCategory() == ObjectInspector.Category.STRUCT) {
            List<Object> s = ((StructObjectInspector)oi).getStructFieldsDataAsList(o);
            List<? extends StructField> sf = ((StructObjectInspector)oi).getAllStructFieldRefs();
            List<Object> result = new ArrayList<>();
            for(int i = 0; i < s.size(); i++) {
                result.add(getJavaObject(s.get(i), sf.get(i).getFieldObjectInspector(), categories));
            }
            return result;
        } else if(oi.getCategory() == ObjectInspector.Category.PRIMITIVE) {
            return ((PrimitiveObjectInspector)oi).getPrimitiveJavaObject(o);
        }
        throw new RuntimeException("Unexpected error!");
    }

    @Override
    public String getDisplayString(String[] arg0) {
        return "string";
    }

     @Override
     public ObjectInspector initialize(ObjectInspector[] argOIs)
             throws UDFArgumentException {

         this.argOIs = argOIs;

         return ObjectInspectorFactory.getReflectionObjectInspector(String.class,
                 ObjectInspectorOptions.JAVA);

     }
}
