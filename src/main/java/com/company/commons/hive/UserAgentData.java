package com.company.commons.hive;

import com.google.common.base.Strings;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.MapObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Arrays;

import com.company.commons.ua.*;

public class UserAgentData extends GenericUDF {
    ObjectInspector[] argOIs;

    @Override
    public Object evaluate(DeferredObject[] args) throws HiveException {
        ArrayList<String> fields = new ArrayList<>();
        fields.addAll(Arrays.asList("", "", "", "", ""));
        if (args[0].get() == null || args.length != 1)
            return fields;

        fields.clear();
        String browserName = "";
        String osName = "";
        String osVersion = "";
        String browserVersion = "";
        String deviceType = "";
        String ua = (String) getJavaObject(args[0].get(), argOIs[0], new ArrayList<Category>());
        if (!Strings.isNullOrEmpty(ua)) {
            UserAgentInfo uai = UserAgentInfo.parse(ua, true);
            browserName = uai.getBrowser().name();
            osName = uai.getOperatingSystem().name();

            osVersion = uai.getOsVersion();
            browserVersion = uai.getBrowserVersion();
            deviceType = uai.getDeviceType().name();
        }
        fields.add(browserName);
        fields.add(browserVersion);
        fields.add(osName);
        fields.add(osVersion);
        fields.add(deviceType);
        return fields;
    }

    private Object getJavaObject(Object o, ObjectInspector oi, List<Category> categories) {
        if(categories != null) {
            categories.add(oi.getCategory());
        }
        if(oi.getCategory() == ObjectInspector.Category.LIST) {
            List<?> l = ((ListObjectInspector)oi).getList(o);
            List<Object> result = new ArrayList<>();
            ObjectInspector elemOI = ((ListObjectInspector)oi).getListElementObjectInspector();
            for(Object lo : l) {
                result.add(getJavaObject(lo, elemOI, categories));
            }
            return result;
        } else if (oi.getCategory() == ObjectInspector.Category.MAP) {
            Map<?,?> m = ((MapObjectInspector)oi).getMap(o);
            Map<String, String> result = new HashMap<>();
            ObjectInspector koi = ((MapObjectInspector)oi).getMapKeyObjectInspector();
            ObjectInspector voi = ((MapObjectInspector)oi).getMapValueObjectInspector();
            for(Entry<?,?> e: m.entrySet()) {
                result.put((String)getJavaObject(e.getKey(), koi, null),
                        (String)getJavaObject(e.getValue(), voi, null));
            }
            return result;

        } else if (oi.getCategory() == ObjectInspector.Category.STRUCT) {
            List<Object> s = ((StructObjectInspector)oi).getStructFieldsDataAsList(o);
            List<? extends StructField> sf = ((StructObjectInspector)oi).getAllStructFieldRefs();
            List<Object> result = new ArrayList<>();
            for(int i = 0; i < s.size(); i++) {
                result.add(getJavaObject(s.get(i), sf.get(i).getFieldObjectInspector(), categories));
            }
            return result;
        } else if(oi.getCategory() == ObjectInspector.Category.PRIMITIVE) {
            return ((PrimitiveObjectInspector)oi).getPrimitiveJavaObject(o);
        }
        throw new RuntimeException("Unexpected error!");
    }

    @Override
    public String getDisplayString(String[] arg0) {
        return "string";
    }

   /* @Override
    public ObjectInspector initialize(ObjectInspector[] argOIs)
            throws UDFArgumentException {

        this.argOIs = argOIs;

        //return ObjectInspectorFactory.getReflectionObjectInspector(String.class,
                //ObjectInspectorOptions.JAVA);
        return ObjectInspectorFactory.getStandardListObjectInspector(ObjectInspectorFactory.getReflectionObjectInspector(String.class,
               ObjectInspectorOptions.JAVA));
    }  */
    @Override
    public StructObjectInspector initialize(ObjectInspector[] args)
            throws UDFArgumentException {

        if (args.length != 1) {
            throw new UDFArgumentException("tokenize() takes exactly 1 argument");
        }

        argOIs = args;

        List<String> fieldNames = new ArrayList<>(5);
        List<ObjectInspector> fieldOIs = new ArrayList<>(5);
        fieldNames.add("browserName");
        fieldNames.add("browserVersion");
        fieldNames.add("osName");
        fieldNames.add("osVersion");
        fieldNames.add("deviceType");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        /*List<String> fieldNames1 = new ArrayList<>(1);
        List<ObjectInspector> fieldOIs1 = new ArrayList<ObjectInspector>(1);
        fieldNames1.add("segmentsitem");
        fieldOIs1.add(ObjectInspectorFactory.getStandardListObjectInspector(ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs)));
        */
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);
    }
}
