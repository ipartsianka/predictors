package com.company.commons;

import com.google.common.io.ByteStreams;
import org.joda.time.DateTime;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class ResourceContent implements Closeable, AutoCloseable {
    protected DateTime lastModiefied;
    protected InputStream stream;
    private String charset = "UTF-8";

    protected ResourceContent() {
    }

    public ResourceContent(DateTime lastModiefied, InputStream stream) {
        this.lastModiefied = lastModiefied;
        this.stream = stream;
    }

    public DateTime getLastModified() {
        return lastModiefied;
    }

    public String readContent() {
        try {
            return new String(ByteStreams.toByteArray(stream), charset);
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                //ignore
            }
        }
    }

    public InputStream getStream() {
        return stream;
    }

    @Override
    public void close() throws IOException {
        stream.close();
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }
}
