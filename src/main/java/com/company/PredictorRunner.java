package com.company;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.company.beans.Tuple;
import com.company.beans.TupleCopier;
import com.company.beans.TupleReader;
import com.company.beans.TupleSchema;
import com.company.commons.Exceptions;
import com.company.commons.JsonUtil;
import com.company.commons.ResourceLoader;
import com.company.vw.FeatureVector;
import com.company.vw.VowpalWabbit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;


public class PredictorRunner {
    private static final Logger LOG = LoggerFactory.getLogger(PredictorRunner.class);

    @Parameter(names = "-type", description = "Type of a predictor", required = false)
    protected String modelType = "vw";

    @Parameter(names = "-in", description = "Model file name", required = false)
    protected String input = null;

    @Parameter(names = "-features", description = "Features that will be used for prediction. Numeric features starts with !", required = true)
    protected String features;

    @Parameter(names = "-data", description = "Data file name", required = false)
    protected String data = null;

    @Parameter(names = "-out", description = "Output file", required = false)
    protected String output = null;

    @Parameter(names = "-schema", description = "Schema location", required = true)
    protected String schemaLocation;

    @Parameter(names = "-outschema", description = "Schema location prediction", required = true)
    protected String schemaLocationPrediction;

    @Parameter(names = "-predictions", description = "Name of prediction field", required = false)
    protected String predictionName = "predicted_clicks";

    protected PredictorRunner(String[] args) {
        new JCommander(this).parse(args);
    }

    public static void main(String[] args) {
        PredictorRunner p = null;
        try {
            p = new PredictorRunner(args);
            p.run();
        } catch (Exception e) {
            LOG.error("Failed: " + Exceptions.getMessage(e), e);
            if (p != null) {
                p.cleanup();
            }
            System.exit(1);
        } finally {
            if (p != null) {
                p.cleanup();
            }
        }
    }

    protected void run() throws Exception {
        Predictor model = null;
        if ("vw".equals(modelType)) {
            model = new VowpalWabbit(JsonUtil.createJsonReader().readTree(new File(input)));
        }
        String schemaString = ResourceLoader.get(schemaLocation).load().readContent();
        String schemaStringPrediction = ResourceLoader.get(schemaLocationPrediction).load().readContent();
        TupleSchema schema = new TupleSchema(schemaString);
        TupleSchema schemaPrediction = new TupleSchema(schemaStringPrediction);
        TupleCopier copier = new TupleCopier(schema, schemaPrediction);
        BufferedWriter outputWriter = new BufferedWriter(new FileWriter(output));
        TupleReader reader = new TupleReader(schema, new File(data));
        outputWriter.write(schemaPrediction.getFieldsNames()+"\n");
        Tuple t;
        List<String> features = Lists.newArrayList(Splitter.on(',').omitEmptyStrings().trimResults().split(this.features));
        List<String> categorialFeatures = CategorialNumericFeatures.getCategorialFeatures(features);
        List<String> numericFeatures = CategorialNumericFeatures.getNumericFeatures(features);
        int linesProcessed = 0;
        LOG.info("Testing on file " + data);
        while (null != (t = reader.readTuple())) {
            FeatureVector featureVector;
            try {
                featureVector = new FeatureVector();
                for (String feature : categorialFeatures) {
                    featureVector.setFeatureValue(feature, t.get(feature), true);
                }
                for (String feature : numericFeatures) {
                    featureVector.setFeatureValue(feature, t.get(feature), false);
                }
            } catch (Exception e) {
                LOG.warn("Invalid segment: " + t.toMap(), e);
                continue;
            }
            Tuple t2 = copier.copy(t);
            t2.set(predictionName, model.predict(featureVector).getProbability());
            StringBuilder b = new StringBuilder();
            b.append(t2.toString()).append("\n");
            linesProcessed++;
            outputWriter.write(b.toString().replaceAll("[\\[|\\]]","").replaceAll(", ","\t"));
            if (linesProcessed % 10000 == 0) {
                LOG.info("Read {} lines", linesProcessed);
            }
        }
        outputWriter.close();
    }

    public void cleanup() {

    }
}