package com.company;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.company.beans.Tuple;
import com.company.beans.TupleReader;
import com.company.beans.TupleSchema;
import com.company.commons.Exceptions;
import com.company.commons.JsonUtil;
import com.company.commons.ResourceLoader;
import com.company.commons.Util;
import com.company.vw.*;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class VowpalWabbitPredictorBuilder extends BasePreditionCLI {

    public static final SecureRandom R = new SecureRandom();
    public static final String DICTIONARY_POSTFIX = ".dict.txt";
    protected String tmpDir;
    private long numberFailures = 0;
    private HashMap<String,Character> featureNamespaces = new HashMap<>();


    @Parameter(names = "-vw", description = "Path to vowpal wabbit executable", required = false)
    protected String vwBinary = "/usr/bin/vw";

    @Parameter(names = "-sed", description = "Path to sed executable", required = false)
    protected String sedLocation = "/bin/sed";

    @Parameter(names = "-sort", description = "Path to sort executable", required = false)
    protected String sortLocation = "/usr/bin/sort";

    @Parameter(names = "-bout", description = "Model file name", required = false)
    protected String binaryOutput = null;

    @Parameter(names = "-artifacts", description = "Run artifacts dir. If empty, all artifacts will be placed in temporary directory", required = false)
    protected String artifactsDir;

    @Parameter(names = "-shuf", description = "If input data should be shuffled", required = false)
    protected boolean shuffle = false;

    @Parameter(names = "-in_prediction", description = "Input files for prediction", variableArity = true)
    protected List<String> inputPrediction;

    @Parameter(names = "-out_prediction", description = "Output file prediction", required = false)
    protected String outputPrediction;

    @Parameter(names = "-m", description = "Mode: test, build or both", required = false)
    protected String mode = "build";

    @Parameter(names = "-predictions", description = "Name of prediction field", required = false)
    protected String prediction_name = "predicted_click";

    @Parameter(names = "-sampling_rate", description = "Number of times impressions set will be reduced in", required = false)
    protected int sampling = 1;

    @Parameter(names = "-cartesian_product", description = "Quadratic features, e.g. campaign_id:country,advertiser_id:browser", required = false)
    protected String quadraticFeatures;

    @Parameter(names = "-thr", description = "Threshold for trimming coefficients", required = false)
    protected double threshold = 0.;

    @Parameter(names = "-passes", description = "passes for vw", required = false)
    protected int passes = 2;

    private static final Logger LOG = LoggerFactory.getLogger(VowpalWabbitPredictorBuilder.class);

    public VowpalWabbitPredictorBuilder(String[] args) {
        super(args);
        new JCommander(this).parse(args);
        tmpDir = tmpDirBase + "/company-" + Util.newIdStr();
        Util.ensureDir(new File(tmpDir));
    }

    public void cleanup() {
        deleteIfNeeded(tmpDir);
    }

    public static void main(String[] args) throws Exception {
        VowpalWabbitPredictorBuilder p = null;
        try {
            p = new VowpalWabbitPredictorBuilder(args);
            List<String> files = new ArrayList<>();
            for (String path : p.input) {
                File folder = new File(path);
                if (folder.isDirectory()){
                    File[] listOfFiles = folder.listFiles();
                    for (File f : listOfFiles){
                        if (f.isFile())
                            files.add(f.getAbsolutePath());
                    }
                } else
                    files.add(folder.getAbsolutePath());
            }
            p.input.clear();
            Collections.sort(files);
            p.input.addAll(files);
            p.run();
        } catch (Exception e) {
            LOG.error("Failed: " + Exceptions.getMessage(e), e);
            if (p != null) {
                p.cleanup();
            }
            System.exit(1);
        } finally {
            if (p != null) {
                p.cleanup();
            }
        }
    }


    private static void deleteIfNeeded(String tmpDirToDelete) {
        if (tmpDirToDelete == null) {
            return;
        }
        File dir = new File(tmpDirToDelete);
        if (dir.exists() && dir.isDirectory()) {
            try {
                FileUtils.deleteDirectory(dir);
            } catch (IOException e) {
                LOG.warn("Unable to delete tmp dir: " + dir, e);
            }
        }
    }

    protected void run() {
        if (artifactsDir != null) {
            Util.ensureDir(artifactsDir);
        }
        String schemaString = ResourceLoader.get(schemaLocation).load().readContent();
        TupleSchema schema = new TupleSchema(schemaString);
        List<String> features = Lists.newArrayList(Splitter.on(',').omitEmptyStrings().trimResults().split(this.features));
        List<String> categorialFeatures = CategorialNumericFeatures.getCategorialFeatures(features);
        List<String> numericFeatures = CategorialNumericFeatures.getNumericFeatures(features);
        LossFunction lossFunction = LossFunction.LOGISTIC;
        char firstNamespace = 'A';
        for (String feature : categorialFeatures)
            featureNamespaces.put(feature,firstNamespace++);

        for (String feature : numericFeatures)
            featureNamespaces.put(feature,firstNamespace++);

        if ("build".equals(mode) || "both".equals(mode)) {
            VowpalWabbitRunner runner = new VowpalWabbitRunner(vwBinary, artifactsDir == null ? tmpDir : artifactsDir, new File(output), lossFunction, sampling, threshold);
            runner.setPasses(passes);
            if (binaryOutput != null) {
                runner.setBinaryModelOuput(new File(binaryOutput));
                runner.setSaveDictionaryTo(new File(binaryOutput + DICTIONARY_POSTFIX));
            }
            runner.start(featureNamespaces, sedLocation, sortLocation);
            if (this.quadraticFeatures != null){
                List<String> quadraticFeatures = Lists.newArrayList(Splitter.on(',').omitEmptyStrings().trimResults().split(this.quadraticFeatures));
                runner.setQuadratic(quadraticFeatures);
            }
            //Since wabbit is an incremental learner, input file list shuffle
            //might be a good idea (at least, it won't make any harm)
            List<String> shuffledInput = new ArrayList<>(input);
            Collections.shuffle(shuffledInput);
            for (String f : input) {
                LOG.info("Reading file " + f);
                TupleReader reader = new TupleReader(schema, new File(f));
                Tuple t;
                int linesProcessed = 0;
                List<Tuple> allData = new ArrayList<>();
                while (null != (t = reader.readTuple())) {
                    if (!shuffle) {
                        processTuple(categorialFeatures, numericFeatures, lossFunction, runner, t);
                    } else {
                        allData.add(t);
                    }
                    linesProcessed++;
                    if (linesProcessed % 10000 == 0) {
                        LOG.info("Read {} lines", linesProcessed);
                    }
                }
                if (shuffle) {
                    LOG.info("Shuffling learning sample");
                    Collections.shuffle(allData);
                    LOG.info("Processing");
                    for (Tuple tuple : allData) {
                        processTuple(categorialFeatures, numericFeatures, lossFunction, runner, tuple);
                        linesProcessed++;
                        if (linesProcessed % 10000 == 0) {
                            LOG.info("Processed {} lines", linesProcessed);
                        }
                    }
                }
            }
            LOG.info("Building model");
            runner.build();
        }
        if ("test".equals(mode) || "both".equals(mode)) {
            try {
                LOG.info("Testing model");
                testModel(schema, categorialFeatures, numericFeatures);
            } catch (IOException e) {
                throw Exceptions.wrap(e);
            }
        }

    }

    private void processTuple(List<String> categorialFeatures, List<String> numericFeatures, LossFunction lossFunction, VowpalWabbitRunner runner, Tuple t) {
        //pass the input raw into vw format of classification problems
        try {
            String clickEvent = t.get(successColumn, String.class);
            String impsEvent = t.get(triesColumn, String.class);
            if (clickEvent.equals("click")) {
                feedLine(numericFeatures, categorialFeatures, runner, t, 1, 1);
            }
            else if (impsEvent.equals("impression")) {
                numberFailures = numberFailures + 1;
                if (numberFailures % sampling == 0){
                    feedLine(numericFeatures, categorialFeatures, runner, t, -1, 1);
                }
            }
        } catch (Exception e) {
        }
    }

    private void testModel(TupleSchema schema, List<String> categorialFeatures, List<String> numericFeatures) throws IOException {
        VowpalWabbit model = new VowpalWabbit(JsonUtil.createJsonReader().readTree(new File(output)));
        NativeVowpalWabbit nativeModel = null;
        BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputPrediction));
        boolean useNamespaces = false;
        if (this.quadraticFeatures != null)
            useNamespaces = true;
        if (binaryOutput != null) {
            nativeModel = new NativeVowpalWabbit(vwBinary, new File(binaryOutput), new File(binaryOutput + DICTIONARY_POSTFIX), featureNamespaces, useNamespaces);
        }
        for (String f : inputPrediction) {
            TupleReader reader = new TupleReader(schema, new File(f));
            outputWriter.write(schema.getFieldsNames() + "\t" + prediction_name + "\n");
            Tuple t;
            int linesProcessed = 0;
            LOG.info("Testing on file " + f);
            while (null != (t = reader.readTuple())) {
                FeatureVector featureVector = new FeatureVector();
                try {
                    for (String feature : categorialFeatures){
                        String fValue = t.get(feature).toString();
                        if (fValue.equals("null")) fValue = "";
                        featureVector.setFeatureValue(feature, fValue, true);
                    }
                    for (String feature : numericFeatures) {
                        featureVector.setFeatureValue(feature, t.get(feature), false);
                    }
                } catch (Exception e) {
                    LOG.warn("Invalid segment: " + t.toMap(), e);
                    continue;
                }
                StringBuilder b = new StringBuilder();
                //b.append("Prediction on ").append(t).append("\n\t").append(t.toMap()).append("\n\t").append(VWHelper.format(Double.valueOf(t.toMap().get("impressions").toString())*model.predict(featureVector)));
                b.append(t.toString().replaceAll("[\\[|\\]]","")).append(", ").append(VWHelper.format(model.predictNew(featureVector).getProbability()));
                /*if (nativeModel != null) {
                    b.append("\n\t").append(VWHelper.format(nativeModel.predict(featureVector)));
                } */
                //LOG.info(b.toString());
                linesProcessed++;
                outputWriter.write(b.toString().replaceAll(", ","\t") + "\n");
                outputWriter.write(model.predictNew(featureVector).getDebugInformation() + "\n");
                if (linesProcessed % 10000 == 0) {
                    LOG.info("Read {} lines", linesProcessed);
                }
            }
        }
        outputWriter.close();
    }

    private void feedLine(List<String> numericFeatures, List<String> categorialFeatures, VowpalWabbitRunner runner, Tuple t, double value, double weight) {
        FeatureVector vector = new FeatureVector();
        try {
            for (String feature : numericFeatures)
                vector.setFeatureValue(feature, t.get(feature), false);
            for (String feature : categorialFeatures){
                String fValue = t.get(feature).toString();
                if (fValue.equals("null")) fValue = "";
                vector.setFeatureValue(feature, fValue, true);
            }
            VWHelper.validate(vector, value, weight);
        } catch (Exception e) {
            LOG.warn("Invalid segment: " + t.toMap(), e);
            return;
        }
        runner.feed(vector, value, weight);
    }

}