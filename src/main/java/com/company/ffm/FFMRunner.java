package com.company.ffm;

import com.company.VowpalWabbitRunner;
import com.company.commons.Exceptions;
import com.company.vw.FeatureDictionary;
import com.company.vw.FeatureType;
import com.company.vw.FeatureVector;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class FFMRunner {
    private static final Logger LOG = LoggerFactory.getLogger(FFMRunner.class);
    private static final double TEST_SET_RATIO = 0.2;
    private String ffmBinaryLocation;
    private String tmpDir;
    private String[] ffmOpts = new String[]{};
    private File modelOutput;
    private int samplingRate;
    private int testSetSize = 0;
    private int trainSetSize = 0;
    private double calibrationCoeff = 0.;

    private String inputDataFile;
    private String inputDataFileTest;
    private String predictionFile;
    private String logFile;
    private String logFilePrediction;
    private BufferedWriter dataWriter;
    private BufferedWriter dataWriterTest;
    private String aucFile;
    private String modelCoeffsFile;
    private int iterations = 15;
    private File dictionaryFile;
    private FeatureDictionary dictionary = new FeatureDictionary();
    public static final DecimalFormat VALUE_FORMAT = new DecimalFormat("#.############", DecimalFormatSymbols.getInstance(Locale.US));
    private HashMap<String,Integer> featureNamespaces = new HashMap<>();
    private double threshold = 0.;

    public FFMRunner(String ffmBinaryLocation, String tmpDir, File modelOutput, int sample, double thr) {
        this.ffmBinaryLocation = ffmBinaryLocation;
        this.tmpDir = tmpDir;
        this.modelOutput = modelOutput;
        this.samplingRate = sample;
        this.threshold = thr;
        dictionaryFile = new File(tmpDir + "/model.dict.txt");
        modelCoeffsFile = tmpDir + "/model.coefficients";
        inputDataFile = tmpDir + "/input.txt";
        inputDataFileTest = tmpDir + "/input_test.txt";
        logFile = tmpDir + "/run.log";
        logFilePrediction = tmpDir + "/run_prediction.log";
        predictionFile = tmpDir + "/prediction.txt";
        aucFile = tmpDir + "/auc.txt";
    }

    public void start(HashMap<String,Integer> namespaces) {
        this.featureNamespaces = namespaces;
        try {
            dataWriter = new BufferedWriter(new FileWriter(inputDataFile));
            dataWriterTest = new BufferedWriter(new FileWriter(inputDataFileTest));
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }


    public void feed(FeatureVector v, double value) {
        //splitting input data into train/test sets according to TEST_SET_RATIO
        try {
            String str = FFMHelper.toLine(dictionary, featureNamespaces, v, value);
            if ((1 - TEST_SET_RATIO)*testSetSize <= TEST_SET_RATIO*trainSetSize){
                dataWriterTest.write(str + "\n");
                testSetSize++;
            } else {
                dataWriter.write(str + "\n");
                trainSetSize++;
            }
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    public void build() {
        try {
            dataWriter.close();
            dataWriterTest.close();
        } catch (Exception e) {
            throw new IllegalStateException("Unable to close data writer file: " + inputDataFile);
        }
        run();
        readModelFile();
    }

    public void run(){
        List<String> args = Lists.newArrayList(ffmBinaryLocation + "/ffm-train");
        args.addAll(Lists.newArrayList(ffmOpts));
        //iterations
        args.addAll(Lists.newArrayList("-t", iterations + ""));

        // Change learning rate if desired (doesn't have real effects on accuracy)
        args.addAll(Arrays.asList("-l","0.001"));
        args.addAll(Arrays.asList("-k","16"));

        args.addAll(Arrays.asList("--no-norm", new File(inputDataFile).getAbsolutePath(), new File(modelCoeffsFile).getAbsolutePath()));


        ProcessBuilder processBuilder = new ProcessBuilder(args);
        LOG.info("Running: " + Joiner.on(" ").join(args));
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File(logFile)));
        processBuilder.directory(new File(tmpDir));
        try {
            Process process = processBuilder.start();
            LOG.info("Waiting for exit code");
            int exitCode = process.waitFor();
            LOG.info("Exit code: " + exitCode + ". VW log:\n", Files.toString(new File(logFile), Charset.defaultCharset()));
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code: " + exitCode);
            }
            if (dictionaryFile != null) {
                try (FileWriter w = new FileWriter(dictionaryFile)) {
                    dictionary.write(w);
                }
            }
            LOG.info("Building - OK. Reading model file");
            predictionWriting();
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            if (dictionaryFile != null) {
                LOG.info("Dictionary file: " + dictionaryFile.getAbsolutePath());
            }
            LOG.info("Model json: " + modelOutput.getAbsolutePath());
            LOG.info("Artifact - log file: " + new File(logFile).getAbsolutePath());
            LOG.info("Artifact - data file: " + new File(inputDataFile).getAbsolutePath());
            if (dictionaryFile != null) {
                LOG.info("Artifact - dictionary file: " + dictionaryFile.getAbsolutePath());
            }
            LOG.info("Artifact - model coeffs: " + new File(modelCoeffsFile).getAbsolutePath());
            LOG.info("Artifact - model json: " + modelOutput.getAbsolutePath());
        }
    }

    public void predictionWriting(){
        List<String> argsPrediction = Lists.newArrayList(ffmBinaryLocation + "/ffm-predict");
        argsPrediction.addAll(Lists.newArrayList(new File(inputDataFileTest).getAbsolutePath(), new File(modelCoeffsFile).getAbsolutePath(), new File(predictionFile).getAbsolutePath()));

        ProcessBuilder processBuilder = new ProcessBuilder(argsPrediction);
        LOG.info("Running prediction: " + Joiner.on(" ").join(argsPrediction));
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File(logFilePrediction)));
        processBuilder.directory(new File(tmpDir));
        try {
            Process process = processBuilder.start();
            LOG.info("Waiting for exit code prediction");
            int exitCode = process.waitFor();
            LOG.info("Exit code prediction: " + exitCode + ". VW log:\n", Files.toString(new File(logFilePrediction), Charset.defaultCharset()));
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code prediction: " + exitCode);
            }

            HashMap<String, Integer> predictions = predictionPerformance();
            Double precision = VowpalWabbitRunner.precisionRecall(predictions.get("true_positive"), predictions.get("false_positive"));
            Double recall = VowpalWabbitRunner.precisionRecall(predictions.get("true_positive"), predictions.get("false_negative"));
            Double accuracy = 1.0*(predictions.get("true_positive") + predictions.get("true_negative"))/(predictions.get("true_positive")
                    + predictions.get("true_negative") + predictions.get("false_positive") + predictions.get("false_negative"));
            LOG.info("Accuracy - prediction: " + accuracy);
            LOG.info("Precision - prediction: " + precision);
            LOG.info("Recall - prediction: " + recall);
            LOG.info("F measure - prediction: " + VowpalWabbitRunner.fMeasure(recall, precision));
            //LOG.info("kappa measure - prediction: " + kappaMeasure(predictions.get("true_positive"), predictions.get("false_positive"), predictions.get("false_negative"), predictions.get("true_negative")));

        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            LOG.info("Artifact - prediction: " + new File(predictionFile).getAbsolutePath());
        }
    }


    public HashMap<String, Integer> predictionPerformance(){
        List<Double> predictedProbabilities = VowpalWabbitRunner.readPredictedLabels(predictionFile);
        List<Integer> realLabels = VowpalWabbitRunner.readRealLabels(inputDataFileTest);
        HashMap<String, Integer> hm = new HashMap<String, Integer>(4);
        Integer truePositive = 0;
        Integer falsePositive = 0;
        Integer falseNegative = 0;
        Integer trueNegative = 0;
        Double logLoss = 0.;
        Double avgCTR = 0.;
        Double avgProbability = 0.;
        long count = 0;
        for (int i = 0; i < predictedProbabilities.size(); i++){
            count++;
            double probability = predictedProbabilities.get(i);
            avgCTR += (realLabels.get(i)>0) ? 1 : 0;
            avgProbability += probability;
            if (realLabels.get(i) == 1) {
                if (probability >= 0.5) truePositive++;
                else  falseNegative++;
                logLoss -= Math.log(probability);
            } else {
                if (probability >= 0.5) falsePositive++;
                else trueNegative++;
                logLoss -= Math.log(1 - probability);
            }
        }
        LOG.info("LogLoss - prediction: " + logLoss/count);
        hm.put("true_positive",truePositive);
        hm.put("false_positive",falsePositive);
        hm.put("false_negative",falseNegative);
        hm.put("true_negative",trueNegative);

        calibrationCoeff = VowpalWabbitRunner.inverseSigmoid(avgProbability / count) - VowpalWabbitRunner.inverseSigmoid(avgCTR / count);
        LOG.info("calibration coefficient: " + calibrationCoeff);
        LOG.info("avgCTR: " + avgCTR/count);
        LOG.info("avgProbability: " + avgProbability/count);

        try{
            BufferedWriter dataWriter = new BufferedWriter(new FileWriter(aucFile));

            StringBuilder b = new StringBuilder();
            for (int i = 0; i < predictedProbabilities.size(); i++){
                b.append(realLabels.get(i)).append(" ").append(VALUE_FORMAT.format(predictedProbabilities.get(i)));
                dataWriter.write(b.toString() + "\n");
                b.setLength(0);
            }
            dataWriter.close();

            int[] trueLabels = Ints.toArray(realLabels);
            int[] trueLabels_new = new int[trueLabels.length];
            int i = 0;
            for (int l : trueLabels){
                trueLabels_new[i] = 0;
                if (l == 1)  trueLabels_new[i] = 1;
                i++;
            }
            double[] scores = Doubles.toArray(predictedProbabilities);

            /*Curve analysis = new PrimitivesBuilder()
                    .predicteds(scores)
                    .actuals(trueLabels)
                    .build();
            // Calculate AUC ROC
            double area = analysis.rocArea();
            LOG.info("AUC - prediction: " + area);*/
        }catch (IOException e) {
            throw Exceptions.wrap(e);
        }
        return hm;
    }

    private void readModelFile() {
        try (
                JsonGenerator gen = new JsonFactory().createJsonGenerator(modelOutput, JsonEncoding.UTF8);
                BufferedReader r = new BufferedReader(new FileReader(modelCoeffsFile))
        ) {
            // n 310926
            // m 11
            // k 4
            // normalization 1
            // w0,0 0.296619 0.288977 0.298473 0.29627
            // w0,1 -0.0443922 -0.046548 -0.0260714 -0.0516951
            Integer numFeatures = Integer.parseInt(r.readLine().split(" ")[1]);
            Integer numFields = Integer.parseInt(r.readLine().split(" ")[1]);
            Integer numFactors = Integer.parseInt(r.readLine().split(" ")[1]);
            Integer normalization = Integer.parseInt(r.readLine().split(" ")[1]);
            String line;
            gen.setPrettyPrinter(new DefaultPrettyPrinter());
            gen.writeStartObject();
            gen.writeFieldName("meta");
            gen.writeStartObject();
            gen.writeStringField("loss_function", "logistic");
            gen.writeEndObject();
            gen.writeFieldName("features");
            gen.writeStartArray();
            writeConstant(gen);
            List<FFMRawLine> lines = new ArrayList<>();
            Integer currentFieldCount = 0;
            while (null != (line = r.readLine())) {
                FFMRawLine l = new FFMRawLine(line, dictionary, featureNamespaces);
                //if (l.getHash() == currentFeature)
                if (currentFieldCount < numFields) {
                    lines.add(l);
                    currentFieldCount ++;
                }
                else{
                    if (lines.size() > 0)
                        writeLines(gen, lines);
                    //currentFeature = l.getHash();
                    currentFieldCount = 1;
                    lines.clear();
                    lines.add(l);
                }
            }
            if (lines.size() > 0)
                writeLines(gen, lines);
            gen.writeEndArray();
            gen.writeEndObject();
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    private void writeLines(JsonGenerator gen, List<FFMRawLine> lines) throws IOException{
        FFMRawLine l = lines.get(0);
        gen.writeStartObject();
        gen.writeStringField("feature", l.getFeature());
        gen.writeStringField("type", FeatureType.classToString(l.getFeatureType().getType()));
        if (l.getFeatureValue() != null)
            gen.writeStringField("value", l.getFeatureValue());
        gen.writeFieldName("coeffs");
        gen.writeStartArray();
        for (FFMRawLine line : lines) {
            gen.writeStartObject();
            gen.writeStringField(line.getFieldValue(), Arrays.toString(line.getCoeff().toArray()));
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }

    private void writeConstant(JsonGenerator gen) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("type", "constant");
        gen.writeNumberField("c", - Math.log(samplingRate) - calibrationCoeff);
        gen.writeEndObject();
    }


    private void writeLine(JsonGenerator gen, FFMRawLine l) throws IOException {
        gen.writeStartObject();
        gen.writeFieldName("tuple");
        gen.writeStartArray();
        String ft = l.getFeature();
            gen.writeStartObject();
            gen.writeStringField("feature", ft);
            gen.writeStringField("ftype", FeatureType.classToString(l.getFeatureType().get(ft).getType()));
            if (l.getFeatureValue() != null) {
                gen.writeStringField("value", l.getFeatureValue());
            }
            gen.writeEndObject();

        gen.writeEndArray();
        gen.writeNumberField("c", l.getHash());
        gen.writeEndObject();
    }

    public void setNumIterations(int iterations) {
        this.iterations = iterations;
    }

    public int getNumIterations() {
        return iterations;
    }

    private static class FFMRawLine{
        protected String feature;
        protected String featureValue;
        protected String fieldValue;
        protected Integer hash;
        protected List<Double> coeffs;
        protected FeatureType featureType;

        public String getFeature() {
            return feature;
        }

        public String  getFeatureValue() {
            return featureValue;
        }

        public String  getFieldValue() {
            return fieldValue;
        }

        public Integer getHash() {
            return hash;
        }

        public List<Double> getCoeff() {
            return coeffs;
        }

        public FeatureType getFeatureType() {
            return featureType;
        }

        private FFMRawLine(String line, FeatureDictionary dictionary, HashMap<String,Integer> namespace) {
            try{
                Iterator<String> it = Splitter.on(" ").split(line.replaceAll("\\s+$", "")).iterator();
                String[] hashedFeature = it.next().substring(1).split(",");
                hash = Integer.parseInt(hashedFeature[0]);
                fieldValue = getKeyByValue(namespace, Integer.parseInt(hashedFeature[1]));
                FeatureDictionary.Feature resolve = dictionary.resolve(hash);
                Preconditions.checkState(resolve != null, "Unknown feature %s", hash);
                feature = resolve.getName();
                featureType = resolve.getFeatureType();
                featureValue = resolve.getValue() == null ? null : resolve.getFeatureType().getDataType().toString(resolve.getValue());
                coeffs = new ArrayList<>();

                String element;
                while (it.hasNext() && !(element = it.next()).contains("nan"))
                    coeffs.add(Double.parseDouble(element));
            } catch (Exception e)  {
                System.out.println(line);
                throw Exceptions.wrap(e);
            }
        }

        public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
            for (Map.Entry<T, E> entry : map.entrySet()) {
                if (com.google.common.base.Objects.equal(value, entry.getValue())) {
                    return entry.getKey();
                }
            }
            return null;
        }
    }
}
