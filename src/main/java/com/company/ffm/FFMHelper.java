package com.company.ffm;

import com.company.commons.Util;
import com.company.vw.FeatureDictionary;
import com.company.vw.FeatureType;
import com.company.vw.FeatureVector;
import com.company.vw.VWHelper;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class FFMHelper {
    public static final DecimalFormat VALUE_FORMAT = Util.decimalFormat(9);

    private FFMHelper() {
    }

    public static void validate(FeatureVector features, double value) throws RuntimeException {
        toLine(features, value);
    }

    public static String toLine(FeatureDictionary dict, HashMap<String,Integer> namespaces, FeatureVector features, double value) {
        StringBuilder b = new StringBuilder();
        b.append(VWHelper.format(value));
        for (FeatureType type : FeatureType.allTypes()) {
            Map<String, Object> featureMap = features.<Object>getFeatures(type);
            if (featureMap.isEmpty()) {
                continue;
            }
            if (!type.isCategorial()) {
                for (Iterator<Map.Entry<String, Object>> iterator = featureMap.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry<String, Object> e = iterator.next();
                    int id = dict.getId(e.getKey(), type, null);
                    b.append(" ").append(namespaces.get(e.getKey()) + ":").append(id).append(":").append(VWHelper.format(type.toDouble(e.getValue())));
                }
            } else {
                for (Iterator<Map.Entry<String, Object>> iterator = featureMap.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry<String, Object> e = iterator.next();
                    int id = dict.getId(e.getKey(), type, e.getValue());
                    b.append(" ").append(namespaces.get(e.getKey()) + ":").append(id).append(":1");
                }
            }
        }
        return b.toString();
    }

    public static String toLine(FeatureVector features, double value) {
        StringBuilder b = new StringBuilder();
        b.append(VWHelper.format(value));
        for (FeatureType type : FeatureType.allTypes()) {
            Map<String, Object> featureMap = features.<Object>getFeatures(type);
            if (featureMap.isEmpty()) {
                continue;
            }
            if (!type.isCategorial()) {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    b.append(" ").append(e.getKey()).append(":").append(VALUE_FORMAT.format(type.toDouble(e.getValue())));
                }
            } else {
                for (Map.Entry<String, Object> e : featureMap.entrySet()) {
                    b.append(" ").append(e.getKey()).append(":").append(type.getDataType().toString(e.getValue()));
                }
            }
        }
        return b.toString();
    }

}
