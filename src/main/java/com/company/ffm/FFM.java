package com.company.ffm;

import java.util.*;


import com.company.Predictor.Prediction;
import com.company.vw.*;
import com.company.vw.VowpalWabbit.CategorialFeatureKey;
import com.google.common.base.Preconditions;
import org.codehaus.jackson.JsonNode;
import org.json.JSONException;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

public class FFM {
    private LossFunction lossFunction;
    private double constant;
    private Map<CategorialFeatureKey, Map<String, List<Double>>> categorialCoefficients = new HashMap<>();
    private Map<String, Map<String, List<Double>>> numericCoefficients = new HashMap<>();
    private Generator<String> gen = new Generator<String>() {
        @Override
        public ICombinatoricsVector<String> getOriginalVector() {
            return null;
        }

        @Override
        public long getNumberOfGeneratedObjects() {
            return 0;
        }

        @Override
        public Iterator<ICombinatoricsVector<String>> iterator() {
            return null;
        }
    };

    public FFM(JsonNode node) throws JSONException{
        String lossFunctionName = node.get("meta").get("loss_function").asText().toUpperCase();
        lossFunction = LossFunction.valueOf(lossFunctionName);
        Preconditions.checkState(lossFunction != null, "Unknown loss function %s " + lossFunctionName);
        for (JsonNode n : node.get("features")) {
            if (n.size() > 0){
                String type = n.get("type").asText();
                if (type.equals("constant")) {
                    constant = n.get("c").asDouble();
                } else {
                    String featureName = n.get("feature").asText();
                    FeatureType featureType = FeatureType.get(FeatureType.getClass(type), n.has("value"));
                    Preconditions.checkState(featureType != null, "Unknown feature type %s", featureType);
                    Map<String, List<Double>> coeff = new HashMap<>();
                    for (JsonNode nn : n.get("coeffs")){
                        List<Double> l = new ArrayList<>();
                        String fName = nn.getFieldNames().next();
                        for(String s: nn.get(fName).getValueAsText().replace("[","").replace("]","").split(", "))
                            l.add(Double.parseDouble(s));
                        coeff.put(fName,l);
                    }
                    if (!featureType.isCategorial()) {
                        numericCoefficients.put(featureName, coeff);
                    } else {
                        Object value = featureType.getDataType().parse(n.get("value").asText());
                        categorialCoefficients.put(new CategorialFeatureKey(featureName, value), coeff);
                    }
                }
            }
        }
        Set<String> features = new HashSet<>();
        if (categorialCoefficients.size() > 0 )
            features.addAll(categorialCoefficients.values().iterator().next().keySet());
        if (numericCoefficients.size() > 0)
            features.addAll(numericCoefficients.values().iterator().next().keySet());
        ICombinatoricsVector<String> initialVector = Factory.createVector(features);
        gen = Factory.createSimpleCombinationGenerator(initialVector, 2);
    }

    public Prediction predict(FeatureVector key) {
        double predictedValue = constant;
        final StringBuilder debugInformationBuilder = new StringBuilder();
        debugInformationBuilder.append(constant);

        Set<String> categoricalFeatures = key.getCategoricalFeatures();

        for (ICombinatoricsVector<String> combination : gen) {
            List<String> pair = combination.getVector();

            String firstName = pair.get(0);
            Object first = key.getFeature(firstName);

            String secondName = pair.get(1);
            Object second = key.getFeature(secondName);

            Double m1 = 1.;
            Double m2 = 1.;

            List<Double> coeff1 = new ArrayList<>();
            List<Double> coeff2 = new ArrayList<>();

            if (categoricalFeatures.contains(firstName)) {
                if (categorialCoefficients.containsKey(new CategorialFeatureKey(firstName, first)))
                    coeff1 = categorialCoefficients.get(new CategorialFeatureKey(firstName, first)).get(secondName);
            } else {
                if (numericCoefficients.containsKey(firstName))
                    coeff1 = numericCoefficients.get(firstName).get(secondName);
                    m1 = Double.parseDouble(first.toString());
            }

            if (categoricalFeatures.contains(secondName)) {
                if (categorialCoefficients.containsKey(new CategorialFeatureKey(secondName, second)))
                    coeff2 = categorialCoefficients.get(new CategorialFeatureKey(secondName, second)).get(firstName);
            } else {
                if (numericCoefficients.containsKey(secondName))
                    coeff2 = numericCoefficients.get(secondName).get(firstName);
                    m2 = Double.parseDouble(second.toString());
            }

            Double coeff = 0.;
            if (coeff1.size() > 0 && coeff1.size() == coeff2.size()) {
                for (int i = 0; i < coeff1.size(); i++)
                    coeff += coeff1.get(i) * coeff2.get(i);

                predictedValue += 2*m1*m2*coeff;
                String sign = (coeff >= 0.) ? " +" : " ";
                debugInformationBuilder.append(sign).append(coeff.toString()).append("*").append(firstName).append("=").append(first).
                        append("&").append(secondName).append("=").append(second);
            }
        }
        debugInformationBuilder.append("=").append(predictedValue);
        return new Prediction(VWHelper.getPrediction(lossFunction, predictedValue)) {
            @Override
            public String getDebugInformation() {
                return debugInformationBuilder.toString();
            }
        };
    }
}
