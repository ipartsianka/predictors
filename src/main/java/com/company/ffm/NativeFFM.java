package com.company.ffm;


import com.company.Predictor;
import com.company.VowpalWabbitRunner;
import com.company.commons.Exceptions;
import com.company.commons.Util;
import com.company.vw.FeatureDictionary;
import com.company.vw.FeatureVector;
import com.company.vw.LossFunction;
import com.company.vw.VWHelper;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class NativeFFM implements Predictor{
    private static final Logger LOG = LoggerFactory.getLogger(NativeFFM.class);
    public static final Charset FFM_CHARSET = Charset.forName("UTF-8");
    private String libffmBinary;
    private File binaryModelFile;
    private File dictionaryFile;
    private FeatureDictionary dictionary;
    private HashMap<String,Integer> featureNamespaces = new HashMap<>();
    private BufferedWriter tmpPredictWriter;

    public NativeFFM(String libffmBinary, File binaryModelFile, File dictionaryFile, HashMap<String, Integer> featureNamespaces) {
        this.libffmBinary = libffmBinary;
        this.binaryModelFile = binaryModelFile;
        this.dictionaryFile = dictionaryFile;
        this.featureNamespaces = featureNamespaces;
        try{
            this.tmpPredictWriter = new BufferedWriter(new FileWriter(dictionaryFile.getAbsolutePath().substring(0, dictionaryFile.getAbsolutePath().lastIndexOf("/") + 1) + "tmpInput.txt"));
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
        if (dictionaryFile != null) {
            try (Reader r = new FileReader(dictionaryFile)) {
                dictionary = new FeatureDictionary();
                dictionary.read(r);
            } catch (Exception e) {
                throw Exceptions.wrap(e);
            }
        }
    }

    @Override
    public Predictor.Prediction predict(FeatureVector key) {

        File tmpOut = null;
        File tmpIn = null;
        try {
            List<String> args = Lists.newArrayList(libffmBinary + "/ffm-predict");

            tmpOut = File.createTempFile(Util.newIdStr(), Util.newIdStr());
            tmpIn = File.createTempFile(Util.newIdStr(), Util.newIdStr());
            args.addAll(Lists.newArrayList(tmpIn.getAbsolutePath(), binaryModelFile.getAbsolutePath(), tmpOut.getAbsolutePath()));

            ProcessBuilder processBuilder = new ProcessBuilder(args);
            String line = FFMHelper.toLine(dictionary, featureNamespaces, key, 1.0);
            Files.write(line, tmpIn, FFM_CHARSET);
            processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
            processBuilder.redirectErrorStream(true);
            processBuilder.redirectOutput(ProcessBuilder.Redirect.PIPE);
            processBuilder.redirectError();
            Process process = processBuilder.start();
            process.getOutputStream().write((line + "\n").getBytes());
            int res = process.waitFor();
            byte[] outputBytes = ByteStreams.toByteArray(process.getInputStream());
            if (res != 0) {
                LOG.info("Running: " + Joiner.on(" ").join(args) + " on '" + line + "'");
                LOG.info("FFM log:\n" + new String(outputBytes));
                throw new IllegalStateException("FFM failed with exit code " + res);
            }
            String outStr = Files.toString(tmpOut, FFM_CHARSET);
            double val = Double.parseDouble(Splitter.on(" ").split(outStr).iterator().next());
            return Predictor.Prediction.prob(val, line);
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            if (tmpOut != null) {
                tmpOut.delete();
            }
            if (tmpIn != null) {
                tmpOut.delete();
            }
        }
    }

    public void addFeatureVector(FeatureVector key){
        String line = FFMHelper.toLine(dictionary, featureNamespaces, key, 1.0);
        try{
            tmpPredictWriter.write(line + "\n");
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public void closeTmpWriter(){
        try{
            this.tmpPredictWriter.close();
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public List<Double> predictAll(){
        File tmpOut = null;
        String tmpBase = dictionaryFile.getAbsolutePath().substring(0, dictionaryFile.getAbsolutePath().lastIndexOf("/") + 1);
        List<String> argsPrediction = Lists.newArrayList(libffmBinary + "/ffm-predict");
        argsPrediction.addAll(Lists.newArrayList(new File(tmpBase + "tmpPrediction.txt").getAbsolutePath(), binaryModelFile.getAbsolutePath(),
                new File(tmpBase + "tmpInput.txt").getAbsolutePath()));

        ProcessBuilder processBuilder = new ProcessBuilder(argsPrediction);
        LOG.info("Running prediction: " + Joiner.on(" ").join(argsPrediction));
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectErrorStream(true);
        try {
            tmpOut = File.createTempFile(Util.newIdStr(), Util.newIdStr());
            processBuilder.redirectOutput(ProcessBuilder.Redirect.to(tmpOut));
            processBuilder.redirectInput(ProcessBuilder.Redirect.from(new File(tmpBase + "tmpInput.txt")));

            Process process = processBuilder.start();
            LOG.info("Waiting for exit code prediction");
            int exitCode = process.waitFor();
            LOG.info("Exit code prediction: " + exitCode);
            if (0 != exitCode) {
                throw new IllegalStateException("VW exited with non-zero exit code prediction: " + exitCode);
            }
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        } finally {
            if (tmpOut != null) {
                tmpOut.delete();
            }
        }
        return VowpalWabbitRunner.readPredictedLabels(tmpBase + "tmpPrediction.txt");
    }
}
