package com.company.ffm;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.company.BasePreditionCLI;
import com.company.CategorialNumericFeatures;
import com.company.Predictor;
import com.company.beans.Tuple;
import com.company.beans.TupleReader;
import com.company.beans.TupleSchema;
import com.company.commons.Exceptions;
import com.company.commons.JsonUtil;
import com.company.commons.ResourceLoader;
import com.company.commons.Util;
import com.company.vw.FeatureVector;
import com.company.vw.VWHelper;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class FFMPredictorBuilder extends BasePreditionCLI {

    protected String tmpDir;
    private long numberFailures = 0;
    private HashMap<String,Integer> featureNamespaces = new HashMap<>();

    @Parameter(names = "-libffm", description = "Path to libffm executable", required = false)
    protected String libffmBinary = "";

    @Parameter(names = "-artifacts", description = "Run artifacts dir. If empty, all artifacts will be placed in temporary directory", required = false)
    protected String artifactsDir;

    @Parameter(names = "-shuf", description = "If input data should be shuffled", required = false)
    protected boolean shuffle = false;

    @Parameter(names = "-in_prediction", description = "Input files for prediction", variableArity = false)
    protected List<String> inputPrediction;

    @Parameter(names = "-out_prediction", description = "Output file prediction", required = false)
    protected String outputPrediction;

    @Parameter(names = "-m", description = "Mode: test, build or both", required = false)
    protected String mode = "build";

    @Parameter(names = "-predictions", description = "Name of prediction field", required = false)
    protected String prediction_name = "predicted_click";

    @Parameter(names = "-sampling_rate", description = "Number of times impressions set will be reduced in", required = false)
    protected int sampling = 1;

    @Parameter(names = "-thr", description = "Threshold for trimming coefficients", required = false)
    protected double threshold = 0.;

    @Parameter(names = "-debug", description = "write debug info", required = false)
    protected boolean debug = false;

    private static final Logger LOG = LoggerFactory.getLogger(FFMPredictorBuilder.class);

    public FFMPredictorBuilder(String[] args) {
        super(args);
        new JCommander(this).parse(args);
        tmpDir = tmpDirBase + "/CTR-" + Util.newIdStr();
        Util.ensureDir(new File(tmpDir));
    }

    public void cleanup() {
        deleteIfNeeded(tmpDir);
    }

    public static void main(String[] args) throws Exception {
        FFMPredictorBuilder p = null;
        try {
            p = new FFMPredictorBuilder(args);
            List<String> files = new ArrayList<>();
            for (String path : p.input) {
                File folder = new File(path);
                if (folder.isDirectory()){
                    File[] listOfFiles = folder.listFiles();
                    for (File f : listOfFiles){
                        if (f.isFile())
                            files.add(f.getAbsolutePath());
                    }
                } else
                    files.add(folder.getAbsolutePath());
            }
            p.input.clear();
            Collections.sort(files);
            p.input.addAll(files);
            p.run();
        } catch (Exception e) {
            LOG.error("Failed: " + Exceptions.getMessage(e), e);
            if (p != null) {
                p.cleanup();
            }
            System.exit(1);
        } finally {
            if (p != null) {
                p.cleanup();
            }
        }
    }


    private static void deleteIfNeeded(String tmpDirToDelete) {
        if (tmpDirToDelete == null) {
            return;
        }
        File dir = new File(tmpDirToDelete);
        if (dir.exists() && dir.isDirectory()) {
            try {
                FileUtils.deleteDirectory(dir);
            } catch (IOException e) {
                LOG.warn("Unable to delete tmp dir: " + dir, e);
            }
        }
    }

    protected void run() {
        if (artifactsDir != null) {
            Util.ensureDir(artifactsDir);
        }
        String schemaString = ResourceLoader.get(schemaLocation).load().readContent();
        TupleSchema schema = new TupleSchema(schemaString);
        List<String> features = Lists.newArrayList(Splitter.on(',').omitEmptyStrings().trimResults().split(this.features));
        List<String> categorialFeatures = CategorialNumericFeatures.getCategorialFeatures(features);
        List<String> numericFeatures = CategorialNumericFeatures.getNumericFeatures(features);

        Integer firstNamespace = 0;
        for (String feature : categorialFeatures)
            featureNamespaces.put(feature,firstNamespace++);

        for (String feature : numericFeatures)
            featureNamespaces.put(feature,firstNamespace++);

        if ("build".equals(mode) || "both".equals(mode)) {
            FFMRunner runner = new FFMRunner(libffmBinary, artifactsDir == null ? tmpDir : artifactsDir, new File(output), sampling, threshold);
            runner.setNumIterations(100);
            runner.start(featureNamespaces);

            List<String> shuffledInput = new ArrayList<>(input);
            Collections.shuffle(shuffledInput);
            for (String f : input) {
                LOG.info("Reading file " + f);
                TupleReader reader = new TupleReader(schema, new File(f));
                Tuple t;
                int linesProcessed = 0;
                List<Tuple> allData = new ArrayList<>();
                while (null != (t = reader.readTuple())) {
                    if (!shuffle) {
                        processTuple(categorialFeatures, numericFeatures, runner, t);
                    } else {
                        allData.add(t);
                    }
                    linesProcessed++;
                    if (linesProcessed % 10000 == 0) {
                        LOG.info("Read {} lines", linesProcessed);
                    }
                }
                if (shuffle) {
                    LOG.info("Shuffling learning sample");
                    Collections.shuffle(allData);
                    LOG.info("Processing");
                    for (Tuple tuple : allData) {
                        processTuple(categorialFeatures, numericFeatures, runner, tuple);
                        linesProcessed++;
                        if (linesProcessed % 10000 == 0) {
                            LOG.info("Processed {} lines", linesProcessed);
                        }
                    }
                }
            }
            LOG.info("Building model");
            runner.build();
        }
        if ("test".equals(mode) || "both".equals(mode)) {
            try {
                LOG.info("Testing model");
                testModel(schema, categorialFeatures, numericFeatures);
            } catch (IOException | JSONException e) {
                throw Exceptions.wrap(e);
            }
        }

    }

    private void processTuple(List<String> categorialFeatures, List<String> numericFeatures, FFMRunner runner, Tuple t) {
        try {
            String clickEvent = t.get(successColumn, String.class);
            String impsEvent = t.get(triesColumn, String.class);
            if (clickEvent.equals("click")) {
                feedLine(numericFeatures, categorialFeatures, runner, t, 1);
            }
            else if (impsEvent.equals("impression")) {
                numberFailures = numberFailures + 1;
                if (numberFailures % sampling == 0){
                    feedLine(numericFeatures, categorialFeatures, runner, t, 0);
                }
            }
        } catch (Exception e) {
        }
    }

    private void testModel(TupleSchema schema, List<String> categorialFeatures, List<String> numericFeatures) throws IOException, JSONException {
        FFM model = new FFM(JsonUtil.createJsonReader().readTree(new File(output)));
        BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputPrediction));
        String dir = artifactsDir == null ? tmpDir : artifactsDir;
        NativeFFM nativeModel = new NativeFFM(libffmBinary, new File(dir + "/model.coefficients"), new File(dir + "/model.dict.txt"), featureNamespaces);
        for (String f : inputPrediction) {
            TupleReader reader = new TupleReader(schema, new File(f));
            outputWriter.write(schema.getFieldsNames() + "\t" + prediction_name + "\n");//"\t" + "native_prediction" + "\n");
            Tuple t;
            int linesProcessed = 0;
            LOG.info("Testing on file " + f);
            while (null != (t = reader.readTuple())) {
                FeatureVector featureVector = new FeatureVector();
                try {
                    for (String feature : categorialFeatures){
                        String fValue = t.get(feature).toString();
                        if (fValue.equals("null")) fValue = "";
                        featureVector.setFeatureValue(feature, fValue, true);
                    }
                    for (String feature : numericFeatures) {
                        featureVector.setFeatureValue(feature, t.get(feature), false);
                    }
                } catch (Exception e) {
                    LOG.warn("Invalid segment: " + t.toMap(), e);
                    continue;
                }

                StringBuilder b = new StringBuilder();
                Predictor.Prediction pr = model.predict(featureVector);
                //b.append("Prediction on ").append(t).append("\n\t").append(t.toMap()).append("\n\t").append(VWHelper.format(Double.valueOf(t.toMap().get("impressions").toString())*model.predict(featureVector)));
                b.append(t.toString().replaceAll("[\\[|\\]]","")).append(", ").append(VWHelper.format(pr.getProbability()));
                linesProcessed++;
                outputWriter.write(b.toString().replaceAll(", ","\t") + "\n");
                //outputWriter.write(model.predictNew(featureVector).getDebugInformation() + "\n");
                if (linesProcessed % 10000 == 0) {
                    LOG.info("Read {} lines", linesProcessed);
                }
            }
        }
        outputWriter.close();
    }

    private void feedLine(List<String> numericFeatures, List<String> categorialFeatures, FFMRunner runner, Tuple t, double value) {
        FeatureVector vector = new FeatureVector();
        try {
            for (String feature : numericFeatures)
                vector.setFeatureValue(feature, t.get(feature), false);
            for (String feature : categorialFeatures){
                String fValue = t.get(feature).toString();
                if (fValue.equals("null")) fValue = "";
                vector.setFeatureValue(feature, fValue, true);
            }
        } catch (Exception e) {
            LOG.warn("Invalid segment: " + t.toMap(), e);
            return;
        }

        runner.feed(vector, value);
    }

}