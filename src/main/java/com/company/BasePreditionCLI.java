package com.company;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.company.beans.TupleSchema;
import com.company.commons.ResourceLoader;
import com.company.commons.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.util.List;

public abstract class BasePreditionCLI {
    protected static final Logger LOG = LoggerFactory.getLogger(BasePreditionCLI.class);

    protected String tmpDir;
    @Parameter(names = "-tmp", description = "Temporary directory", required = false)
    protected String tmpDirBase = "/tmp";
    @Parameter(names = "-out", description = "Model file name", required = false)
    protected String output = "model.json";
    @Parameter(names = "-schema", description = "Schema location", required = true)
    protected String schemaLocation;
    @Parameter(names = "-features", description = "Features that will be used for prediction. Numeric features starts with !", required = true)
    protected String features;
    @Parameter(names = "-tries", description = "Tries column", required = false)
    protected String triesColumn = "impressions";
    @Parameter(names = "-successes", description = "Success column", required = false)
    protected String successColumn = "clicks";
    @Parameter(names = "-in", description = "Input files", variableArity = true)
    protected List<String> input;

    protected BasePreditionCLI(String[] args) {
        new JCommander(this).parse(args);
        tmpDir = tmpDirBase + "/CTR-" + Util.newIdStr();
        Util.ensureDir(new File(tmpDir));
    }

    protected TupleSchema readSchema() {
        String schemaString = ResourceLoader.get(schemaLocation).load().readContent();
        return new TupleSchema(schemaString);
    }

    protected abstract void run() throws FileNotFoundException, Exception;

    public abstract void cleanup();

    public static void run(Class<? extends BasePreditionCLI> cls, String[] args) {
        BasePreditionCLI instance = null;
        try {
            Constructor<? extends BasePreditionCLI> constructor = cls.getDeclaredConstructor(String[].class);
            constructor.setAccessible(true);
            instance = constructor.newInstance((Object)args);
            instance.run();
            System.exit(0);
        } catch (Throwable e) {
            LOG.error("Error", e);
            if (instance != null) {
                try {
                    instance.cleanup();
                } catch (Exception e2) {
                    LOG.warn("Error during cleanup", e2);
                }
            }
            System.exit(1);
        }

    }

}
