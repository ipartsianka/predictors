package com.company;

import com.company.vw.FeatureType;

import java.util.ArrayList;
import java.util.List;

public class CategorialNumericFeatures {

    public static List<String> getCategorialFeatures(List<String> features) {
        List<String> res = new ArrayList();
        for (String feature : features) {
            if (!feature.startsWith(FeatureType.NUMERIC_PREFIX)) {
                if (feature.startsWith(FeatureType.CATEGORIAL_PREFIX)) {
                    feature = feature.substring(1);
                }
                res.add(feature);
            }
        }
        return res;
    }

    public static List<String> getNumericFeatures(List<String> features) {
        List<String> res = new ArrayList();
        for (String feature : features) {
            if (feature.startsWith(FeatureType.NUMERIC_PREFIX)) {
                feature = feature.substring(1);
                res.add(feature);
            }
        }
        return res;
    }
}