package com.company;

import com.company.vw.FeatureVector;

public interface Predictor {
    public Prediction predict(FeatureVector key);

    public abstract static class Prediction {
        private double probability;

        protected Prediction(double probability) {
            this.probability = probability;
        }

        public double getProbability() {
            return probability;
        }

        public abstract String getDebugInformation();

        public static Prediction prob(double prob) {
            return new Prediction(prob) {
                @Override
                public String getDebugInformation() {
                    return null;
                }
            };
        }

        public static Prediction prob(double prob, final String debugInformation) {
            return new Prediction(prob) {
                @Override
                public String getDebugInformation() {
                    return debugInformation;
                }
            };
        }
    }
}
