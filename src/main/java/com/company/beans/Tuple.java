package com.company.beans;

import com.google.common.base.Preconditions;

import java.util.*;

public class Tuple implements Comparable<Tuple> {
    protected Object[] values;
    protected boolean[] initialized;
    protected TupleSchema schema;
    protected final int size;

    public Tuple(TupleSchema schema) {
        this.schema = schema;
        size = schema.size();
        values = new Object[size];
        initialized = new boolean[schema.size()];
        Arrays.fill(initialized, false);
    }

    public void set(int idx, Object value) {
        checkIndex(idx);
        values[idx] = value;
        initialized[idx] = true;
    }

    public void setUnchecked(int idx, Object value){
        if (idx>=0 && idx<values.length){
            values[idx] = value;
            initialized[idx] = true;
        }
    }

    public Object get(int idx) {
        checkIndex(idx);
        Preconditions.checkArgument(initialized[idx], "Value at index %s (field %s) isn' initialized", idx, schema.field(idx).getName());
        return values[idx];
    }

    public void set(String name, Object value) {
        set(schema.indexOfStrict(name), value);
    }

    public Object get(String name) {
        return get(schema.indexOfStrict(name));
    }

    public String getAsString(String name) {
        return getAsString(schema.indexOfStrict(name));
    }

    public String getAsString(int i) {
        return schema.getField(i).getType().toString(get(i));
    }

    public <T> T get(String name, Class<T> cls) {
        return get(schema.indexOfStrict(name), cls);
    }

    public <T> T get(int idx, Class<T> cls) {
        return (T) get(idx);
    }

    protected void checkIndex(int idx) {
        Preconditions.checkArgument(idx >= 0 && idx < size, "Index %s out of bounds [0, %s]", idx, size - 1);
    }

    public Tuple copy() {
        Tuple copy = new Tuple(schema);
        for (int i = 0 ; i < schema.size(); i++) {
            copy.set(i, get(i));
        }
        return copy;
    }

    public TupleSchema getSchema() {
        return schema;
    }

    @Override
    public String toString() {
        List<String> fields = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            fields.add(schema.getField(i).getType().toString(get(i)));
        }
        return fields.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple tuple = (Tuple) o;
        return Arrays.equals(values, tuple.values);
    }

    @Override
    public int hashCode() {
        return values != null ? Arrays.hashCode(values) : 0;
    }

    public void validate() {
        for (int idx = 0; idx < initialized.length; idx++) {
            if (!initialized[idx]) {
                throw new IllegalStateException("Field " + schema.getField(idx).getName() + " isn't initialized");
            }
        }
    }

    public void setAsString(String fieldName, String fieldValue) {
        setAsString(schema.indexOfStrict(fieldName), fieldValue);
    }

    private void setAsString(int idx, String fieldValue) {
        set(idx, schema.getField(idx).getType().parse(fieldValue));
    }

    public Map<String, Object> toMap() {
        Map<String, Object> res = new LinkedHashMap();
        for (int i = 0 ; i < size; i++) {
            res.put(schema.getField(i).getName(), values[i]);
        }
        return res;
    }

    @Override
    public int compareTo(Tuple o) {
        return schema.getComparator().compare(this, o);
    }
}
