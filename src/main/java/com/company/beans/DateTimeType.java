package com.company.beans;

import com.google.common.base.Strings;
import com.company.commons.Util;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Comparator;
import java.util.TimeZone;

public class DateTimeType implements DataType<DateTime>, ParamsAware, BinaryType<DateTime>, Comparator<DateTime> {
    private String pattern = "yyyy-MM-dd HH:mm:ss";
    private DateTimeZone timeZone = DateTimeZone.UTC;
    private DateTimeFormatter dateFormat = DateTimeFormat.forPattern(pattern).withZone(timeZone);

    @Override
    public DateTime parse(String str) {
        if (Strings.isNullOrEmpty(str) || str.equals("null")) {
            return null;
        }
        try {
            return dateFormat.parseDateTime(str);
        } catch (Exception e) {
            throw new IllegalStateException("Can't parse date '" + str + "' as " + pattern, e);
        }
    }

    @Override
    public String toString(DateTime object) {
        if (object == null) {
            return "";
        } else {
            return dateFormat.print(object);
        }
    }

    @Override
    public String describe() {
        return "Date[" + pattern + "]";
    }

    @Override
    public void setParams(String[] args) {
        this.pattern = args[0];
        if (args.length > 1) {
            this.timeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(args[1]));
        }
        this.dateFormat = DateTimeFormat.forPattern(args[0]).withZone(timeZone);
    }

    @Override
    public DateTime read(DataInput in) throws IOException {
        return new DateTime(in.readLong(), timeZone);
    }

    @Override
    public void write(DateTime obj, DataOutput out) throws IOException {
        out.writeLong(obj.toDate().getTime());
    }

    @Override
    public int compare(DateTime dateTime, DateTime dateTime1) {
        return Util.nullSafeCompare(dateTime, dateTime1);
    }
}
