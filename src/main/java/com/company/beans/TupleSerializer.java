package com.company.beans;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.company.beans.util.LogFieldEscaper;
import com.company.commons.Exceptions;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TupleSerializer implements DataType<Tuple>, ParamsAware, BinaryType<Tuple> {
    private TupleSchema schema;
    private Splitter splitter;
    private Joiner joiner;
    private Integer[] indexes;
    private List<String> header;
    private LogFieldEscaper escaper;

    public TupleSerializer(TupleSchema schema) {
        this.schema = schema;
        setDelimiter('\t');
        setHeader(Lists.newArrayList(Iterables.transform(schema.getFields(), new Function<TupleSchema.Field, String>() {
            @Override
            public String apply(TupleSchema.Field input) {
                return input.getName();
            }
        })));
    }

    public void setHeader(List<String> fieldNames) {
        this.header = fieldNames;
        indexes = new Integer[header.size()];
        for (int i = 0, fieldNamesSize = fieldNames.size(); i < fieldNamesSize; i++) {
            String field = fieldNames.get(i);
            indexes[i] = schema.indexOf(field);
        }
    }

    /**
     * Checks if all fields that doesn't present in header have default values
     */
    public void validateDefaultValues() throws IllegalStateException {
        Set<String> headerSet = new HashSet<String>(header);
        for (TupleSchema.Field f : schema.getFields()) {
            if (!f.hasDefaultValue() && !headerContainsField(headerSet, f)) {
                throw new IllegalStateException("Field " + f.getName() + " isn't present in header and doesn't have default value");
            }
        }
    }

    private boolean headerContainsField(Set<String> headerSet, TupleSchema.Field f) {
        if (headerSet.contains(f.getName())) {
            return true;
        }
        for (String a : f.getAliases()) {
            if (headerSet.contains(a)) {
                return true;
            }
        }
        return false;
    }

    public List<String> getHeader() {
        return header;
    }

    @Override
    public Tuple parse(String str) {
        return parse(str, null);
    }

    public Tuple parse(String str, Tuple reuse) {
        Tuple tuple = reuse == null ? schema.newTuple() : reuse;
        int idx = 0;
        for (String v : splitter.split(str)) {
            v = escaper.unEscapeLogField(v);
            int targetIndex = indexes[idx];
            if (targetIndex >= 0) {
                Object val;
                try {
                    val = schema.field(targetIndex).getType().parse(v);
                } catch (Exception e) {
                    throw Exceptions.runtime(e, "Can't parse field #%s:%s, value: '%s'", targetIndex, schema.field(targetIndex), v);
                }
                tuple.set(targetIndex, val);
            }
            idx++;
        }
        return tuple;
    }

    @Override
    public String toString(final Tuple object) {
        List<String> fields = new ArrayList<String>();
        for (int i = 0; i < indexes.length; i++) {
            int targetIndex = indexes[i];
            String field = schema.field(targetIndex).getType().toString(object.get(targetIndex));
            fields.add(escaper.escapeLogField(field));
        }
        return joiner.join(fields);
    }



    @Override
    public String describe() {
        return "tuple[" + schema + "]";
    }

    @Override
    public void setParams(String[] args) {
        if (args.length > 0) {
            setDelimiter(args[0].charAt(0));
        }
    }

    private void setDelimiter(char separator) {
        this.joiner = Joiner.on(separator);
        this.splitter = Splitter.on(separator);
        escaper = new LogFieldEscaper(new char[]{'\n', separator,'\r'},new char[]{'n','t','r'});
    }

    @Override
    public Tuple read(DataInput in) throws IOException {
        Tuple tuple = schema.newTuple();
        for (int i = 0; i < indexes.length; i++) {
            int targetIndex = indexes[i];
            TupleSchema.Field f = schema.field(targetIndex);
            DataType type = f.getType();
            if (!(type instanceof BinaryType)) {
                throw new IllegalStateException("Field " + f.getName() + " type of " + f.getType() + " isn't support binary serialization");
            }
            BinaryType binaryType = (BinaryType) type;
            Object val = binaryType.read(in);
            tuple.set(targetIndex, val);
        }
        return tuple;
    }

    @Override
    public void write(Tuple object, DataOutput out) throws IOException {
        for (int i = 0; i < indexes.length; i++) {
            int targetIndex = indexes[i];
            TupleSchema.Field f = schema.field(targetIndex);
            DataType type = f.getType();
            Object val = object.get(targetIndex);
            if (!(type instanceof BinaryType)) {
                throw new IllegalStateException("Field " + f.getName() + " type of " + f.getType() + " isn't support binary serialization");
            }
            BinaryType binaryType = (BinaryType) type;
            try {
                binaryType.write(val, out);
            } catch (Exception e) {
                throw new RuntimeException("Can't write field " + f.getName() + " with value '" + val + "'", e);
            }
        }
    }
}
