package com.company.beans;

public interface ParamsAware {
    public void setParams(String[] args);
}
