package com.company.beans;

import com.clearspring.analytics.stream.cardinality.HyperLogLog;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.company.commons.Exceptions;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;


public class UniqueCounterType implements DataType<HyperLogLog>, ParamsAware {
    private static final Charset UTF_8 = Charset.forName("utf-8");
    public static final String NAME = "unique_counter";
    public static final double RSD = 0.02;
    private String cardinalityField;

    public UniqueCounterType() {
    }

    @Override
    public HyperLogLog parse(String str) {
        if(str.isEmpty()){
            return new HyperLogLog(RSD);
        }
        try {
            byte[] strBytes = str.getBytes(UTF_8);
            byte[] decodedBytes = Base64.decodeBase64(strBytes);
            byte[] decompressedBytes = decompress(decodedBytes);
            return HyperLogLog.Builder.build(decompressedBytes);
        } catch (IOException | DataFormatException e) {
            throw Exceptions.wrap(e);
        }
    }

    @Override
    public String toString(HyperLogLog object) {
        final byte[] bytes;
        try {
            bytes = object.getBytes();
            byte[] compressedBytes = compress(bytes);
            byte[] encodedBytes = Base64.encodeBase64(compressedBytes);
            return new String(encodedBytes, UTF_8);
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    @Override
    public String describe() {
        return NAME;
    }

    public static byte[] compress(byte[] data) throws IOException {
        Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION);
        deflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer); // returns the generated code... index
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        return outputStream.toByteArray();
    }

    public static byte[] decompress(byte[] data) throws IOException, DataFormatException {
        Inflater inflater = new Inflater();
        inflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();

        return output;
    }

    public String getCardinalityFieldName() {
        return cardinalityField;
    }

    @Override
    public void setParams(String[] args) {
        Preconditions.checkState(args.length >= 1, "not enough params for counter field type. Set [\"unique_field\", \"cardinality_field\"]");
        this.cardinalityField = Preconditions.checkNotNull(Strings.emptyToNull(args[0]), "value of cardinality_field param is empty or null");
    }
}
