package com.company.beans.util;

import com.company.commons.Exceptions;

import java.util.Arrays;

/**
 * Class for escaping/un-escaping line delimiter and field delimiter
 * characters in TSV lines
 */
public class LogFieldEscaper {
    private static final char ESCAPE_CHAR = '\\';
    public static final int MAX_ESCAPE_CHAR = 256;
    private char[] escape = new char[MAX_ESCAPE_CHAR + 1];
    private char[] unescape = new char[MAX_ESCAPE_CHAR + 1];

    public LogFieldEscaper(char[] chars, char[] codes) {
        Arrays.fill(escape, (char)0);
        Arrays.fill(unescape, (char)0);
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            char code = codes[i];
            if (c > MAX_ESCAPE_CHAR) {
                throw Exceptions.illegalState("Only chars from [0, %s] could be escaped. '%s' is %s", MAX_ESCAPE_CHAR, c, (int) c);
            }
            if (code > MAX_ESCAPE_CHAR) {
                throw Exceptions.illegalState("Only chars from [0, %s] could be codes. '%s' is %s", MAX_ESCAPE_CHAR, code, (int) code);
            }
            escape[c] = code;
            unescape[code] = c;
        }
    }



    public String escapeLogField(String field) {
        StringBuilder b = new StringBuilder(field.length() + 10);
        boolean hasChanges = false;
        for (int i = 0; i < field.length(); i++) {
            char c = field.charAt(i);
            if (c >= 0 && c < escape.length && escape[c] > 0) {
                hasChanges = true;
                b.append(ESCAPE_CHAR).append(escape[c]);
            } else if (c == ESCAPE_CHAR) {
                hasChanges = true;
                b.append(ESCAPE_CHAR).append(ESCAPE_CHAR);
            } else {
                b.append(c);
            }
        }
        return hasChanges ? b.toString() : field;
    }


    public String unEscapeLogField(String field) {
        StringBuilder b = new StringBuilder(field.length());
        boolean hasChanges = false;
        for (int i = 0; i < field.length(); i++) {
            char c = field.charAt(i);
            if (c == ESCAPE_CHAR && i + 1 < field.length()) {
                hasChanges = true;
                int nextIndex = ++i;
                char next = field.charAt(nextIndex);
                if (next >= 0 && next < unescape.length && unescape[next] > 0) {
                    b.append(unescape[next]);
                } else if (next == ESCAPE_CHAR) {
                    b.append(ESCAPE_CHAR);
                } else {
                    b.append(ESCAPE_CHAR).append(next);
                }
            } else {
                b.append(c);
            }
        }
        return hasChanges ? b.toString() : field;
    }
}
