package com.company.beans;

import com.google.common.base.*;
import com.google.common.collect.Sets;
import com.company.commons.Util;
import org.joda.time.DateTime;

import javax.annotation.Nullable;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DataTypes {
    public static final DecimalFormat PERCENT_FORMAT = Util.decimalFormat("#.###%");
    private static Map<String, Function<String, DataType>> types = new HashMap<String, Function<String, DataType>>();

    public static void register(String name, Function<String, DataType> type) {
        types.put(name.toLowerCase(), type);
    }

    public static void register(String name, DataType type) {
        Function constant = Functions.constant(type);
        types.put(name.toLowerCase(), constant);
    }

    static {
        register("integer", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return ScalarType.forClass(Integer.class);
            }
        });
        register("boolean", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return ScalarType.forClass(Boolean.class);
            }
        });
        register("long", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return ScalarType.forClass(Long.class);
            }
        });
        register("date_time", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return new DateTimeType();
            }
        });
        register("string", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return ScalarType.forClass(String.class);
            }
        });
        register("double", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return ScalarType.forClass(Double.class);
            }
        });
        register("ip_addr", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return InetAddressType.T;
            }
        });
        register("enum", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return new EnumType();
            }
        });
        register("set", new Function<String, DataType>() {
            @Override
            public DataType apply(@Nullable String input) {
                return new SetDataType();
            }
        });
        register("percents", new Function<String, DataType>() {
            @Nullable
            @Override
            public DataType apply(@Nullable String s) {
                return new PercentsType();
            }
        });
        register(UniqueCounterType.NAME, new Function<String, DataType>() {
            @Nullable
            @Override
            public DataType apply(@Nullable String input) {
                return new UniqueCounterType();
            }
        });
    }

    public static DataType getType(String name) {
        Function<String, DataType> type = types.get(name.toLowerCase());
        Preconditions.checkNotNull(type, "Unknown type %s", name);
        return type.apply(name);
    }

    public static Class<? extends DataType> getTypeByClass(Class<?> fieldType) {
        if (InetAddress.class.isAssignableFrom(fieldType)) {
            return InetAddressType.class;
        } else if (DateTime.class.isAssignableFrom(fieldType)) {
            return DateTimeType.class;
        } else if (Enum.class.isAssignableFrom(fieldType)) {
            return EnumType.class;
        } else if (getCustomInnerTypeClass(fieldType) != null) {
            return getCustomInnerTypeClass(fieldType);
        } else if (ScalarType.isApplicableTo(fieldType)){
            return ScalarType.class;
        } else {
            throw new IllegalStateException("Can't get data type for " + fieldType + ". Please, set type explicitly");
        }
    }

    private static Class getCustomInnerTypeClass(Class<?> fieldType) {
        for (Class innerClass : fieldType.getDeclaredClasses()) {
            if (DataType.class.isAssignableFrom(innerClass) && innerClass.getSimpleName().equals("Type")) {
                return innerClass;
            }
        }
        return null;
    }

    private static class SetDataType implements DataType<Set<String>>, Comparator<Set<String>> {
        public static final Joiner JOINER = Joiner.on(",");
        public static final Splitter SPLITTER = Splitter.on(",").omitEmptyStrings();

        @Override
        public Set<String> parse(String str) {
            return Sets.newLinkedHashSet(SPLITTER.split(str));
        }

        @Override
        public String toString(Set<String> object) {
            return JOINER.join(object);
        }

        @Override
        public String describe() {
            return "Set";
        }

        @Override
        public int compare(Set<String> o1, Set<String> o2) {
            return toString(o1).compareTo(toString(o2));
        }
    }

    private static class PercentsType implements DataType<Double>, Comparator<Double> {
        @Override
        public int compare(Double o1, Double o2) {
            return Util.nullSafeCompare(o1, o2);
        }

        @Override
        public Double parse(String str) {
            if (Strings.isNullOrEmpty(str)) {
                return null;
            } else {
                if (str.endsWith("%")) {
                    str = str.substring(0, str.length() - 1);
                }
                return Double.parseDouble(str)/1000;
            }
        }

        @Override
        public String toString(Double object) {
            if (object == null) {
                return "";
            } else {
                return PERCENT_FORMAT.format(object);
            }
        }

        @Override
        public String describe() {
            return "percents";
        }
    }
}
