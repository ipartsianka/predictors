package com.company.beans;

import com.google.common.base.Defaults;
import com.google.common.collect.Sets;
import com.google.common.primitives.Primitives;
import com.company.commons.Util;

import java.io.DataInput;
import java.io.DataOutput;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class ScalarType<T> implements ClassAware, DataType<T>, BinaryType<T>, Comparator<T> {
    public static final Set<Class> CLASSES_WHITELIST = Sets.<Class>newHashSet(
            int.class, Integer.class,
            byte.class, Byte.class,
            long.class, Long.class,
            short.class, Short.class,
            boolean.class, Boolean.class,
            char.class, Character.class,
            double.class, Double.class,
            float.class, Float.class,
            String.class);
    private static Map<Class, String> binaryMethodPrefix = new HashMap<Class, String>();

    static {
        binaryMethodPrefix.put(Long.class, "Long");
        binaryMethodPrefix.put(Integer.class, "Int");
        binaryMethodPrefix.put(Short.class, "Short");
        binaryMethodPrefix.put(Double.class, "Double");
        binaryMethodPrefix.put(Boolean.class, "Boolean");
        binaryMethodPrefix.put(Character.class, "Char");
        binaryMethodPrefix.put(Float.class, "Float");
    }

    private Class<T> scalarClass;
    private Method method;
    private T nullValue = null;
    private Method binaryReadMethod;
    private Method binaryWriteMethod;

    /**
     * HACK: the usual schema doesn't work for byte, so
     * we mark byt scalar type
     */
    private boolean isByte = false;

    public static <T> ScalarType<T> forClass(Class<T> scalarClass) {
        ScalarType<T> type = new ScalarType<T>();
        type.setClass(scalarClass);
        return type;
    }

    public ScalarType() {
    }

    public T parse(String str) {
        if (method == null) {
            return (T) str;
        }
        if (str == null || str.equals("null") || str.equals("")) {
            return nullValue;
        }
        try {
            return (T) method.invoke(null, str);
        } catch (Exception e) {
            throw new IllegalStateException("Can't parse " + str + ": " + e.getMessage(), e);
        }
    }

    @Override
    public String toString(T object) {
        return object == null ? "" : object.toString();
    }

    @Override
    public String describe() {
        return scalarClass.getSimpleName();
    }


    @Override
    public void setClass(Class cls) {
        if (cls.isPrimitive()) {
            nullValue = (T) Defaults.defaultValue(cls);
            String name;
            if (cls == int.class) {
                name = Integer.class.getName();
            } else if (cls == char.class) {
                name = Character.class.getName();
            } else {
                name = "java.lang." + Character.toUpperCase(cls.getName().charAt(0)) + cls.getName().substring(1);
            }
            try {
                cls = Class.forName(name);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException(e.getMessage(), e);
            }
        }
        scalarClass = cls;
        if (cls != String.class) {
            try {
                method = scalarClass.getMethod("valueOf", new Class[]{String.class});
            } catch (NoSuchMethodException e) {
                throw new IllegalArgumentException("Class " + scalarClass.getName() + " is not scalar! There's no valueOf(String) method");
            }
            if (!Modifier.isStatic(method.getModifiers())) {
                throw new IllegalArgumentException("Method " + scalarClass.getName() + ".valueOf() is not static");
            }
            if (!Modifier.isPublic(method.getModifiers())) {
                throw new IllegalArgumentException("Method " + scalarClass.getName() + ".valueOf() is not public");
            }
            try {
                //hack: support for byte
                if (Primitives.wrap(scalarClass).equals(Byte.class)) {
                    isByte = true;
                } else {
                    binaryWriteMethod = DataOutput.class.getMethod("write" + binaryMethodPrefix.get(scalarClass), Primitives.unwrap(scalarClass));
                    binaryReadMethod = DataInput.class.getMethod("read" + binaryMethodPrefix.get(scalarClass));
                }
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e.getMessage(), e);
            }
        } else {
            try {
                binaryWriteMethod = DataOutput.class.getMethod("writeUTF", String.class);
                binaryReadMethod = DataInput.class.getMethod("readUTF");
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e.getMessage(), e);
            }
        }
    }

    public Class<T> getScalarClass() {
        return scalarClass;
    }

    @Override
    public T read(DataInput in) {
        try {
            boolean isNull = in.readBoolean();
            if (isNull) {
                return null;
            }
            if (isByte) {
                return (T) (Object) in.readByte();
            }
            return (T) binaryReadMethod.invoke(in);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void write(T obj, DataOutput out) {
        try {
            if (obj == null) {
                out.writeBoolean(true);
            } else {
                out.writeBoolean(false);
                if (isByte) {
                    Byte b = (Byte) obj;
                    out.writeByte(b);
                } else {
                    binaryWriteMethod.invoke(out, obj);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public int compare(T t, T t1) {
        return Util.nullSafeCompare((Comparable) t, (Comparable) t1);
    }

    public static boolean isApplicableTo(Class<?> cls) {
        return CLASSES_WHITELIST.contains(cls);
    }
}
