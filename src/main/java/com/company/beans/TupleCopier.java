package com.company.beans;

public class TupleCopier {
    private TupleSchema targetSchema;
    private int[] indexMap;

    public TupleCopier(TupleSchema sourceSchema, TupleSchema targetSchema) {
        this.targetSchema = targetSchema;
        indexMap = new int[sourceSchema.size()];
        for (int i = 0; i < sourceSchema.size(); i++) {
            int index = targetSchema.indexOf(sourceSchema.getField(i).getName());
            indexMap[i] = index >= 0 ? index : -1;
        }
    }


    public Tuple copy(Tuple source, Tuple targetReuse) {
        Tuple tuple = targetReuse == null ? targetSchema.newTuple() : targetReuse;
        for (int i = 0; i < indexMap.length; i++) {
            if (indexMap[i] >= 0) {
                tuple.set(indexMap[i], source.get(i));
            }
        }
        return tuple;
    }

    public Tuple copy(Tuple source) {
        return copy(source, null);
    }
}
