package com.company.beans;

import com.google.common.base.Strings;
import com.company.commons.Util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Comparator;

public class InetAddressType implements DataType<InetAddress>, BinaryType<InetAddress>, Comparator<InetAddress> {
    public static final InetAddressType T = new InetAddressType();

    private InetAddressType() {}

    @Override
    public InetAddress read(DataInput in) throws IOException {
        int len = in.readByte();
        if (len < 0) {
            return null;
        }
        byte[] bytes = new byte[len];
        in.readFully(bytes);
        return InetAddress.getByAddress(bytes);
    }

    @Override
    public void write(InetAddress obj, DataOutput out) throws IOException {
        if (obj == null) {
            out.writeByte(-1);
            return;
        }
        byte[] bytes = obj.getAddress();
        if (bytes.length > Byte.MAX_VALUE) {
            throw new IllegalStateException("Address is to long (" + bytes.length + "). Max value is: " + Byte.MAX_VALUE);
        }
        out.writeByte((byte)bytes.length);
        out.write(bytes);
    }

    @Override
    public InetAddress parse(String str) {
        if (Strings.isNullOrEmpty(str) || str.equals("null")) {
            return null;
        }
        try {
            return InetAddress.getByName(str);
        } catch (UnknownHostException e) {
            throw new IllegalStateException("Can't parse ip " + str);
        }
    }

    @Override
    public String toString(InetAddress object) {
        if (object == null) {
            return "";
        }
        return object.getHostAddress();
    }

    @Override
    public String describe() {
        return getClass().getSimpleName();
    }

    @Override
    public int compare(InetAddress inetAddress, InetAddress inetAddress1) {
        return Util.nullSafeCompare(inetAddress, inetAddress1, new Comparator<InetAddress>() {
            @Override
            public int compare(InetAddress i1, InetAddress i2) {
                return InetAddressType.this.toString(i1).compareTo(InetAddressType.this.toString(i2));
            }
        });
    }
}
