package com.company.beans;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.company.beans.util.LogFieldEscaper;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.EOFException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;

public class BeanSerializer<T> implements DataType<T>, ClassAware<T>, ParamsAware, BinaryType<T> {
    public static final char DEFAULT_SEPARATOR = '\t';
    private Class<T> cls;
    private char separator = DEFAULT_SEPARATOR;
    private LinkedHashMap<String, FieldInfo> fieldsDictionary = new LinkedHashMap<String, FieldInfo>();
    private List<FieldInfo> fieldList = new ArrayList<FieldInfo>();
    private List<FieldInfo> presentFieldList = new ArrayList<FieldInfo>();
    private Constructor<T> constructor;
    private LogFieldEscaper escaper;
    private boolean fillWithDefaultsAfterEOF = false;

    public BeanSerializer(Class<T> cls, char separator, String[] header) {
        this.cls = cls;
        try {
            constructor = cls.getDeclaredConstructor();
            constructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("Can't find default constructor in " + cls.getName(), e);
        }
        this.separator = separator;
        escaper = new LogFieldEscaper(new char[]{'\n', separator,'\r'},new char[]{'n','t','r'});
        buildFieldDictionary();
        setHeader(header);
    }

    public BeanSerializer(Class<T> cls, char separator, String header) {
        this(cls, separator, Iterables.toArray(Splitter.on(separator).split(header), String.class));
    }

    public BeanSerializer(Class<T> cls, char separator) {
        this(cls, separator, new String[]{});
    }

    public BeanSerializer(Class<T> cls) {
        this(cls, DEFAULT_SEPARATOR);
    }

    public BeanSerializer() {
    }

    public void setFillWithDefaultsAfterEOF(boolean fillWithDefaultsAfterEOF) {
        this.fillWithDefaultsAfterEOF = fillWithDefaultsAfterEOF;
    }

    public void setHeader(String[] header) {
        if (header == null || header.length == 0) {
            return;
        }
        if (fieldsDictionary == null) {
            throw new IllegalStateException("buildFieldDictionary() should be called before setHeader()");
        }
        fieldList = new ArrayList<FieldInfo>();
        presentFieldList = new ArrayList<FieldInfo>();
        for (String h : header) {
            FieldInfo fieldInfo = fieldsDictionary.get(h);
            if (fieldInfo != null) {
                fieldList.add(fieldInfo);
                presentFieldList.add(fieldInfo);
            } else {
                fieldList.add(null);
            }
        }
    }

    protected List<FieldInfo> allFields() {
        return presentFieldList;
    }

    private LinkedHashMap<String, FieldInfo> listFields(Class cls, String namePrefix, List<java.lang.reflect.Field> chain) {
        LinkedHashMap<String, FieldInfo> res = new LinkedHashMap<String, FieldInfo>();
        for (java.lang.reflect.Field f : getAllFields(cls)) {
            ArrayList<java.lang.reflect.Field> newChain = new ArrayList<java.lang.reflect.Field>(chain);
            newChain.add(f);
            SerializableField annotation = f.getAnnotation(SerializableField.class);
            Class<?> fieldType = f.getType();
            if (annotation != null) {
                String name = namePrefix + (annotation.value().isEmpty() ? f.getName() : annotation.value());
                DataType dataType;
                Class<? extends DataType> type = annotation.type();
                if (type == DataType.class) {
                    type = DataTypes.getTypeByClass(fieldType);

                }
                try {
                    dataType = newInstance(type);
                } catch (Exception e) {
                    throw new IllegalStateException("Can't create instance of format class " + type.getClass() + ": " + e.getMessage(), e);
                }
                if (dataType instanceof ClassAware) {
                    ((ClassAware) dataType).setClass(fieldType);
                }
                if (dataType instanceof ParamsAware && annotation.args().length != 0) {
                    ((ParamsAware) dataType).setParams(annotation.args());
                }
                if (res.put(name, new FieldInfo(name, dataType, newChain, annotation.required())) != null) {
                    throw new IllegalStateException("Can't register field with name '" + name + "'. Field with same name was already registered");
                }
            }
            EmbeddedDataField em = f.getAnnotation(EmbeddedDataField.class);
            f.setAccessible(true);
            if (em != null) {
                String prefix = em.fieldPrefix();
                if (prefix.equals(EmbeddedDataField.INHERIT_PREFIX)) {
                    prefix = f.getName() + "_";
                }
                LinkedHashMap<String, FieldInfo> embedded = listFields(fieldType, namePrefix + prefix, newChain);
                for (Map.Entry<String, FieldInfo> e : embedded.entrySet()) {
                    if (res.put(e.getKey(), e.getValue()) != null) {
                        throw new IllegalStateException("Can't register field with name '" + e.getKey() + "'. Field with same name was already registered");
                    }
                }
            }
        }
        return res;
    }

    private void buildFieldDictionary() {
        fieldsDictionary = listFields(cls, "", new ArrayList<java.lang.reflect.Field>());
        fieldList = new ArrayList<FieldInfo>(fieldsDictionary.values());
        presentFieldList = new ArrayList<FieldInfo>(fieldsDictionary.values());
    }

    public String[] getHeader() {
        String[] header = new String[presentFieldList.size()];
        for (int i = 0, fieldListSize = presentFieldList.size(); i < fieldListSize; i++) {
            FieldInfo f = presentFieldList.get(i);
            header[i] = f.name;
        }
        return header;
    }

    @Override
    public T read(DataInput in) throws IOException {
        T instance;
        try {
            instance = newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Can't make new instance of " + cls.getName() + " check if it has default constructor", e);
        }
        boolean isEof = false;
        for (FieldInfo f : presentFieldList) {
            if (!(f.type instanceof BinaryType)) {
                throw new IllegalStateException("Field " + f.name + " doesn't support binary serialization");
            }
            BinaryType type = (BinaryType) f.type;
            if (!isEof) {
                Object obj;
                try {
                    obj = type.read(in);
                    f.set(instance, obj);
                } catch (EOFException e) {
                    if (!fillWithDefaultsAfterEOF) {
                        throw new RuntimeException("Unable to read field " + f.name + " type of " + f.type + ": end of stream", e);
                    } else {
                        isEof = true;
                    }
                } catch (Exception e) {
                    throw new RuntimeException("Unable to read field " + f.name + " type of " + f.type, e);
                }
            } else {
                if (f.required) {
                    throw new RuntimeException("Stream ended, but field " + f.name + " type of " + f.type + " is required and cannnot be filled with default value");
                }
            }
        }
        return instance;
    }

    @Override
    public void write(T obj, DataOutput out) throws IOException {
        for (FieldInfo f : presentFieldList) {
            if (!(f.type instanceof BinaryType)) {
                throw new IllegalStateException("Field " + f.name + " type of " + f.type.describe() + " isn't support binary serialization");
            }
            BinaryType type = (BinaryType) f.type;
            type.write(f.get(obj), out);
        }
    }

    private static class FieldInfo {
        private final boolean required;
        private List<java.lang.reflect.Field> fieldChain;
        private String name;
        private DataType type;

        private FieldInfo(String name, DataType type, List<java.lang.reflect.Field> fieldChain, boolean required) {
            this.name = name;
            this.type = type;
            this.required = required;
            this.fieldChain = fieldChain;
        }

        public void set(Object instance, Object value) {
            for (Iterator<java.lang.reflect.Field> iterator = fieldChain.iterator(); iterator.hasNext(); ) {
                java.lang.reflect.Field f = iterator.next();
                try {
                    if (!iterator.hasNext()) {
                        f.set(instance, value);
                    } else {
                        Object newInstance = f.get(instance);
                        if (newInstance == null) {
                            newInstance = newInstance(f.getType());
                            f.set(instance, newInstance);
                        }
                        instance = newInstance;
                    }
                } catch (Exception e) {
                    throw new IllegalStateException(e.getMessage(), e);
                }
            }
        }

        public Object get(Object instance) {
            for (Iterator<java.lang.reflect.Field> iterator = fieldChain.iterator(); iterator.hasNext(); ) {
                java.lang.reflect.Field f = iterator.next();
                try {
                    if (instance == null) {
                        return null;
                    }
                    if (!iterator.hasNext()) {
                        return f.get(instance);
                    } else {
                        instance = f.get(instance);
                    }
                } catch (Exception e) {
                    throw new IllegalStateException(e.getMessage(), e);
                }
            }
            //never happens
            throw new Error();
        }

        public String toString() {
            return name;
        }

    }


    public T parse(Map<String, String> map) {
        Iterable<Map.Entry<String, String>> entries = map.entrySet();
        return parse(entries);
    }

    public T parse(Iterable<Map.Entry<String, String>> entries) {
        T res = newInstance();
        for (Map.Entry<String, String> e : entries) {
            FieldInfo targetField = fieldsDictionary.get(e.getKey().toLowerCase());
            if (targetField != null) {
                Object value;
                try {
                    value = targetField.type.parse(e.getValue());
                } catch (Exception ex) {
                    throw new RuntimeException("Format of field " + e.getKey() + "=" + e.getValue() + " mismatch: " + ex.getMessage(), ex);
                }
                targetField.set(res, value);
            }
        }
        return res;
    }


    public T parse(String str) {
        T instance;
        try {
            instance = newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Can't make new instance of " + cls.getName() + " check if it has default constructor", e);
        }
        int i = 0;
        for (String field : Splitter.on(separator).split(str)) {
            field = escaper.unEscapeLogField(field);
            FieldInfo fieldInfo;
            try {
                fieldInfo = fieldList.get(i++);
            } catch (IndexOutOfBoundsException e) {
                throw new IllegalStateException("Number of fields in line exceeded number of fields in object (" + fieldList.size() + "). Line: '" + str + "'");
            }
            if (fieldInfo != null) {
                Object val = fieldInfo.type.parse(field);
                fieldInfo.set(instance, val);
            }
        }
        if (i != fieldList.size()) {
            throw new IllegalStateException("Too few fields in line (" + i + "). It should be equal to fields in object (" + fieldList.size() + "). Line: '" + str + "'");
        }

        return instance;
    }

    private static final Map<Class, Constructor> constructorCache = new HashMap<Class, Constructor>();

    private static <K> K newInstance(Class<K> cls) {
        Constructor constructor = constructorCache.get(cls);
        if (constructor == null) {
            try {
                constructor = cls.getDeclaredConstructor();
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException("Can't find default constructor in " + cls.getName(), e);
            }
            constructor.setAccessible(true);
            constructorCache.put(cls, constructor);
        }
        try {
            return (K) constructor.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Can't create instance of " + cls.getName(), e);
        }
    }

    private T newInstance() {
        try {
            return constructor.newInstance();
        } catch (Throwable e) {
            throw new IllegalStateException("Can't instantiate " + cls.getName(), e);
        }
    }

    @Override
    public String toString(T object) {
        String[] fields = toArray(object);
        return Joiner.on(separator).join(Iterators.transform(Iterators.forArray(fields), new Function<String, Object>() {
            @Override
            public Object apply(String input) {
                return escaper.escapeLogField(input);
            }
        }));
    }

    public String[] toArray(T object) {
        String[] fields = new String[presentFieldList.size()];
        for (int i = 0, allFieldsSize = presentFieldList.size(); i < allFieldsSize; i++) {
            FieldInfo f = presentFieldList.get(i);
            fields[i] = f.type.toString(f.get(object));
        }
        return fields;
    }

    @Override
    public String describe() {
        return cls.getSimpleName() + "Serializer";
    }

    /**
     * Returns all field of class and its superclasses
     *
     * @param cls class
     * @return all fields
     */
    public static List<java.lang.reflect.Field> getAllFields(Class cls) {
        List<java.lang.reflect.Field> r = new ArrayList<java.lang.reflect.Field>();
        while (cls != null) {
            List<java.lang.reflect.Field> newList = new ArrayList<java.lang.reflect.Field>(Arrays.asList(cls.getDeclaredFields()));
            newList.addAll(r);
            r = newList;
            cls = cls.getSuperclass();
        }
        return r;
    }

    @Override
    public void setClass(Class cls) {
        this.cls = cls;
        buildFieldDictionary();
    }

    @Override
    public void setParams(String[] args) {
        this.separator = args[0].charAt(0);
    }


    public static String getCompoundTupleSchema(List<Class> classes) {
        LinkedHashMap<String, FieldInfo> fields = new LinkedHashMap<String, FieldInfo>();
        for (Class c : classes) {
            BeanSerializer<?> s = new BeanSerializer(c);
            for (FieldInfo f : s.allFields()) {
                if (!fields.containsKey(f.name)) {
                    fields.put(f.name, f);
                }
            }
        }
        StringBuffer res = new StringBuffer("[\n");
        for (FieldInfo i : fields.values()) {
            res.append("\t").append(String.format("{\"name\": \"%s\", \"type\": \"%s\"}", i.name, i.type.describe())).append("\n");
        }
        res.append("]\n");
        return res.toString();
    }

}
