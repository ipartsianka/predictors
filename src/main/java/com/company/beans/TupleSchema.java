package com.company.beans;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.company.commons.Exceptions;
import com.company.commons.JsonHelper;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.*;

public class TupleSchema {
    private ArrayList<Field> fields = new ArrayList<Field>();
    private Map<String, Integer> index = new HashMap<String, Integer>();
    private JsonNode jsonNode;
    private Comparator<Tuple> comparator;

    private TupleSchema() {
    }

    public TupleSchema(JsonNode jsonNode) {
        this.jsonNode = jsonNode;
        Preconditions.checkArgument(jsonNode.isArray(), "TupleSchema should be constructed from json array");
        for (JsonNode n : jsonNode) {
            String name = n.get("name").asText();
            String typeName = n.get("type").asText();
            DataType type = DataTypes.getType(typeName);
            if (n.has("options")) {
                Preconditions.checkState(type instanceof ParamsAware, "Options specified for field %s, but it's type (%s) doesn't support additional configuration", name, typeName);
                List<String> list = JsonHelper.toStringList(n.get("options"));
                ((ParamsAware) type).setParams(list.toArray(new String[list.size()]));
            }
            if (n.has("class")) {
                Preconditions.checkState(type instanceof ClassAware, "Class specified for field %s, but it's type (%s) doesn't support additional configuration", name, typeName);
                try {
                    ((ClassAware) type).setClass(Class.forName(n.get("class").asText()));
                } catch (ClassNotFoundException e) {
                    throw Exceptions.wrap(e);
                }
            }
            Preconditions.checkNotNull(type, "Type %s of %s is unknown", typeName, name);
            List<String> aliases = n.has("aliases") ? JsonHelper.toStringList(n.get("aliases")) : Collections.<String>emptyList();
            Field field = new Field(name, type, aliases);
            if (n.has("default")) {
                field.parseAndSetDefaultValue(n.get("default").asText());
            }
            if (n.has("repeated")){
                field.setRepeated(n.get("repeated").asBoolean());
            }
            if (n.has("sub_type")){
                field.setSubType(DataTypes.getType(n.get("sub_type").asText()));
            }
            addField(field);
        }
        init();

    }

    private void init() {
        if (allFielsAreComparable()) {
            this.comparator = makeComparator();
        }
    }

    public TupleSchema slice(Collection<String> fields) {
        TupleSchema slice = new TupleSchema();
        for (String field : fields) {
            Field f = this.fields.get(indexOfStrict(field));
            slice.addField(f);
        }
        slice.init();
        return slice;
    }

    public TupleSchema(String json) {
        this(parse(json));
    }

    private static JsonNode parse(String json) {
        ObjectMapper m = new ObjectMapper();
        m.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        m.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        try {
            return m.readTree(json);
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    public Tuple newTuple() {
        Tuple t = new Tuple(this);
        for (int i = 0, fieldsSize = fields.size(); i < fieldsSize; i++) {
            Field f = fields.get(i);
            if (f.hasDefaultValue) {
                t.set(i, f.defaultValue);
            }
        }
        return t;
    }




    private void addField(Field field) {
        int fieldIndex = fields.size();
        Integer old = index.put(field.getName(), fieldIndex);
        Preconditions.checkState(old == null, "Field with name %s was already defined", field.getName());
        for (String a : field.aliases) {
            index.put(a, fieldIndex);
        }
        fields.add(field);
    }

    public int indexOf(String field) {
        Integer idx = index.get(field);
        return idx == null ? -1 : idx;
    }

    public int indexOfStrict(String field) {
        Integer idx = index.get(field);
        Preconditions.checkNotNull(idx, "Field with name %s isn't found in %s", field, this);
        return idx;
    }

    public String toString() {
        return "{" + Joiner.on(",").join(fields) + "}";
    }

    public String getFieldsNames() {
        ArrayList<String> names = new ArrayList<String>();
        for (Field field : fields)
            names.add(field.getName());
        return Joiner.on("\t").join(names);
    }

    public int size() {
        return fields.size();
    }


    public ArrayList<Field> getFields() {
        return fields;
    }

    public Field getField(int idx) {
        return getFields().get(idx);
    }

    public Field getField(String name) {
        int idx = indexOf(name);
        return idx < 0 ? null : getField(idx);
    }

    public void write(JsonGenerator gen) {
        try {
            gen.writeTree(jsonNode);
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    public Field field(int idx) {
        return getField(idx);
    }


    public static class Field {
        private String name;
        private DataType type;
        private Object defaultValue;
        private boolean hasDefaultValue;
        private List<String> aliases;
        private boolean repeated;
        private DataType subType;

        public Field(String name, DataType type, List<String> aliases) {
            this.name = name;
            this.type = type;
            this.aliases = aliases;
            this.repeated = false;
        }

        public void setDefaultValue(Object object) {
            hasDefaultValue = true;
            defaultValue = object;
        }

        public void parseAndSetDefaultValue(String defaultValue) {
            setDefaultValue(type.parse(defaultValue));
        }

        public String getName() {
            return name;
        }

        public DataType getType() {
            return type;
        }

        public String toString() {
            return name + ":" + type.describe();
        }

        public boolean hasDefaultValue() {
            return hasDefaultValue;
        }

        public List<String> getAliases() {
            return aliases;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Field field = (Field) o;

            if (!name.equals(field.name)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        public void setRepeated(boolean repeated){
            this.repeated = repeated;
        }

        public boolean isRepeated(){
            return repeated;
        }

        public DataType getSubType() {
            return subType;
        }

        public void setSubType(DataType subType) {
            this.subType = subType;
        }
    }

    private boolean allFielsAreComparable() {
        for (int i = 0, fieldsSize = fields.size(); i < fieldsSize; i++) {
            Field f = fields.get(i);
            if (!(f.type instanceof Comparator)) {
                return false;
            }
        }
        return true;
    }

    private Comparator<Tuple> makeComparator() {
        final Comparator[] comparators = new Comparator[fields.size()];
        for (int i = 0, fieldsSize = fields.size(); i < fieldsSize; i++) {
            Field f = fields.get(i);
            if (!(f.type instanceof Comparator)) {
                throw new IllegalStateException("Can't make comparator, field " + f.getName() + " isn't comparable. Type: " + f.getType());
            } else {
                comparators[i] = (Comparator)f.getType();
            }
        }
        return new Comparator<Tuple>() {
            @Override
            public int compare(Tuple tuple, Tuple tuple1) {
                if (tuple == tuple1) {
                    return 0;
                }
                for (int i = 0; i< fields.size(); i++) {
                    Object v1 = tuple.get(i);
                    Object v2 = tuple1.get(i);
                    int res = comparators[i].compare(v1, v2);
                    if (res != 0) {
                        return res;
                    }
                }
                return 0;
            }
        };
    }

    public Comparator<Tuple> getComparator() {
        if (comparator == null) {
            //Trick: the method will throw an exception and tell what fields aren't comparable
            makeComparator();
        }
        return comparator;
    }
}
