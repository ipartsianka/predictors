package com.company.beans;

public interface ClassAware<T> {
    public void setClass(Class<T> cls);
}
