package com.company.beans;

import com.company.commons.Util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class EnumType<T extends Enum<T>> implements DataType<T>, ParamsAware, ClassAware, BinaryType<T>, Comparator<T> {
    private Class cls;
    private Map<String, T> stringIndex;
    private Map<T, String> enumIndex;
    private String keyField;

    @Override
    public void setClass(Class cls) {
        this.cls = cls;
        for (Field f : BeanSerializer.getAllFields(cls)) {
            if (f.getAnnotation(KeyField.class) != null) {
                keyField = f.getName();
            }
        }
        rebuildIndex();
    }

    private void rebuildIndex() {
        stringIndex = new HashMap<String, T>();
        enumIndex = new HashMap<T, String>();
        if (cls != null) {
            Object[] constants = cls.getEnumConstants();
            for (Object o : constants) {
                String key;
                if (keyField != null) {
                    Field field;
                    try {
                        field = cls.getDeclaredField(keyField);
                    } catch (NoSuchFieldException e) {
                        throw new IllegalStateException("Can'f find field " + keyField + " in enum " + cls.getName(), e);
                    }
                    field.setAccessible(true);
                    try {
                        key = String.valueOf(field.get(o));
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException("Can't get value of field " + cls.getName() + "." + keyField);
                    }
                } else {
                    key = ((Enum)o).name();
                }
                stringIndex.put(key, (T) o);
                enumIndex.put((T) o, key);
            }
        }
    }

    @Override
    public T parse(String str) {
        if (str == null || "null".equals(str) || str.equals("")) {
            return null;
        }
        T t = stringIndex.get(str);
        if (t == null) {
            throw new IllegalStateException("Unknown enum key " + str);
        }
        return t;
    }

    @Override
    public String toString(T object) {
        if (object == null) {
            return "";
        }
        return enumIndex.get(object);
    }

    @Override
    public String describe() {
        return cls.getSimpleName();
    }

    @Override
    public void setParams(String[] args) {
        if (args.length != 0) {
            this.keyField = args[0];
        }
        rebuildIndex();
    }

    @Override
    public T read(DataInput in) throws IOException {
        return stringIndex.get(in.readUTF());
    }

    @Override
    public void write(T obj, DataOutput out) throws IOException {
        String s = enumIndex.get(obj);
        out.writeUTF(s == null ? "" : s);
    }

    @Override
    public int compare(T t, T t1) {
        return Util.nullSafeCompare(toString(t), toString(t1));
    }
}
