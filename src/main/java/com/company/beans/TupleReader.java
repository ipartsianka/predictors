package com.company.beans;

import com.company.commons.Exceptions;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class TupleReader implements Closeable {
    private static final Logger LOG = LoggerFactory.getLogger(TupleReader.class);
    private BufferedReader reader;
    private final TupleSerializer serializer;
    private TupleSchema schema;
    private ArrayList<String> header;

    public TupleReader(TupleSchema schema, Reader r, String readerName) {
        try {
            this.schema = schema;
            reader = new BufferedReader(r);
            String header = reader.readLine();
            Preconditions.checkState(header != null, "Can't read header from %s", readerName == null ? "unknown source" : readerName);
            //sometimes first symbol is 0x0A
            if (header.length() == 0) {
                LOG.warn("First line is absent! {}", readerName);
                header = reader.readLine();
                Preconditions.checkState(header != null, "Can't read header from %s", readerName == null ? "unknown source" : readerName);
            }
            serializer = new TupleSerializer(schema);
            this.header = Lists.newArrayList(Splitter.on("\t").split(header));
            serializer.setHeader(this.header);
        } catch (Exception e) {
            throw Exceptions.wrap(e);
        }
    }

    public TupleReader(TupleSchema schema, Reader r) {
        this(schema, r, null);
    }

    public TupleReader(TupleSchema schema, File f, Charset charset) {
        this(schema, new InputStreamReader(openFile(f), charset), f.getAbsolutePath());
    }

    public TupleReader(TupleSchema schema, File f) {
        this(schema, f, Charset.defaultCharset());
    }

    /**
     * See {@link com.company.beans.TupleSerializer#validateDefaultValues()}
     * @throws IllegalStateException in case of header is inconsistent
     */
    public void validateDefaultValues() throws IllegalStateException {
        serializer.validateDefaultValues();
    }

    private static InputStream openFile(File f) {
        try {
            return f.getName().endsWith(".gz") ? new GZIPInputStream(new BufferedInputStream(new FileInputStream(f))) : new FileInputStream(f);
        } catch (Exception e) {
            throw new IllegalStateException("Unable to open file " + f.getAbsolutePath(), e);
        }
    }

    public Tuple readTuple() {
        try {
            String line = reader.readLine();
            if (line == null) {
                return null;
            } else {
                if (line.isEmpty()) {
                    return readTuple();
                }
                try {
                    return serializer.parse(line);
                } catch (Exception e) {
                    throw new IllegalStateException("Can't parse line '" + line + "' as " + this.schema + " with header " + header, e);
                }
            }
        } catch (IOException e) {
            throw Exceptions.wrap(e);
        }
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }


    public List<Tuple> readAll() {
        ArrayList<Tuple> r = new ArrayList<Tuple>();
        Tuple t = null;
        while ((t = readTuple()) != null) {
            r.add(t);
        }
        return r;
    }

    public List<Tuple> readAllAndClose() {
        try {
            return readAll();
        } finally {
            try {
                close();
            } catch (IOException e) {
                //ignore
            }
        }
    }
}
